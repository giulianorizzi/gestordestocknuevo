﻿/*
 * UNIVERSIDAD PROVINCIAL DE EZEIZA
 * CARRERA: TENICATURA UNIVERSITARIA EN DESARROLLO DE SOFTWARE
 * MATERIA: TECNICAS DE PROGRAMACION (1ER CUAT 2020)
 * TRABAJO PRACTICO INTEGRADOR
 * INTEGRANTES DEL EQUIPO:
 *      -RIZZI GIULIANO
 *      -SEGADE FLAVIO
 *      -LALANNE DARIO
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Telefono
    {
        private int idTelefono;
        private string codArea;
        private string numero;

        public int IdTelefono
        {
            set { this.idTelefono = value; }
            get { return this.idTelefono; }
        }

        public string CodArea
        {
            set { this.codArea = value; }
            get { return this.codArea; }
        }
        public string Numero
        {
            set { this.numero = value; }
            get { return this.numero; }
        }

        public override string ToString()
        {
            return "(" + this.codArea + ") " + this.numero;
        }
    }
}
