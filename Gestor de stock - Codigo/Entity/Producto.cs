﻿/*
 * UNIVERSIDAD PROVINCIAL DE EZEIZA
 * CARRERA: TENICATURA UNIVERSITARIA EN DESARROLLO DE SOFTWARE
 * MATERIA: TECNICAS DE PROGRAMACION (1ER CUAT 2020)
 * TRABAJO PRACTICO INTEGRADOR
 * INTEGRANTES DEL EQUIPO:
 *      -RIZZI GIULIANO
 *      -SEGADE FLAVIO
 *      -LALANNE DARIO
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Producto
    {
		private int codiigo;
		private string nombre;
		private string descripcion;
		private bool activo;
		private TipoProducto tipoProducto;
		private int stock;

		public int Codigo
		{
			get { return codiigo; }
			set { codiigo = value; }
		}

		public string Nombre
		{
			get { return nombre; }
			set { nombre = value; }
		}

		public string Descripcion
		{
			get { return descripcion; }
			set { descripcion = value; }
		}

		public bool Activo
		{
			get { return activo; }
			set { activo = value; }
		}

		public TipoProducto TipoDeProducto
		{
			get { return tipoProducto; }
			set { tipoProducto = value; }
		}

		public int Stock
		{
			get { return stock; }
			set { stock = value; }
		}

		public override string ToString()
		{
			return this.nombre;
		}

	}
}
