﻿/*
 * UNIVERSIDAD PROVINCIAL DE EZEIZA
 * CARRERA: TENICATURA UNIVERSITARIA EN DESARROLLO DE SOFTWARE
 * MATERIA: TECNICAS DE PROGRAMACION (1ER CUAT 2020)
 * TRABAJO PRACTICO INTEGRADOR
 * INTEGRANTES DEL EQUIPO:
 *      -RIZZI GIULIANO
 *      -SEGADE FLAVIO
 *      -LALANNE DARIO
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class OrdenDeCompra
    {
		private int numeroOrden;
		private DateTime fecha;
		private Proveedor proveedor;
		private EstadoOrden estado;
		private List<DetalleOrden> detalleOrden = new List<DetalleOrden>();
		private List<HistoricoOrdenDeCompra> listaHistoricos = new List<HistoricoOrdenDeCompra>();

		public List<DetalleOrden> DetalleOrden
		{
			get { return detalleOrden; }
			set { detalleOrden = value; }
		}
		public List<HistoricoOrdenDeCompra> ListaHistoricos
		{
			get { return listaHistoricos; }
			set { listaHistoricos = value; }
		}


		public EstadoOrden Estado
		{
			get { return estado; }
			set { estado = value; }
		}


		public Proveedor Proveedor
		{
			get { return proveedor; }
			set { proveedor = value; }
		}


		public DateTime Fecha
		{
			get { return fecha; }
			set { fecha = value; }
		}


		public int NumeroOrden
		{
			get { return numeroOrden; }
			set { numeroOrden = value; }
		}

	}
}
