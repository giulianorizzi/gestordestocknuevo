﻿/*
 * UNIVERSIDAD PROVINCIAL DE EZEIZA
 * CARRERA: TENICATURA UNIVERSITARIA EN DESARROLLO DE SOFTWARE
 * MATERIA: TECNICAS DE PROGRAMACION (1ER CUAT 2020)
 * TRABAJO PRACTICO INTEGRADOR
 * INTEGRANTES DEL EQUIPO:
 *      -RIZZI GIULIANO
 *      -SEGADE FLAVIO
 *      -LALANNE DARIO
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Usuario : Persona
    {
        public const int OK = 0;
        public const int ERR_NOMBRE = 1;
        public const int ERR_APELLIDO = 2;
        public const int ERR_CUIL = 3;
        public const int ERR_COD_AREA = 4;
        public const int ERR_NUM_TEL = 5;
        public const int ERR_CALLE = 6;
        public const int ERR_NUM_CALLE = 7;
        public const int ERR_PISO = 8;
        public const int ERR_LOC = 9;
        public const int ERR_TIPO = 10;
        public const int ERR_CUIL_REPETIDO = 11;
        public const int ERR_DB = 12;

        private string pass;
        private int legajo;
        private TipoUsuario tipoUsuario;
        private bool activo;

        public bool Activo
        {
            get { return activo; }
            set { activo = value; }
        }
        public TipoUsuario TipoUsuario
        {
            get { return tipoUsuario; }
            set { tipoUsuario = value; }
        }
        public int Legajo        
        {
            set { this.legajo = value; }
            get { return this.legajo; }
        }
        public string Pass
        {
            set { this.pass = value; }
            get { return this.pass; }
        }

        public override string ToString()
        {
            return Nombre + " " + Apellido + " (Legajo: " + legajo + ")";
        }
    }
}
