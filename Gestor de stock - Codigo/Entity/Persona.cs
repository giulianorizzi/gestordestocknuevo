﻿/*
 * UNIVERSIDAD PROVINCIAL DE EZEIZA
 * CARRERA: TENICATURA UNIVERSITARIA EN DESARROLLO DE SOFTWARE
 * MATERIA: TECNICAS DE PROGRAMACION (1ER CUAT 2020)
 * TRABAJO PRACTICO INTEGRADOR
 * INTEGRANTES DEL EQUIPO:
 *      -RIZZI GIULIANO
 *      -SEGADE FLAVIO
 *      -LALANNE DARIO
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Persona
    {
        private int idPersona;
        private string nombre;
        private string apellido;
        private string cuil;
        private Telefono telefono;
        private Domicilio domicilio;

        public int IdPersona
        {

            set { this.idPersona = value; }
            get { return this.idPersona; }
        }

        public string Nombre
        {

            set { this.nombre = value; }
            get { return this.nombre; }
        }
        public string Apellido
        {
            set { this.apellido = value; }
            get { return this.apellido; }
        }
        public Domicilio Domicilio
        {
            get { return domicilio; }
            set { domicilio = value; }
        }
        public Telefono Telefono
        {
            get { return telefono; }
            set { telefono = value; }
        }
        public string Cuil
        {
            set { this.cuil = value; }
            get { return this.cuil; }
        }
    }
}
