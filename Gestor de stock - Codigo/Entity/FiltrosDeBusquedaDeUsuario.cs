﻿/*
 * UNIVERSIDAD PROVINCIAL DE EZEIZA
 * CARRERA: TENICATURA UNIVERSITARIA EN DESARROLLO DE SOFTWARE
 * MATERIA: TECNICAS DE PROGRAMACION (1ER CUAT 2020)
 * TRABAJO PRACTICO INTEGRADOR
 * INTEGRANTES DEL EQUIPO:
 *      -RIZZI GIULIANO
 *      -SEGADE FLAVIO
 *      -LALANNE DARIO
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class FiltrosDeBusquedaDeUsuario
    {
        public const int TODOS_LOS_ESTADOS = 2;
        public const int TODOS_LOS_TIPOS = 0;
        public const int TODOS_LOS_LEGAJOS = 0;

        int estado = TODOS_LOS_ESTADOS;
        int legajo  =TODOS_LOS_LEGAJOS;
        TipoUsuario tipoUsuario = new TipoUsuario() { Descripcion ="",Codigo = TODOS_LOS_TIPOS};
        string cadenaContenidaEnNombreOApellido = "";

        public int Estado 
        {
            get { return estado; }
            set { estado = value; } 
        }
        public int Legajo
        {
            get { return legajo; }
            set { legajo = value; }
        }
        public string CadenaContenidaEnNombreOApellido
        {
            get { return cadenaContenidaEnNombreOApellido; }
            set { cadenaContenidaEnNombreOApellido = value; }
        }

        public TipoUsuario TipoUsuario
        {
            get { return tipoUsuario; }
            set { tipoUsuario = value; }
        }
    }
    
}
