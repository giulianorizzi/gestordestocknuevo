﻿/*
 * UNIVERSIDAD PROVINCIAL DE EZEIZA
 * CARRERA: TENICATURA UNIVERSITARIA EN DESARROLLO DE SOFTWARE
 * MATERIA: TECNICAS DE PROGRAMACION (1ER CUAT 2020)
 * TRABAJO PRACTICO INTEGRADOR
 * INTEGRANTES DEL EQUIPO:
 *      -RIZZI GIULIANO
 *      -SEGADE FLAVIO
 *      -LALANNE DARIO
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class DetalleOrden:Producto
    {
		private int nroDetalle;
		private int cantidadPedida;
		private int cantidadRecibida;
		private float precio;

		public int NroDetalle
		{
			get { return nroDetalle; }
			set { nroDetalle = value; }
		}

		public int CantidadPedida
		{
			get { return cantidadPedida; }
			set { cantidadPedida = value; }
		}

		public int CantidadRecibida
		{
			get { return cantidadRecibida; }
			set { cantidadRecibida = value; }
		}

		public float Precio
		{
			get { return precio; }
			set { precio = value; }
		}

		public override string ToString()
		{
			return this.nroDetalle.ToString();
		}


	}
}
