﻿/*
 * UNIVERSIDAD PROVINCIAL DE EZEIZA
 * CARRERA: TENICATURA UNIVERSITARIA EN DESARROLLO DE SOFTWARE
 * MATERIA: TECNICAS DE PROGRAMACION (1ER CUAT 2020)
 * TRABAJO PRACTICO INTEGRADOR
 * INTEGRANTES DEL EQUIPO:
 *      -RIZZI GIULIANO
 *      -SEGADE FLAVIO
 *      -LALANNE DARIO
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class FiltrosBusquedaDeOrdenes
    {
        public const int TODAS_LAS_ORDENES = 0;
        public const int TODOS_LOS_ESTADOS = 0;
        public const string TODOS_LOS_PROVEEDORES = "0";

        private int numOrden = TODAS_LAS_ORDENES;
        private EstadoOrden estado  = new EstadoOrden() { Codigo = TODOS_LOS_ESTADOS, Descripcion = "Todos Los Estados" };
        private DateTime fechaInicial;
        private DateTime fechaFinal;
        private bool filtrarPorFecha;
        private Proveedor proveedor = new Proveedor() { Cuit = TODOS_LOS_PROVEEDORES, RazonSocial = "" };

        public int NumOrden
        {
            get 
            {
                return numOrden;
            }
            set 
            {
                numOrden = value;
            }
        }
        public EstadoOrden Estado
        {
            get
            {
                return estado;
            }
            set
            {
                estado = value;
            }
        }
        public DateTime FechaInicial
        {
            get
            {
                return fechaInicial;
            }
            set
            {
                fechaInicial = value;
            }
        }
        public DateTime FechaFinal
        {
            get
            {
                return fechaFinal;
            }
            set
            {
                fechaFinal = value;
            }
        }
        public Proveedor Proveedor
        {
            get
            {
                return proveedor;
            }
            set
            {
                proveedor = value;
            }
        }
        public bool FiltrarPorFecha
        {
            get
            {
                return filtrarPorFecha;
            }
            set
            {
                filtrarPorFecha = value;
            }
        }
    }
}
