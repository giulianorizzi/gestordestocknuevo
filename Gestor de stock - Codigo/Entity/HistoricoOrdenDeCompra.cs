﻿/*
 * UNIVERSIDAD PROVINCIAL DE EZEIZA
 * CARRERA: TENICATURA UNIVERSITARIA EN DESARROLLO DE SOFTWARE
 * MATERIA: TECNICAS DE PROGRAMACION (1ER CUAT 2020)
 * TRABAJO PRACTICO INTEGRADOR
 * INTEGRANTES DEL EQUIPO:
 *      -RIZZI GIULIANO
 *      -SEGADE FLAVIO
 *      -LALANNE DARIO
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
	//La clase historicoOrdenDeCompra representa el estado de la orden en un determinado momento
	//Es un estado al cual se le agrega la fecha y el usuario que seteo ese estado
    public class HistoricoOrdenDeCompra:EstadoOrden
    {
		private Usuario usuario;
		private DateTime fecha;

		public DateTime Fecha
		{
			get { return fecha; }
			set { fecha = value; }
		}

		public Usuario Usuario
		{
			get { return usuario; }
			set { usuario = value; }
		}

	}
}
