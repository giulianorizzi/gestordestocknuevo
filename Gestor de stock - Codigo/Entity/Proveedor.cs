﻿/*
 * UNIVERSIDAD PROVINCIAL DE EZEIZA
 * CARRERA: TENICATURA UNIVERSITARIA EN DESARROLLO DE SOFTWARE
 * MATERIA: TECNICAS DE PROGRAMACION (1ER CUAT 2020)
 * TRABAJO PRACTICO INTEGRADOR
 * INTEGRANTES DEL EQUIPO:
 *      -RIZZI GIULIANO
 *      -SEGADE FLAVIO
 *      -LALANNE DARIO
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Proveedor
    {
		private string cuit;
		private string razonSocial;
		private Telefono telefono;
		private Domicilio domicilio;

		public Domicilio Domicilio
		{
			get { return domicilio; }
			set { domicilio = value; }
		}


		public Telefono Telefono
		{
			get { return telefono; }
			set { telefono = value; }
		}


		public string RazonSocial
		{
			get { return razonSocial; }
			set { razonSocial = value; }
		}


		public string Cuit
		{
			get { return cuit; }
			set { cuit = value; }
		}

		public override string ToString()
		{
			return this.RazonSocial;
		}
	}
}
