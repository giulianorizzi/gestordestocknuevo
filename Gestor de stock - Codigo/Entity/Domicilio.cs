﻿/*
 * UNIVERSIDAD PROVINCIAL DE EZEIZA
 * CARRERA: TENICATURA UNIVERSITARIA EN DESARROLLO DE SOFTWARE
 * MATERIA: TECNICAS DE PROGRAMACION (1ER CUAT 2020)
 * TRABAJO PRACTICO INTEGRADOR
 * INTEGRANTES DEL EQUIPO:
 *      -RIZZI GIULIANO
 *      -SEGADE FLAVIO
 *      -LALANNE DARIO
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Domicilio
    {
        private int idDomicilio;
        private Localidad localidad;
        private string calle;
        private int altura = 0;
        private int piso = 0;
        private string depto = "0";

        public int IdDomicilio
        {
            set { this.idDomicilio = value; }
            get { return this.idDomicilio; }
        }

        public string Calle
        {
            set { this.calle = value; }
            get { return this.calle; }
        }

        public int Altura
        {
            set { this.altura = value; }
            get { return this.altura; }
        }

        public int Piso
        {
            set { this.piso = value; }
            get { return this.piso; }
        }
        public string Depto
        {
            set { this.depto = value; }
            get { return this.depto; }
        }
        public Localidad Localidad
        {
            set { this.localidad = value; }
            get { return this.localidad; }
        }
        public override string ToString()
        {
            return this.calle + " " + this.altura + " piso " + this.piso + ", Depto " + this.depto + ", " + this.Localidad.Nombre;
        }
    }


}
