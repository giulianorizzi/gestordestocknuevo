﻿/*
 * UNIVERSIDAD PROVINCIAL DE EZEIZA
 * CARRERA: TENICATURA UNIVERSITARIA EN DESARROLLO DE SOFTWARE
 * MATERIA: TECNICAS DE PROGRAMACION (1ER CUAT 2020)
 * TRABAJO PRACTICO INTEGRADOR
 * INTEGRANTES DEL EQUIPO:
 *      -RIZZI GIULIANO
 *      -SEGADE FLAVIO
 *      -LALANNE DARIO
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Stock
    {
		private int codigo;
		private DetalleOrden detalleOrden;
		private Almacen almacen;
		private int cantidadRecibida;
		private float precio;
		private DateTime fecha;
		private int nroOrden;
		private int cantidadPedida;

		public int ID
		{
			get { return codigo; }
			set { codigo = value; }
		}

		public int CantidadRecibida
		{
			get { return cantidadRecibida; }
			set { cantidadRecibida = value; }
		}

		public int CantidadPedida
		{
			get { return cantidadPedida; }
			set { cantidadPedida = value; }
		}

		public float Precio
		{
			get { return precio; }
			set { precio = value; }
		}

		public Almacen Almacen
		{
			get { return almacen; }
			set { almacen = value; }
		}

		public int NroOrden
		{
			get { return nroOrden; }
			set { nroOrden = value; }
		}

		public DetalleOrden DetalleOrden
		{
			get { return detalleOrden; }
			set { detalleOrden = value; }
		}

		public DateTime Fecha
		{
			get { return fecha; }
			set { fecha = value; }
		}

	}
}
