﻿/*
 * UNIVERSIDAD PROVINCIAL DE EZEIZA
 * CARRERA: TENICATURA UNIVERSITARIA EN DESARROLLO DE SOFTWARE
 * MATERIA: TECNICAS DE PROGRAMACION (1ER CUAT 2020)
 * TRABAJO PRACTICO INTEGRADOR
 * INTEGRANTES DEL EQUIPO:
 *      -RIZZI GIULIANO
 *      -SEGADE FLAVIO
 *      -LALANNE DARIO
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLLGestorDeStock;
using Entity;
namespace GestorDeStock
{
    public partial class formLogin : Form

    {
        public formLogin()
        {
            InitializeComponent();
            this.MaximizeBox = false;
            this.MinimizeBox = false;
        }

        //########## EVENTO AL PRESIONAR EL BOTON INGRESAR #############

        private void btnIngresar_Click(object sender, EventArgs e)
        {
            Usuario usuarioUI = new Usuario();
            int legajo;
            string pass;
            if (txtPass.Text != "" && txtLegajo.Text != "")
            {
                if (int.TryParse(txtLegajo.Text, out legajo))
                {
                    pass = BLLEncriptacion.GetSHA256(txtPass.Text);
                    //ejecuto el login de usuario
                    usuarioUI = BLLUsuario.login(pass, legajo);

                    //Codigo a ejecutar si el login fue exitoso
                    if (usuarioUI != null && usuarioUI.Activo)
                    {

                      
                        formPrincipal ObjFormPrincipal = new formPrincipal(usuarioUI);
                        ObjFormPrincipal.Owner = this;
                        ObjFormPrincipal.Show();
                        this.Hide();
                    }
                    //Codigo a ejecutar si el usuario devuelto es nulo 
                    else if (usuarioUI == null)
                    {
                        MessageBox.Show("Usuario o contraseña incorrecta", "Error al iniciar sesión", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    }
                    //Codigo a ejecutar si el legajo y pass coinciden pero estaba inactivo
                    else if (!usuarioUI.Activo)
                    {
                        MessageBox.Show("El usuario fue desactivado por un administrador", "Error al iniciar sesión", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Debe ingresar un legajo válido", "Inicio de sesión", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                MessageBox.Show("Recuerde completar todos los campos", "Inicio de sesión", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            txtLegajo.Text = "";
            txtPass.Text = "";
            txtLegajo.Focus();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        
        }
        private void formLogin_formClose(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void formLogin_VisibleChanged(object sender, EventArgs e)
        {
                txtLegajo.Focus();
        }
    }
}
