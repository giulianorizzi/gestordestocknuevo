﻿/*
 * UNIVERSIDAD PROVINCIAL DE EZEIZA
 * CARRERA: TENICATURA UNIVERSITARIA EN DESARROLLO DE SOFTWARE
 * MATERIA: TECNICAS DE PROGRAMACION (1ER CUAT 2020)
 * TRABAJO PRACTICO INTEGRADOR
 * INTEGRANTES DEL EQUIPO:
 *      -RIZZI GIULIANO
 *      -SEGADE FLAVIO
 *      -LALANNE DARIO
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLLGestorDeStock;
using Entity;
namespace GestorDeStock
{
    public partial class formHistoricoOrdenDeCompra : Form
    {
        OrdenDeCompra ordenUI;
        public formHistoricoOrdenDeCompra(OrdenDeCompra pOrden)
        {
            InitializeComponent();
            ordenUI = pOrden;
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void formHistoricoOrdenDeCompra_Load(object sender, EventArgs e)
        {
            this.MaximizeBox = false;
            BLLOrdenDeCompra objBLLOrdenDeCompra = new BLLOrdenDeCompra();
            objBLLOrdenDeCompra.NumeroOrden = ordenUI.NumeroOrden;
            txtEstadoOrden.Text = ordenUI.Estado.Descripcion;
            txtFechaAlta.Text = ordenUI.Fecha.ToString();
            txtNroOrden.Text = ordenUI.NumeroOrden.ToString();
            ordenUI.ListaHistoricos = objBLLOrdenDeCompra.consultarHistoricos();
            grillaHistoricoOrdenDeCompra.DataSource = ordenUI.ListaHistoricos;
        }

        private void btnDetalleUsuario_Click(object sender, EventArgs e)
        {
            Object objetoSeleccionado = grillaHistoricoOrdenDeCompra.SelectedRows[0].DataBoundItem;
            HistoricoOrdenDeCompra historicoSeleccionado = (HistoricoOrdenDeCompra)objetoSeleccionado;
            Usuario objUsuario = historicoSeleccionado.Usuario;
            objUsuario.Cuil = BLLEncriptacion.DesEncriptar(objUsuario.Cuil);
            formDetalleUsuario objFormDetalleUsuario = new formDetalleUsuario(objUsuario);
            objFormDetalleUsuario.ShowDialog(this);
        }
    }
}
