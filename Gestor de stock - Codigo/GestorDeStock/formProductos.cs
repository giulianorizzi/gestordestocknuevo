﻿/*
 * UNIVERSIDAD PROVINCIAL DE EZEIZA
 * CARRERA: TENICATURA UNIVERSITARIA EN DESARROLLO DE SOFTWARE
 * MATERIA: TECNICAS DE PROGRAMACION (1ER CUAT 2020)
 * TRABAJO PRACTICO INTEGRADOR
 * INTEGRANTES DEL EQUIPO:
 *      -RIZZI GIULIANO
 *      -SEGADE FLAVIO
 *      -LALANNE DARIO
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLLGestorDeStock;
using Entity;
namespace GestorDeStock
{
    public partial class formProductos : Form
    {

        Entity.Usuario usuarioUI;
        OrdenDeCompra ordenUI;
       
        public formProductos(Usuario pUsuarioUI)
        {
            this.usuarioUI = pUsuarioUI;
            InitializeComponent();
            btnAgregar.Visible = false;
            btnMostrarFormAltaProducto.Visible = true;
            btnMostrarFormModificarProducto.Visible = true;
            btnVerDetalle.Visible = true;
        }

        public formProductos(Usuario pUsuarioUI, OrdenDeCompra pOrden)
        {
            InitializeComponent();
                ordenUI = pOrden;
                btnAgregar.Visible = true;
                btnMostrarFormAltaProducto.Visible = false;
                btnMostrarFormModificarProducto.Visible = false;
                btnVerDetalle.Visible = false;
        }

        private void formProductos_Load(object sender, EventArgs e)
        {
            this.CargarTiposDeProducto();
            this.CargarEstados();
            this.CargarStock();

            if (ordenUI == null)
            {
                this.Listar();
            }
            else
            {
                cbEstadoProducto.SelectedIndex = 1;
                cbEstadoProducto.Enabled = false;
                btnListar.Visible = false;
                buscar();
            }

        }

        private void btnVerDetalle_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection filasSeleccionadas = grillaProductos.SelectedRows;

            if (filasSeleccionadas.Count > 0)
            {
                formProductoDetalle ObjProductoDetalle = new formProductoDetalle((Producto)filasSeleccionadas[0].DataBoundItem);
                ObjProductoDetalle.ShowDialog(this);
            }
            else
            {
                MessageBox.Show("Primero debe seleccionar un producto");
            }
            
        }

        private void btnMostrarFormAltaProducto_Click(object sender, EventArgs e)
        {
            formAltaProducto ObjAltaProducto = new formAltaProducto();
            ObjAltaProducto.ShowDialog(this);
            this.Listar();
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            formPrincipal ObjFormPrincipal = new formPrincipal(usuarioUI);
            this.Close();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            buscar();
        }

        private void buscar()
        {
            BLLProducto objBLLProducto = new BLLProducto();
            //Creo un array de string con los parametros de busqueda
            string[] parametros = new string[4];
            parametros[0] = txtCodigoNombre.Text;
            parametros[1] = cbCategoria.SelectedValue.ToString();
            parametros[2] = cbEstadoProducto.SelectedValue.ToString();
            parametros[3] = cbStock.SelectedValue.ToString();
            //busco
            grillaProductos.DataSource = objBLLProducto.BuscarPorParametros(parametros);
            grillaProductos.Columns["TipoDeProducto"].HeaderText = "Categoria";
            grillaProductos.ClearSelection();
        }
        private void CargarTiposDeProducto()
        {
            BLLTipoProducto objTipoProducto = new BLLTipoProducto();
            cbCategoria.DataSource = objTipoProducto.Listar( new TipoProducto() { Descripcion = "TODOS", Codigo = -1 } );
            cbCategoria.DisplayMember = "Descripcion";
            cbCategoria.ValueMember = "Codigo";
        }

        private void CargarEstados()
        {
            //Combobox del estado
            cbEstadoProducto.DisplayMember = "Texto";
            cbEstadoProducto.ValueMember = "Valor";

            var items = new[] {
                new { Texto = "TODOS" , Valor = "2"},
                new { Texto = "Activo" , Valor = "1"},
                new { Texto = "Inactivo", Valor = "0" }
            };

            cbEstadoProducto.DataSource = items;
        }

        private void CargarStock()
        {
            //Combobox del estado
            cbStock.DisplayMember = "Texto";
            cbStock.ValueMember = "Valor";

            var items = new[] {
                new { Texto = "TODOS" , Valor = "-1"},
                new { Texto = "1" , Valor = "1"},
                new { Texto = "10", Valor = "10" },
                new { Texto = "100", Valor = "100" },
                new { Texto = "1000", Valor = "1000" }
            };

            cbStock.DataSource = items;
        }


        private void btnMostrarFormModificarProducto_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection filasSeleccionadas = grillaProductos.SelectedRows;

            if (filasSeleccionadas.Count > 0)
            {
                formAltaProducto ObjFormAltaProducto = new formAltaProducto((Producto)filasSeleccionadas[0].DataBoundItem);
                ObjFormAltaProducto.ShowDialog(this);
                this.Listar();
            }
            else
            {
                MessageBox.Show("Primero debe seleccionar un producto");
            }
        }


        private void btnAgregar_Click(object sender, EventArgs e)
        {
            List<Producto> productosSelecionados = new List<Producto>();
            Producto prod;
            DataGridViewSelectedRowCollection filasSeleccionadas = grillaProductos.SelectedRows;

            if (filasSeleccionadas.Count > 0)
            {
                for (int i = 0; i < filasSeleccionadas.Count; i++)
                { 
                    prod = (Producto)filasSeleccionadas[i].DataBoundItem;
                    if (!verificarSiEstaEnLaOrden(prod)) { 
                        DetalleOrden unDetalle = new DetalleOrden();
                        unDetalle.Nombre = prod.Nombre;
                        unDetalle.Codigo = prod.Codigo;
                        unDetalle.Descripcion = prod.Descripcion;
                        unDetalle.TipoDeProducto = prod.TipoDeProducto;
                        unDetalle.Activo = prod.Activo;
                        unDetalle.Stock = prod.Stock;
                        ordenUI.DetalleOrden.Add(unDetalle);
                    }
                }
                this.Hide();
            }
            else
            {
                MessageBox.Show("Primero debe seleccionar un producto");
            }
        }

        private void btnListar_Click(object sender, EventArgs e)
        {
            this.Listar();
        }

        public void Listar()
        {
            BLLProducto objBLLProducto = new BLLProducto();
            grillaProductos.DataSource = objBLLProducto.Listar();
            grillaProductos.Columns["TipoDeProducto"].HeaderText = "Categoria";
            grillaProductos.ClearSelection();
            cbCategoria.SelectedIndex = 0;
            cbEstadoProducto.SelectedIndex = 0;
            cbStock.SelectedIndex = 0;
            txtCodigoNombre.Text = "";
        }

        private bool verificarSiEstaEnLaOrden(Producto prod)
        {
            foreach (Producto unProducto in ordenUI.DetalleOrden)
            {
                if(unProducto.Codigo == prod.Codigo)
                {
                    MessageBox.Show("Ya hay un/a " +unProducto.Nombre+ " en la lista, no se va a agregar.");
                    return true;
                }
            }
            return false;
        }


    }
}
