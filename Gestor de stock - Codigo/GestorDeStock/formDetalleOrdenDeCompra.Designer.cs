﻿namespace GestorDeStock
{
    partial class formDetalleOrdenDeCompra
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnVolver = new System.Windows.Forms.Button();
            this.grillaProductoDetalle = new System.Windows.Forms.DataGridView();
            this.lblTipoProducto = new System.Windows.Forms.Label();
            this.cbProveedor = new System.Windows.Forms.ComboBox();
            this.btnAprobar = new System.Windows.Forms.Button();
            this.btnAgregarProductoOC = new System.Windows.Forms.Button();
            this.btnVerHistorico = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.lblEstadoOrden = new System.Windows.Forms.Label();
            this.lblFechaAlta = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblNumOrden = new System.Windows.Forms.Label();
            this.btnEliminar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.grillaProductoDetalle)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnVolver
            // 
            this.btnVolver.BackColor = System.Drawing.Color.Tomato;
            this.btnVolver.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVolver.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVolver.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnVolver.Location = new System.Drawing.Point(576, 390);
            this.btnVolver.Name = "btnVolver";
            this.btnVolver.Size = new System.Drawing.Size(109, 32);
            this.btnVolver.TabIndex = 21;
            this.btnVolver.Text = "Volver";
            this.btnVolver.UseVisualStyleBackColor = false;
            this.btnVolver.Click += new System.EventHandler(this.btnVolver_Click);
            // 
            // grillaProductoDetalle
            // 
            this.grillaProductoDetalle.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grillaProductoDetalle.BackgroundColor = System.Drawing.Color.Silver;
            this.grillaProductoDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grillaProductoDetalle.Location = new System.Drawing.Point(23, 176);
            this.grillaProductoDetalle.MultiSelect = false;
            this.grillaProductoDetalle.Name = "grillaProductoDetalle";
            this.grillaProductoDetalle.ReadOnly = true;
            this.grillaProductoDetalle.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grillaProductoDetalle.Size = new System.Drawing.Size(663, 208);
            this.grillaProductoDetalle.TabIndex = 20;
            this.grillaProductoDetalle.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.grillaProductoDetalle_DataError);
            // 
            // lblTipoProducto
            // 
            this.lblTipoProducto.AutoSize = true;
            this.lblTipoProducto.Location = new System.Drawing.Point(366, 420);
            this.lblTipoProducto.Name = "lblTipoProducto";
            this.lblTipoProducto.Size = new System.Drawing.Size(0, 13);
            this.lblTipoProducto.TabIndex = 19;
            // 
            // cbProveedor
            // 
            this.cbProveedor.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.cbProveedor.Enabled = false;
            this.cbProveedor.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbProveedor.FormattingEnabled = true;
            this.cbProveedor.Location = new System.Drawing.Point(141, 109);
            this.cbProveedor.Name = "cbProveedor";
            this.cbProveedor.Size = new System.Drawing.Size(141, 26);
            this.cbProveedor.TabIndex = 23;
            // 
            // btnAprobar
            // 
            this.btnAprobar.BackColor = System.Drawing.Color.SteelBlue;
            this.btnAprobar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAprobar.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAprobar.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnAprobar.Location = new System.Drawing.Point(342, 84);
            this.btnAprobar.Name = "btnAprobar";
            this.btnAprobar.Size = new System.Drawing.Size(311, 55);
            this.btnAprobar.TabIndex = 24;
            this.btnAprobar.Text = "Aprobar";
            this.btnAprobar.UseVisualStyleBackColor = false;
            this.btnAprobar.Click += new System.EventHandler(this.btnAprobar_Click);
            // 
            // btnAgregarProductoOC
            // 
            this.btnAgregarProductoOC.BackColor = System.Drawing.Color.YellowGreen;
            this.btnAgregarProductoOC.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAgregarProductoOC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAgregarProductoOC.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAgregarProductoOC.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnAgregarProductoOC.Location = new System.Drawing.Point(500, 41);
            this.btnAgregarProductoOC.Name = "btnAgregarProductoOC";
            this.btnAgregarProductoOC.Size = new System.Drawing.Size(153, 30);
            this.btnAgregarProductoOC.TabIndex = 25;
            this.btnAgregarProductoOC.Text = "Agregar Producto";
            this.btnAgregarProductoOC.UseVisualStyleBackColor = false;
            this.btnAgregarProductoOC.Visible = false;
            this.btnAgregarProductoOC.Click += new System.EventHandler(this.btnAgregarProductoOC_Click);
            // 
            // btnVerHistorico
            // 
            this.btnVerHistorico.BackColor = System.Drawing.Color.SteelBlue;
            this.btnVerHistorico.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVerHistorico.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVerHistorico.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnVerHistorico.Location = new System.Drawing.Point(342, 42);
            this.btnVerHistorico.Name = "btnVerHistorico";
            this.btnVerHistorico.Size = new System.Drawing.Size(152, 29);
            this.btnVerHistorico.TabIndex = 26;
            this.btnVerHistorico.Text = "Ver historico";
            this.btnVerHistorico.UseVisualStyleBackColor = false;
            this.btnVerHistorico.Click += new System.EventHandler(this.btnVerHistorico_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LightGray;
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.cbProveedor);
            this.panel1.Controls.Add(this.btnAprobar);
            this.panel1.Controls.Add(this.btnVerHistorico);
            this.panel1.Controls.Add(this.lblEstadoOrden);
            this.panel1.Controls.Add(this.btnAgregarProductoOC);
            this.panel1.Controls.Add(this.lblFechaAlta);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.lblNumOrden);
            this.panel1.Location = new System.Drawing.Point(23, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(663, 152);
            this.panel1.TabIndex = 32;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Gray;
            this.label2.Location = new System.Drawing.Point(6, 109);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(131, 23);
            this.label2.TabIndex = 33;
            this.label2.Text = "Proveedor:";
            // 
            // lblEstadoOrden
            // 
            this.lblEstadoOrden.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstadoOrden.ForeColor = System.Drawing.Color.Tomato;
            this.lblEstadoOrden.Location = new System.Drawing.Point(137, 77);
            this.lblEstadoOrden.Name = "lblEstadoOrden";
            this.lblEstadoOrden.Size = new System.Drawing.Size(145, 23);
            this.lblEstadoOrden.TabIndex = 4;
            this.lblEstadoOrden.Text = "Estado";
            // 
            // lblFechaAlta
            // 
            this.lblFechaAlta.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechaAlta.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lblFechaAlta.Location = new System.Drawing.Point(137, 42);
            this.lblFechaAlta.Name = "lblFechaAlta";
            this.lblFechaAlta.Size = new System.Drawing.Size(145, 23);
            this.lblFechaAlta.TabIndex = 3;
            this.lblFechaAlta.Text = "Fecha";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Gray;
            this.label3.Location = new System.Drawing.Point(4, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(145, 23);
            this.label3.TabIndex = 2;
            this.label3.Text = "Estado actual: ";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Gray;
            this.label4.Location = new System.Drawing.Point(4, 42);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(145, 23);
            this.label4.TabIndex = 1;
            this.label4.Text = "Fecha de alta: ";
            // 
            // lblNumOrden
            // 
            this.lblNumOrden.BackColor = System.Drawing.Color.DodgerBlue;
            this.lblNumOrden.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumOrden.ForeColor = System.Drawing.SystemColors.Control;
            this.lblNumOrden.Location = new System.Drawing.Point(2, 0);
            this.lblNumOrden.Name = "lblNumOrden";
            this.lblNumOrden.Size = new System.Drawing.Size(660, 28);
            this.lblNumOrden.TabIndex = 0;
            this.lblNumOrden.Text = "Detalle de la orden N° ";
            this.lblNumOrden.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnEliminar
            // 
            this.btnEliminar.BackColor = System.Drawing.Color.Tomato;
            this.btnEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminar.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnEliminar.Location = new System.Drawing.Point(23, 390);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(109, 32);
            this.btnEliminar.TabIndex = 34;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.UseVisualStyleBackColor = false;
            this.btnEliminar.Visible = false;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // formDetalleOrdenDeCompra
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(708, 432);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnVolver);
            this.Controls.Add(this.grillaProductoDetalle);
            this.Controls.Add(this.lblTipoProducto);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "formDetalleOrdenDeCompra";
            this.Text = "Detalle de Orden";
            this.Load += new System.EventHandler(this.formDetalleOrdenDeCompra_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grillaProductoDetalle)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnVolver;
        private System.Windows.Forms.DataGridView grillaProductoDetalle;
        private System.Windows.Forms.Label lblTipoProducto;
        private System.Windows.Forms.ComboBox cbProveedor;
        private System.Windows.Forms.Button btnAprobar;
        private System.Windows.Forms.Button btnAgregarProductoOC;
        private System.Windows.Forms.Button btnVerHistorico;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblEstadoOrden;
        private System.Windows.Forms.Label lblFechaAlta;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblNumOrden;
        private System.Windows.Forms.Button btnEliminar;
    }
}