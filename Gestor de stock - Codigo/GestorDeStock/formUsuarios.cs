﻿/*
 * UNIVERSIDAD PROVINCIAL DE EZEIZA
 * CARRERA: TENICATURA UNIVERSITARIA EN DESARROLLO DE SOFTWARE
 * MATERIA: TECNICAS DE PROGRAMACION (1ER CUAT 2020)
 * TRABAJO PRACTICO INTEGRADOR
 * INTEGRANTES DEL EQUIPO:
 *      -RIZZI GIULIANO
 *      -SEGADE FLAVIO
 *      -LALANNE DARIO
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLLGestorDeStock;
using Entity;

namespace GestorDeStock
{
    public partial class formUsuarios : Form
    {
        Usuario usuarioUI;
        formAltaUsuario objFormAltaUsuario;
        public formUsuarios(Usuario pUsuarioUI)
        {
            this.usuarioUI = pUsuarioUI;
            InitializeComponent();
          
        }
        
        private void formUsuarios_Load(object sender, EventArgs e)
        {

            cbTipoUsuario.DataSource = BLLTipoUsuario.listar(new TipoUsuario() { Descripcion = "Todos", Codigo = 0});
            cbEstado.DisplayMember = "Texto";
            cbEstado.ValueMember = "Valor";
            
            var items = new [] {
                new { Texto = "Todos" , Valor = 2},
                new { Texto = "Activo" , Valor = 1},
                new { Texto = "Inactivo", Valor = 0}
            };

            cbEstado.DataSource = items;

            buscarUsuario();
            grillaUsuarios.Select();
        }

        //######### EVENTO AL CLICKEAR EL BOTON DE MODIFICAR USUARIO ###########

        private void btnMostrarFormModificarUsuario_Click(object sender, EventArgs e)
        {
            //Obtengo las filas que se seleccionaron en la grilla de usuarios
            DataGridViewSelectedRowCollection filasSeleccionadas = grillaUsuarios.SelectedRows;
            
            if(filasSeleccionadas.Count > 0)
            {
                //Si hay al menos una seleccionada creo un nuevo formAltaUsuario y le paso el usuario de la fila seleccionada
                objFormAltaUsuario = new formAltaUsuario((Usuario)filasSeleccionadas[0].DataBoundItem);
                //Muestro el formulario
                objFormAltaUsuario.ShowDialog(this);
                buscarUsuario();
            }
            else
            {
                //Si no hay filas seleccionadas muestro mensaje de error
                MessageBox.Show("Debe seleccionar un usuario de la lista", "Error", MessageBoxButtons.OK,MessageBoxIcon.Error);
            }

        }

        //######### EVENTO AL CLICKEAR EL BOTON DE NUEVO USUARIO ###########

        private void btnMostrarFormAltaUsuario_Click(object sender, EventArgs e)
        {
            objFormAltaUsuario = new formAltaUsuario();
            objFormAltaUsuario.ShowDialog(this);
            this.buscarUsuario();
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            buscarUsuario();
        }

        //######### METODO PARA BUSCAR USUARIOS ###########

        private void buscarUsuario()
        {
            FiltrosDeBusquedaDeUsuario filtros = new FiltrosDeBusquedaDeUsuario();
            BLLUsuario objBLLUsuario = new BLLUsuario();
            int legajoBuscado;
            filtros.Estado = (int)cbEstado.SelectedValue;
            filtros.TipoUsuario = (TipoUsuario)cbTipoUsuario.SelectedItem;

            if (int.TryParse(txtBusqueda.Text, out legajoBuscado))
            {
                //Si txtBusqueda contiene un numero entonces seteo el filtro 'legajo' con ese numero
                filtros.Legajo = legajoBuscado;
                filtros.CadenaContenidaEnNombreOApellido = "";
            }
            else
            {
                //si txtBusqueda no contiene un numero seteo el filtro legajo para que busque cualquier legajo
                filtros.Legajo = FiltrosDeBusquedaDeUsuario.TODOS_LOS_LEGAJOS;
                filtros.CadenaContenidaEnNombreOApellido = txtBusqueda.Text;

            }
            //Cargo la grilla de usuarios con los resultados de la busqueda y oculto algunas columnas
            grillaUsuarios.DataSource = objBLLUsuario.buscarUsuario(filtros);
            grillaUsuarios.Columns["Legajo"].DisplayIndex = 0; //Especifico que muestre primero la columna del legajo
            grillaUsuarios.Columns["Cuil"].DisplayIndex = 4; 
            grillaUsuarios.Columns["Pass"].Visible = false; //Oculto la columna pass
            grillaUsuarios.Columns["Domicilio"].Visible = false; //Oculto la columna domicilio
            grillaUsuarios.Columns["TipoUsuario"].HeaderText = "Tipo"; //Cambio el nombre del encabezado de la columna de tipo de usuario
            grillaUsuarios.Columns["idPersona"].Visible = false; //Oculto la columna de idPersona

            //Muestro en un label la cantidad de usuarios encontrados y otros detalles de la busqueda
            lblResultados.Text = "Se encontraron " + grillaUsuarios.Rows.Count + " usuarios con los parametros: \r\nEstado: "
                + cbEstado.Text +
                "\r\nTipo: " + cbTipoUsuario.Text +
                "\r\nLegajo: " + filtros.Legajo + " (Muestra todos los legajos si es 0)" +
                "\r\nNombres que contienen: '" + filtros.CadenaContenidaEnNombreOApellido + "' (Muestra todos los nombres si es '')";
        }

        private void btnDetalle_Click(object sender, EventArgs e)
        {

            DataGridViewSelectedRowCollection filasSeleccionadas = grillaUsuarios.SelectedRows;

            if (filasSeleccionadas.Count > 0)
            {
                formDetalleUsuario objFormDetalleUsuario = new formDetalleUsuario((Usuario)filasSeleccionadas[0].DataBoundItem);
                objFormDetalleUsuario.ShowDialog(this);
            }
        }
    }
}
