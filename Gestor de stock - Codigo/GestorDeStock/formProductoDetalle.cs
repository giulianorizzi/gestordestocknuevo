﻿/*
 * UNIVERSIDAD PROVINCIAL DE EZEIZA
 * CARRERA: TENICATURA UNIVERSITARIA EN DESARROLLO DE SOFTWARE
 * MATERIA: TECNICAS DE PROGRAMACION (1ER CUAT 2020)
 * TRABAJO PRACTICO INTEGRADOR
 * INTEGRANTES DEL EQUIPO:
 *      -RIZZI GIULIANO
 *      -SEGADE FLAVIO
 *      -LALANNE DARIO
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entity;
using BLLGestorDeStock;

namespace GestorDeStock
{
    public partial class formProductoDetalle : Form
    {
        BLLStock objBLLStock = new BLLStock();
        Producto unProducto;
        public formProductoDetalle()
        {
            InitializeComponent();
        }

        public formProductoDetalle(Producto pPoducto)
        {
            this.unProducto = pPoducto;
            InitializeComponent();
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void formProductoDetalle_Load(object sender, EventArgs e)
        {
            lblCodigoProducto.Text = unProducto.Codigo.ToString();
            lblTipoProducto.Text = unProducto.TipoDeProducto.Descripcion;
            lblNombre.Text = unProducto.Nombre;
            lblDescripcion.Text = unProducto.Descripcion;
            this.CargarDetalleProducto();
            this.CargarAlmacenes();
        }

        public void CargarDetalleProducto()
        {
            BLLStock objBLLStock = new BLLStock();
            grillaProductoDetalle.DataSource = objBLLStock.ListarDetalle(unProducto);
            grillaProductoDetalle.Columns["ID"].Visible = false;
            grillaProductoDetalle.Columns["CantidadPedida"].Visible = false;
            grillaProductoDetalle.Columns["CantidadRecibida"].HeaderText = "Cant. recibida";
            grillaProductoDetalle.Columns["NroOrden"].HeaderText = "Nro. de Orden";
            grillaProductoDetalle.Columns["DetalleOrden"].HeaderText = "Nro. de Detalle";
            grillaProductoDetalle.ClearSelection();
        }

        public void CargarAlmacenes()
        {
            BLLAlmacen objBLLAlmacen = new BLLAlmacen();
            cbAlmacen.DataSource = objBLLAlmacen.Listar();
            cbAlmacen.DisplayMember = "Descripcion";
            cbAlmacen.ValueMember = "Codigo";
        }



        private void grillaProductoDetalle_SelectionChanged(object sender, EventArgs e)
        {
            Stock unStock;
            if (grillaProductoDetalle.SelectedRows.Count<=0)
            {
                return;
            }
            else 
            {
                DataGridViewSelectedRowCollection filasSeleccionadas = grillaProductoDetalle.SelectedRows;
                unStock = (Stock)filasSeleccionadas[0].DataBoundItem;
                cbAlmacen.SelectedValue = unStock.Almacen.Codigo;
                objBLLStock.ID = unStock.ID;
            }
            
        }


        private void btnMover_Click(object sender, EventArgs e)
        {

            if (grillaProductoDetalle.SelectedRows.Count > 0)
            {
                objBLLStock.Almacen = (Almacen)cbAlmacen.SelectedItem;
                if (objBLLStock.MoverDeAlmacen())
                {
                    MessageBox.Show("Se ha movido correctamente.");
                    this.CargarDetalleProducto();
                }
                else
                {
                    MessageBox.Show("Hubo un error al mover de almacen.");
                }
            }
            else
            {
                MessageBox.Show("Primero debe seleccionar un producto.");
            }
        }

    }
}
