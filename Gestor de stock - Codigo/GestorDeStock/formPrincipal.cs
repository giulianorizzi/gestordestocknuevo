﻿/*
 * UNIVERSIDAD PROVINCIAL DE EZEIZA
 * CARRERA: TENICATURA UNIVERSITARIA EN DESARROLLO DE SOFTWARE
 * MATERIA: TECNICAS DE PROGRAMACION (1ER CUAT 2020)
 * TRABAJO PRACTICO INTEGRADOR
 * INTEGRANTES DEL EQUIPO:
 *      -RIZZI GIULIANO
 *      -SEGADE FLAVIO
 *      -LALANNE DARIO
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLLGestorDeStock;
namespace GestorDeStock
{
    public partial class formPrincipal : Form
    {
        Entity.Usuario usuarioUI;

        public formPrincipal(Entity.Usuario pUsuarioUI)
        {
            InitializeComponent();
            this.MinimizeBox = false;
            this.MaximizeBox = false;
            this.usuarioUI = pUsuarioUI;
            
        }

        private void productorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            formProductos ObjFormProductos = new formProductos(usuarioUI);
            ObjFormProductos.ShowDialog(this);
        }

        private void usuariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            formUsuarios ObjFormUsuarios = new formUsuarios(usuarioUI);
            ObjFormUsuarios.ShowDialog(this);
        }
        private void btnSalir_Click(object sender, EventArgs e)
        {
                this.Close();  
        }

        private void formPrincipal_Load(object sender, EventArgs e)
        {
            
            lblNombre.Text = this.usuarioUI.Nombre + " " + this.usuarioUI.Apellido + " (" + this.usuarioUI.TipoUsuario.Descripcion + ")";
            lblCuil.Text = this.usuarioUI.Cuil;
            lblLegajo.Text = this.usuarioUI.Legajo.ToString();
            lblTelefono.Text = this.usuarioUI.Telefono.ToString();
            lblDomicilio.Text = this.usuarioUI.Domicilio.ToString();
            //Por cada menu item en nuestros menuitems
            foreach (ToolStripMenuItem item in menuStrip1.Items)
            {
                //Si el tag contiene el tipo de usuario... (Uso contiene porque algunos tags tienen varios tipos de usuarios separados por ;)
                if(item.Tag.ToString().Contains(usuarioUI.TipoUsuario.Descripcion))
                {
                    item.Visible = true;
                }
                else
                {
                    item.Visible = false;
                }
             
            }
           
        }

        private void ordenDeCompraToolStripMenuItem_Click(object sender, EventArgs e)
        {
            formOrdenDeCompra ObjFormOrdenDeCompra = new formOrdenDeCompra(usuarioUI);
            ObjFormOrdenDeCompra.ShowDialog(this);
        }

        private void formPrincipal_FormClosing(Object sender, FormClosingEventArgs e)
        {          
            DialogResult respuesta = MessageBox.Show("¿Esta seguro que desea finalizar su sesión?", "Cerrar sesion", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            
            if(respuesta == DialogResult.Yes)
            {
                this.Owner.Show();  
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void btnCambiarClave_Click(object sender, EventArgs e)
        {
            formModificarPass objFormModificarPass = new formModificarPass(usuarioUI);
            objFormModificarPass.ShowDialog(this);
            Console.WriteLine(BLLEncriptacion.GetSHA256("123"));
        }
    }
}
