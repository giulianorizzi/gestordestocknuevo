﻿/*
 * UNIVERSIDAD PROVINCIAL DE EZEIZA
 * CARRERA: TENICATURA UNIVERSITARIA EN DESARROLLO DE SOFTWARE
 * MATERIA: TECNICAS DE PROGRAMACION (1ER CUAT 2020)
 * TRABAJO PRACTICO INTEGRADOR
 * INTEGRANTES DEL EQUIPO:
 *      -RIZZI GIULIANO
 *      -SEGADE FLAVIO
 *      -LALANNE DARIO
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLLGestorDeStock;
using Entity;

namespace GestorDeStock
{
    
    public partial class formDetalleOrdenDeCompra : Form
    {
        Usuario usuarioUI;
        OrdenDeCompra ordenUI;

        public formDetalleOrdenDeCompra(Usuario pUsuarioUI, OrdenDeCompra pOrdenUI)
        {
            this.usuarioUI = pUsuarioUI;
            this.ordenUI = pOrdenUI;
            InitializeComponent();
        }
        public formDetalleOrdenDeCompra(Usuario pUsuarioUI)
        {
            this.usuarioUI = pUsuarioUI;
            this.ordenUI = new OrdenDeCompra();
            this.ordenUI.Estado = new EstadoOrden() { Codigo = 0, Descripcion = ""};
            this.ordenUI.ListaHistoricos = new List<HistoricoOrdenDeCompra>();
            ordenUI.NumeroOrden = 0;
            InitializeComponent();
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAgregarProductoOC_Click(object sender, EventArgs e)
        {
            formProductos ObjFormAgregarProductoOrdenDeCompra = new formProductos(usuarioUI, ordenUI);
            ObjFormAgregarProductoOrdenDeCompra.ShowDialog();
            BindingSource source = new BindingSource();
            source.DataSource = ordenUI.DetalleOrden;
            grillaProductoDetalle.DataSource = source;
        }

        private void btnVerHistorico_Click(object sender, EventArgs e)
        {
            formHistoricoOrdenDeCompra ObjFormHistoricoOrdenDeCompra = new formHistoricoOrdenDeCompra(ordenUI);
            ObjFormHistoricoOrdenDeCompra.ShowDialog(this);
        }

        private void formDetalleOrdenDeCompra_Load(object sender, EventArgs e)
        {
            this.CargarProveedores();
            btnAprobar.Visible = true;
            this.MaximizeBox = false;
            if (ordenUI.NumeroOrden > 0)
            {
                lblNumOrden.Text = "Detalle de la orden N° " + ordenUI.NumeroOrden.ToString();
                lblEstadoOrden.Text = ordenUI.Estado.Descripcion;
                lblFechaAlta.Text = ordenUI.Fecha.ToString();
                foreach (Proveedor prov in cbProveedor.Items)
                {
                    if (prov.Cuit == ordenUI.Proveedor.Cuit)
                    {
                        cbProveedor.SelectedItem = prov;
                        break;
                    }
                }
            }
            else
            {
                lblNumOrden.Text = "Nueva lista de necesidades";
                lblEstadoOrden.Text = "Nueva lista";
                lblFechaAlta.Text = "---";
                btnVerHistorico.Enabled = false;
            }
            if (this.usuarioUI.TipoUsuario.Descripcion.Equals("Repositor"))
            {

                if (lblEstadoOrden.Text.Equals("Nueva lista") || this.ordenUI.Estado.Descripcion.Equals("Necesidad"))
                {
                    btnEliminar.Visible = true;
                    btnAgregarProductoOC.Visible = true;
                    btnAprobar.Text = "Guardar lista";
                    cbProveedor.Enabled = true;

                }
                else if (this.ordenUI.Estado.Descripcion.Equals("Orden pagada"))
                {
                    grillaProductoDetalle.ReadOnly = false;
                    btnAprobar.Text = "Controlar";
                }
                else
                {
                    btnAprobar.Visible = false;
                }

            }
            else if (this.usuarioUI.TipoUsuario.Descripcion.Equals("Empleado de compras"))
            {
                if (this.ordenUI.Estado.Descripcion.Equals("Necesidad"))
                {
                    cbProveedor.Enabled = true;
                    grillaProductoDetalle.ReadOnly = false;
                    btnAprobar.Text = "Generar";
                }
                else if (this.ordenUI.Estado.Descripcion.Equals("Orden aprobada"))
                {
                    btnAprobar.Text = "Pagar";
                }
                else
                {
                    btnAprobar.Visible = false;
                }

            }
            else if (this.usuarioUI.TipoUsuario.Descripcion.Equals("Gerente de compras"))
            {
                if (this.ordenUI.Estado.Descripcion.Equals("Orden generada"))
                {
                    btnAprobar.Text = "Aprobar";
                }
                else
                {
                    btnAprobar.Visible = false;
                }
            }


            grillaProductoDetalle.DataSource = ordenUI.DetalleOrden;
            grillaProductoDetalle.Columns["Stock"].Visible = false;
            grillaProductoDetalle.Columns["Activo"].Visible = false;
            grillaProductoDetalle.Columns["NroDetalle"].Visible = false;
            grillaProductoDetalle.Columns["Codigo"].DisplayIndex = 0;
            grillaProductoDetalle.Columns["Nombre"].DisplayIndex = 1;
            grillaProductoDetalle.Columns["Descripcion"].DisplayIndex = 2;
            grillaProductoDetalle.Columns["TipoDeProducto"].DisplayIndex = 3;
            grillaProductoDetalle.Columns["TipoDeProducto"].HeaderText = "Categoria";
            grillaProductoDetalle.Columns["CantidadRecibida"].Visible = false;
            grillaProductoDetalle.Columns["CantidadRecibida"].HeaderText = "Cant. recibida";
            grillaProductoDetalle.Columns["CantidadPedida"].HeaderText = "Cant. pedida";



            //Si esta en necesidad y el empleado es el repositor lo oculto, si no no..

            if (ordenUI.Estado.ToString().Equals("Necesidad") && usuarioUI.TipoUsuario.Descripcion.Equals("Repositor") ||
                lblEstadoOrden.Text.Equals("Nueva lista"))
            {
                grillaProductoDetalle.Columns["CantidadPedida"].Visible = false;
                grillaProductoDetalle.Columns["CantidadRecibida"].Visible = false;
                grillaProductoDetalle.Columns["Precio"].Visible = false;
            }
            else if (this.ordenUI.Estado.Descripcion.Equals("Orden pagada")|| this.ordenUI.Estado.Descripcion.Equals("Orden recibida"))
            {
                grillaProductoDetalle.Columns["CantidadRecibida"].Visible = true;
            }
        }

        private void btnAprobar_Click(object sender, EventArgs e)
        {
            Proveedor proveedor = new Proveedor();
            proveedor.Cuit = cbProveedor.SelectedValue.ToString();
            BLLOrdenDeCompra ordenBLL = new BLLOrdenDeCompra
            {
                NumeroOrden = ordenUI.NumeroOrden,
                DetalleOrden = ordenUI.DetalleOrden,
                Proveedor = proveedor
            };
            if (btnAprobar.Text.Equals("Guardar lista"))
            {
                ordenBLL.guardarListaNecesidad(usuarioUI);
                MessageBox.Show("La lista numero " + ordenBLL.NumeroOrden + " fue guardada con exito");
                ordenUI.NumeroOrden = ordenBLL.NumeroOrden;
                Close();
            }
            else if (btnAprobar.Text.Equals("Generar"))
            {
                if (grillaProductoDetalle.Rows.Count > 0)
                    GenerarOrdenDeCompra(ordenBLL);
                else
                    MessageBox.Show("Error. No se puede generar una orden sin productos");
            }
            else if (btnAprobar.Text.Equals("Aprobar"))
            {
                AprobarOrdenDeCompra(ordenBLL);
            }
            else if (btnAprobar.Text.Equals("Pagar"))
            {
                PagarOrdenDeCompra(ordenBLL);
            }
            else if (btnAprobar.Text.Equals("Controlar"))
            {
                ControlarOrdenDeCompra(ordenBLL);
            }
        }

        public void CargarProveedores()
        {
            BLLProveedor objProveedor = new BLLProveedor();
            cbProveedor.
                DataSource = objProveedor.Listar();
            cbProveedor.DisplayMember = "RazonSocial";
            cbProveedor.ValueMember = "Cuit";
        }

        public void GenerarOrdenDeCompra(BLLOrdenDeCompra ordenBLL)
        {
            DetalleOrden det;
            bool error = false;
            List<DetalleOrden> detalles = new List<DetalleOrden>();
            DataGridViewRowCollection filas = grillaProductoDetalle.Rows;
            for (int i = 0; i<filas.Count; i++)
            {
                det = (DetalleOrden)filas[i].DataBoundItem;
                if(det.Precio == 0 || det.CantidadPedida == 0)
                {
                    MessageBox.Show("Por favor complete la cantidad y el precio de todos los detalles.");
                    error = true;
                    break;
                }
                else if (det.Precio < 0 || det.CantidadPedida < 0)
                {
                    MessageBox.Show("Uno o mas valores ingresados son incorrectos.");
                    error = true;
                    break;
                }
                else 
                {
                    detalles.Add(det);
                }
            }
            if (!error)
            {
                ordenBLL.DetalleOrden = detalles;
                if (ordenBLL.GenerarOrden(usuarioUI))
                {
                    MessageBox.Show("La orden de compra numero " +ordenBLL.NumeroOrden+" fue generada con exito");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Hubo un error al generar la orden");
                }
            }
        }

        public void AprobarOrdenDeCompra(BLLOrdenDeCompra ordenBLL)
        {
            if (ordenBLL.AprobarOrden(usuarioUI))
            {
                MessageBox.Show("Orden aprobada con exito");
                this.Close();
            }
            else
            {
                MessageBox.Show("Hubo un error al aprobar la orden");
            }
        }

        public void PagarOrdenDeCompra(BLLOrdenDeCompra ordenBLL)
        {
            if (ordenBLL.PagarOrden(usuarioUI))
            {
                MessageBox.Show("Orden pagada con exito");
                this.Close();
            }
            else
            {
                MessageBox.Show("Hubo un error al pagar la orden");
            }
        }


        public void ControlarOrdenDeCompra(BLLOrdenDeCompra ordenBLL)
        {
            DetalleOrden det;
            bool error = false;
            List<DetalleOrden> detalles = new List<DetalleOrden>();
            DataGridViewRowCollection filas = grillaProductoDetalle.Rows;
            for (int i = 0; i < filas.Count; i++)
            {
                det = (DetalleOrden)filas[i].DataBoundItem;
                if (det.CantidadRecibida<=0)
                {
                    MessageBox.Show("Por favor ingrese datos correctos.");
                    error = true;
                    break;
                }
                else
                {
                    detalles.Add(det);
                }
            }
            if (!error)
            {
                ordenBLL.DetalleOrden = detalles;
                if (ordenBLL.ControlarOrden(usuarioUI))
                {
                    MessageBox.Show("La orden de compra numero " + ordenBLL.NumeroOrden + " fue controlada con exito");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Hubo un error al controlar la orden");
                }
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection filasSeleccionadas = grillaProductoDetalle.SelectedRows;
            BLLDetalleOrden objBLLDetalleOrden;
            DetalleOrden det;
            if (filasSeleccionadas.Count == 1)
            {
                objBLLDetalleOrden = new BLLDetalleOrden();
                det = (DetalleOrden)filasSeleccionadas[0].DataBoundItem;
                if (det != null)
                {
                    for (int i = ordenUI.DetalleOrden.Count - 1; i >= 0; i--)
                    {
                        if (ordenUI.DetalleOrden.ElementAt(i).Equals(det))
                        {
                            ordenUI.DetalleOrden.RemoveAt(i);
                        }
                    }
                    objBLLDetalleOrden.NroDetalle = det.NroDetalle;
                    if (objBLLDetalleOrden.NroDetalle > 0)
                    {
                        if (objBLLDetalleOrden.Eliminar())
                        {
                            MessageBox.Show("Detalle eliminado");
                        }
                        else
                        {
                            MessageBox.Show("Hubo un error al eliminar el detalle");
                        }
                    }
                    BindingSource source = new BindingSource();
                    source.DataSource = ordenUI.DetalleOrden;
                    grillaProductoDetalle.DataSource = source;

                }
            }
            else
            {
                MessageBox.Show("Debe seleccionar un detalle");
            }
        }

        private void grillaProductoDetalle_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.ThrowException = false;

            string txt = "Se produjo un error con el campo " +
                grillaProductoDetalle.Columns[e.ColumnIndex].HeaderText +
                "\n\n" + e.Exception.Message;
            MessageBox.Show(txt, "Error",
                MessageBoxButtons.OK, MessageBoxIcon.Error);

            e.Cancel = false;
        }
    }
}
