﻿/*
 * UNIVERSIDAD PROVINCIAL DE EZEIZA
 * CARRERA: TENICATURA UNIVERSITARIA EN DESARROLLO DE SOFTWARE
 * MATERIA: TECNICAS DE PROGRAMACION (1ER CUAT 2020)
 * TRABAJO PRACTICO INTEGRADOR
 * INTEGRANTES DEL EQUIPO:
 *      -RIZZI GIULIANO
 *      -SEGADE FLAVIO
 *      -LALANNE DARIO
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLLGestorDeStock;

namespace GestorDeStock
{
    public partial class formAltaProducto : Form
    {
        Entity.Producto pProductoUI;
        public formAltaProducto()
        {
            InitializeComponent();
        }

        public formAltaProducto(Entity.Producto pProductoUI)
        {
            InitializeComponent();
            this.pProductoUI = pProductoUI;
            this.Text = "Modificar Producto";

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void formAltaProducto_Load(object sender, EventArgs e)
        {
            this.CargarTiposDeProducto();
            if (pProductoUI != null)
            {
                //Si no esta vacio entonces lo pase por parametro para modificarlo
                this.Text = "Modificar producto";
                CargarEstados();
                CargarProductoAModificar(pProductoUI);
                gbModificar.Visible = true;
                btnAceptar.Text = "Modificar";
            }
            else
            {
                //Si esta vacio entonces es la ventana agregar
                this.Text = "Agregar producto";
                gbModificar.Visible = false;
                btnAceptar.Text = "Guardar";
            }
        }

        private void CargarTiposDeProducto()
        {
            BLLTipoProducto objTipoProducto = new BLLTipoProducto();
            cbTipoDeProducto.DataSource = objTipoProducto.Listar();
            cbTipoDeProducto.DisplayMember = "Descripcion";
            cbTipoDeProducto.ValueMember = "Codigo";
        }

        private void CargarEstados()
        {
            //Combobox del estado
            cbEstadoProducto.DisplayMember = "Texto";
            cbEstadoProducto.ValueMember = "Valor";
            var items = new[] {
                new { Texto = "activo" , Valor = "true"},
                new { Texto = "inactivo", Valor = "false" }
            };
            cbEstadoProducto.DataSource = items;
        }


        private void CargarProductoAModificar(Entity.Producto pProducto)
        {
            txtNombre.Text = pProducto.Nombre;
            txtDescripcion.Text = pProducto.Descripcion;
            foreach (Entity.TipoProducto item in cbTipoDeProducto.Items)
            {
                if (item.Descripcion == pProducto.TipoDeProducto.Descripcion)
                {
                    cbTipoDeProducto.SelectedItem = item;
                }
            }
            cbEstadoProducto.SelectedIndex = (pProducto.Activo) ? 0 : 1;
        }


        private void btnAceptar_Click(object sender, EventArgs e)
        {
            if (txtNombre.Text != "")
            {
                BLLProducto objProducto = new BLLProducto();
                objProducto.Nombre = txtNombre.Text;
                objProducto.Descripcion = txtDescripcion.Text;
                objProducto.TipoDeProducto = (Entity.TipoProducto)cbTipoDeProducto.SelectedItem;
                //Si es el boton aceptar entonces inserto un producto
                if (btnAceptar.Text.Equals("Guardar"))
                {
                    if (objProducto.RegistrarProducto())
                    {
                        MessageBox.Show("Producto agregado correctamente");
                        txtNombre.Text = "";
                        txtDescripcion.Text = "";
                        txtNombre.Focus();
                    }
                    else
                    {
                        MessageBox.Show("Hubo un error en el proceso de alta");
                    }
                }
                //Si no es el boton aceptar entonces es el boton modificar. Modifico un producto
                else
                {
                    objProducto.Codigo = pProductoUI.Codigo;
                    objProducto.Activo = bool.Parse(cbEstadoProducto.SelectedValue.ToString());
                    if (objProducto.ModificarProducto())
                    {
                        MessageBox.Show("Producto modificado correctamente");
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Hubo un error en el proceso de modificacion");
                    }
                }

            }
            else
            {
                MessageBox.Show("Por favor ingrese un nombre");
                txtNombre.Focus();
            }
        }

    }
}
