﻿/*
 * UNIVERSIDAD PROVINCIAL DE EZEIZA
 * CARRERA: TENICATURA UNIVERSITARIA EN DESARROLLO DE SOFTWARE
 * MATERIA: TECNICAS DE PROGRAMACION (1ER CUAT 2020)
 * TRABAJO PRACTICO INTEGRADOR
 * INTEGRANTES DEL EQUIPO:
 *      -RIZZI GIULIANO
 *      -SEGADE FLAVIO
 *      -LALANNE DARIO
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLLGestorDeStock;
using Entity;
namespace GestorDeStock
{
    public partial class formOrdenDeCompra : Form
    {
        Usuario usuarioUI;
        BLLOrdenDeCompra ordenDeCompra = new BLLOrdenDeCompra();
        public formOrdenDeCompra(Usuario pUsuarioUI)
        {
            this.usuarioUI = pUsuarioUI;
            InitializeComponent();
        }

        private void btnVerDetalle_Click(object sender, EventArgs e)
        {
            formDetalleOrdenDeCompra ObjFormDetalleOrdenDeCompra = new formDetalleOrdenDeCompra(usuarioUI, (OrdenDeCompra)grillaOrdenDeCompra.SelectedRows[0].DataBoundItem);
            ObjFormDetalleOrdenDeCompra.ShowDialog(this);
            this.Listar();
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            formPrincipal ObjFormPrincipal = new formPrincipal(usuarioUI);
            this.Close();
        }

        private void formOrdenDeCompra_Load(object sender, EventArgs e)
        {
            this.MaximizeBox = false;
            this.CargarEstadosOrden();
            this.CargarProveedores();
            this.ConfigurarBoton();
            this.Listar();
        }

        public void CargarEstadosOrden()
        {
            BLLEstadoOrden objEstadoOrden = new BLLEstadoOrden();
            cbEstadoOrden.DataSource = objEstadoOrden.Listar(new EstadoOrden() { Codigo = 0, Descripcion = "Todos" });
            cbEstadoOrden.DisplayMember = "Descripcion";
            cbEstadoOrden.ValueMember = "Codigo";
        }

        public void CargarProveedores()
        {
            BLLProveedor objProveedor = new BLLProveedor();
            cbProveedor.DataSource = objProveedor.Listar(new Proveedor() { Cuit = "0", RazonSocial = "Todos" });
            cbProveedor.DisplayMember = "RazonSocial";
            cbProveedor.ValueMember = "Cuit";
        }

        private void btnGenerarNecesidades_Click(object sender, EventArgs e)
        {
            formDetalleOrdenDeCompra ObjFormDetalleOrdenDeCompra = new formDetalleOrdenDeCompra(usuarioUI);
            ObjFormDetalleOrdenDeCompra.ShowDialog(this);
            this.Listar();
        }

        public void ConfigurarBoton()
        {
            if (this.usuarioUI.TipoUsuario.Descripcion.Equals("Repositor"))
            {
                btnGenerarNecesidades.Text = "Generar lista de necesidades";
                btnGenerarNecesidades.Visible = true;
            }
            else
            {
                btnGenerarNecesidades.Visible = false;
            }
        }

        public void Listar()
        {

            BLLOrdenDeCompra objBLLOrdenCompra = new BLLOrdenDeCompra();
            FiltrosBusquedaDeOrdenes filtros = new FiltrosBusquedaDeOrdenes();
            filtros.Estado = (EstadoOrden)cbEstadoOrden.SelectedItem;
            filtros.Proveedor = (Proveedor)cbProveedor.SelectedItem;
            int nroOrden = 0;
            filtros.FechaInicial = calendario.SelectionStart;
            filtros.FechaFinal= calendario.SelectionEnd;
            filtros.FiltrarPorFecha = chkFecha.Checked;
            if (int.TryParse(txtNroOrden.Text, out nroOrden) || String.IsNullOrEmpty(txtNroOrden.Text))
            {
                filtros.NumOrden = nroOrden;
                grillaOrdenDeCompra.DataSource = objBLLOrdenCompra.buscarOrdenesDeCompra(filtros);
            }
            else
            {

                MessageBox.Show("El numero de orden ingresado no es valido", "Error en la búsqueda", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtNroOrden.Text = "";
                txtNroOrden.Focus();
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            Listar();
        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {

        }

        private void chkFecha_CheckedChanged(object sender, EventArgs e)
        {
            if (chkFecha.Checked)
            {
                calendario.Enabled = true;
            }
            else
            {
                calendario.Enabled = false;
            }
        }
    }
}
