﻿namespace GestorDeStock
{
    partial class formHistoricoOrdenDeCompra
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grillaHistoricoOrdenDeCompra = new System.Windows.Forms.DataGridView();
            this.btnVolver = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtNroOrden = new System.Windows.Forms.Label();
            this.txtEstadoOrden = new System.Windows.Forms.Label();
            this.txtFechaAlta = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnDetalleUsuario = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.grillaHistoricoOrdenDeCompra)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grillaHistoricoOrdenDeCompra
            // 
            this.grillaHistoricoOrdenDeCompra.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grillaHistoricoOrdenDeCompra.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grillaHistoricoOrdenDeCompra.Location = new System.Drawing.Point(12, 131);
            this.grillaHistoricoOrdenDeCompra.Name = "grillaHistoricoOrdenDeCompra";
            this.grillaHistoricoOrdenDeCompra.ReadOnly = true;
            this.grillaHistoricoOrdenDeCompra.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grillaHistoricoOrdenDeCompra.Size = new System.Drawing.Size(526, 261);
            this.grillaHistoricoOrdenDeCompra.TabIndex = 29;
           
            // 
            // btnVolver
            // 
            this.btnVolver.BackColor = System.Drawing.Color.Tomato;
            this.btnVolver.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVolver.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVolver.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnVolver.Location = new System.Drawing.Point(439, 398);
            this.btnVolver.Name = "btnVolver";
            this.btnVolver.Size = new System.Drawing.Size(99, 38);
            this.btnVolver.TabIndex = 30;
            this.btnVolver.Text = "Volver";
            this.btnVolver.UseVisualStyleBackColor = false;
            this.btnVolver.Click += new System.EventHandler(this.btnVolver_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LightGray;
            this.panel1.Controls.Add(this.txtNroOrden);
            this.panel1.Controls.Add(this.txtEstadoOrden);
            this.panel1.Controls.Add(this.txtFechaAlta);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(16, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(522, 113);
            this.panel1.TabIndex = 31;
            // 
            // txtNroOrden
            // 
            this.txtNroOrden.BackColor = System.Drawing.Color.DodgerBlue;
            this.txtNroOrden.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNroOrden.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.txtNroOrden.Location = new System.Drawing.Point(420, 2);
            this.txtNroOrden.Name = "txtNroOrden";
            this.txtNroOrden.Size = new System.Drawing.Size(101, 25);
            this.txtNroOrden.TabIndex = 5;
            this.txtNroOrden.Text = "0000";
            // 
            // txtEstadoOrden
            // 
            this.txtEstadoOrden.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEstadoOrden.ForeColor = System.Drawing.Color.Tomato;
            this.txtEstadoOrden.Location = new System.Drawing.Point(137, 77);
            this.txtEstadoOrden.Name = "txtEstadoOrden";
            this.txtEstadoOrden.Size = new System.Drawing.Size(145, 23);
            this.txtEstadoOrden.TabIndex = 4;
            this.txtEstadoOrden.Text = "Estado";
            // 
            // txtFechaAlta
            // 
            this.txtFechaAlta.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFechaAlta.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtFechaAlta.Location = new System.Drawing.Point(137, 42);
            this.txtFechaAlta.Name = "txtFechaAlta";
            this.txtFechaAlta.Size = new System.Drawing.Size(145, 23);
            this.txtFechaAlta.TabIndex = 3;
            this.txtFechaAlta.Text = "Fecha";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Gray;
            this.label3.Location = new System.Drawing.Point(4, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(145, 23);
            this.label3.TabIndex = 2;
            this.label3.Text = "Estado actual: ";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Gray;
            this.label2.Location = new System.Drawing.Point(4, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(145, 23);
            this.label2.TabIndex = 1;
            this.label2.Text = "Fecha de alta: ";
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.DodgerBlue;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.Control;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(519, 28);
            this.label1.TabIndex = 0;
            this.label1.Text = "Registro historico de la orden N° ";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnDetalleUsuario
            // 
            this.btnDetalleUsuario.BackColor = System.Drawing.Color.SteelBlue;
            this.btnDetalleUsuario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDetalleUsuario.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDetalleUsuario.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnDetalleUsuario.Location = new System.Drawing.Point(250, 398);
            this.btnDetalleUsuario.Name = "btnDetalleUsuario";
            this.btnDetalleUsuario.Size = new System.Drawing.Size(161, 38);
            this.btnDetalleUsuario.TabIndex = 32;
            this.btnDetalleUsuario.Text = "Detalle del usuario";
            this.btnDetalleUsuario.UseVisualStyleBackColor = false;
            this.btnDetalleUsuario.Click += new System.EventHandler(this.btnDetalleUsuario_Click);
            // 
            // formHistoricoOrdenDeCompra
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(556, 448);
            this.Controls.Add(this.btnDetalleUsuario);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnVolver);
            this.Controls.Add(this.grillaHistoricoOrdenDeCompra);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "formHistoricoOrdenDeCompra";
            this.Text = "Historico de Orden";
            this.Load += new System.EventHandler(this.formHistoricoOrdenDeCompra_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grillaHistoricoOrdenDeCompra)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView grillaHistoricoOrdenDeCompra;
        private System.Windows.Forms.Button btnVolver;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnDetalleUsuario;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label txtEstadoOrden;
        private System.Windows.Forms.Label txtFechaAlta;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label txtNroOrden;
    }
}