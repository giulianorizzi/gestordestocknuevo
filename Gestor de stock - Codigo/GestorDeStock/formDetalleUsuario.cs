﻿/*
 * UNIVERSIDAD PROVINCIAL DE EZEIZA
 * CARRERA: TENICATURA UNIVERSITARIA EN DESARROLLO DE SOFTWARE
 * MATERIA: TECNICAS DE PROGRAMACION (1ER CUAT 2020)
 * TRABAJO PRACTICO INTEGRADOR
 * INTEGRANTES DEL EQUIPO:
 *      -RIZZI GIULIANO
 *      -SEGADE FLAVIO
 *      -LALANNE DARIO
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entity;
namespace GestorDeStock
{
    public partial class formDetalleUsuario : Form
    {
        Usuario objUsuario;

        public formDetalleUsuario(Usuario pUsuario)
        {
            objUsuario = pUsuario;
            InitializeComponent();
        }

        private void formDetalleUsuario_Load(object sender, EventArgs e)
        {
            //Cargo todos los labels con los datos del usuario
            lblNombre.Text = this.objUsuario.Nombre + " " + this.objUsuario.Apellido + " (" + this.objUsuario.TipoUsuario.Descripcion + ")";
            lblCuil.Text = this.objUsuario.Cuil;
            lblLegajo.Text = this.objUsuario.Legajo.ToString();
            lblTelefono.Text = this.objUsuario.Telefono.ToString();
            lblDomicilio.Text = this.objUsuario.Domicilio.ToString();

            //Asigno el color y texto del lblEstado segun el usuario este activo o inactivo
            if(objUsuario.Activo)
            {
                lblEstado.Text = "Activo";
                lblEstado.ForeColor = Color.FromArgb(0, 192, 0);
                
            }
            else
            {
                lblEstado.Text = "Inactivo";
                lblEstado.ForeColor = Color.FromArgb(192,0, 0);
            }

        }
        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

    }
}
