﻿namespace GestorDeStock
{
    partial class formProductos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grillaProductos = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCodigoNombre = new System.Windows.Forms.TextBox();
            this.cbCategoria = new System.Windows.Forms.ComboBox();
            this.cbEstadoProducto = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnMostrarFormAltaProducto = new System.Windows.Forms.Button();
            this.btnAgregar = new System.Windows.Forms.Button();
            this.btnMostrarFormModificarProducto = new System.Windows.Forms.Button();
            this.btnVerDetalle = new System.Windows.Forms.Button();
            this.btnVolver = new System.Windows.Forms.Button();
            this.fbFiltros = new System.Windows.Forms.GroupBox();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.btnListar = new System.Windows.Forms.Button();
            this.cbStock = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.grillaProductos)).BeginInit();
            this.fbFiltros.SuspendLayout();
            this.SuspendLayout();
            // 
            // grillaProductos
            // 
            this.grillaProductos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grillaProductos.BackgroundColor = System.Drawing.Color.Silver;
            this.grillaProductos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grillaProductos.Location = new System.Drawing.Point(12, 137);
            this.grillaProductos.Name = "grillaProductos";
            this.grillaProductos.ReadOnly = true;
            this.grillaProductos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grillaProductos.Size = new System.Drawing.Size(781, 322);
            this.grillaProductos.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(140, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Buscar por codigo o nombre";
            // 
            // txtCodigoNombre
            // 
            this.txtCodigoNombre.Location = new System.Drawing.Point(9, 35);
            this.txtCodigoNombre.Name = "txtCodigoNombre";
            this.txtCodigoNombre.Size = new System.Drawing.Size(137, 20);
            this.txtCodigoNombre.TabIndex = 8;
            // 
            // cbCategoria
            // 
            this.cbCategoria.FormattingEnabled = true;
            this.cbCategoria.Location = new System.Drawing.Point(9, 61);
            this.cbCategoria.Name = "cbCategoria";
            this.cbCategoria.Size = new System.Drawing.Size(137, 21);
            this.cbCategoria.TabIndex = 16;
            // 
            // cbEstadoProducto
            // 
            this.cbEstadoProducto.FormattingEnabled = true;
            this.cbEstadoProducto.Location = new System.Drawing.Point(164, 61);
            this.cbEstadoProducto.Name = "cbEstadoProducto";
            this.cbEstadoProducto.Size = new System.Drawing.Size(163, 21);
            this.cbEstadoProducto.TabIndex = 18;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(161, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(166, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "Ver productos con Stock menor a";
            // 
            // btnMostrarFormAltaProducto
            // 
            this.btnMostrarFormAltaProducto.BackColor = System.Drawing.Color.YellowGreen;
            this.btnMostrarFormAltaProducto.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnMostrarFormAltaProducto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMostrarFormAltaProducto.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMostrarFormAltaProducto.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnMostrarFormAltaProducto.Location = new System.Drawing.Point(12, 475);
            this.btnMostrarFormAltaProducto.Name = "btnMostrarFormAltaProducto";
            this.btnMostrarFormAltaProducto.Size = new System.Drawing.Size(107, 32);
            this.btnMostrarFormAltaProducto.TabIndex = 24;
            this.btnMostrarFormAltaProducto.Text = "Nuevo";
            this.btnMostrarFormAltaProducto.UseVisualStyleBackColor = false;
            this.btnMostrarFormAltaProducto.Click += new System.EventHandler(this.btnMostrarFormAltaProducto_Click);
            // 
            // btnAgregar
            // 
            this.btnAgregar.BackColor = System.Drawing.Color.YellowGreen;
            this.btnAgregar.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnAgregar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAgregar.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAgregar.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnAgregar.Location = new System.Drawing.Point(568, 474);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(107, 32);
            this.btnAgregar.TabIndex = 25;
            this.btnAgregar.Text = "Agregar";
            this.btnAgregar.UseVisualStyleBackColor = false;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // btnMostrarFormModificarProducto
            // 
            this.btnMostrarFormModificarProducto.BackColor = System.Drawing.Color.SteelBlue;
            this.btnMostrarFormModificarProducto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMostrarFormModificarProducto.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMostrarFormModificarProducto.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnMostrarFormModificarProducto.Location = new System.Drawing.Point(125, 475);
            this.btnMostrarFormModificarProducto.Name = "btnMostrarFormModificarProducto";
            this.btnMostrarFormModificarProducto.Size = new System.Drawing.Size(104, 31);
            this.btnMostrarFormModificarProducto.TabIndex = 26;
            this.btnMostrarFormModificarProducto.Text = "Modificar";
            this.btnMostrarFormModificarProducto.UseVisualStyleBackColor = false;
            this.btnMostrarFormModificarProducto.Click += new System.EventHandler(this.btnMostrarFormModificarProducto_Click);
            // 
            // btnVerDetalle
            // 
            this.btnVerDetalle.BackColor = System.Drawing.Color.SteelBlue;
            this.btnVerDetalle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVerDetalle.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVerDetalle.ForeColor = System.Drawing.SystemColors.Control;
            this.btnVerDetalle.Location = new System.Drawing.Point(235, 475);
            this.btnVerDetalle.Name = "btnVerDetalle";
            this.btnVerDetalle.Size = new System.Drawing.Size(104, 31);
            this.btnVerDetalle.TabIndex = 27;
            this.btnVerDetalle.Text = "Ver detalle";
            this.btnVerDetalle.UseVisualStyleBackColor = false;
            this.btnVerDetalle.Click += new System.EventHandler(this.btnVerDetalle_Click);
            // 
            // btnVolver
            // 
            this.btnVolver.BackColor = System.Drawing.Color.Tomato;
            this.btnVolver.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVolver.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVolver.ForeColor = System.Drawing.SystemColors.Control;
            this.btnVolver.Location = new System.Drawing.Point(681, 465);
            this.btnVolver.Name = "btnVolver";
            this.btnVolver.Size = new System.Drawing.Size(112, 41);
            this.btnVolver.TabIndex = 28;
            this.btnVolver.Text = "Volver";
            this.btnVolver.UseVisualStyleBackColor = false;
            this.btnVolver.Click += new System.EventHandler(this.btnVolver_Click);
            // 
            // fbFiltros
            // 
            this.fbFiltros.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.fbFiltros.Controls.Add(this.cbStock);
            this.fbFiltros.Controls.Add(this.btnBuscar);
            this.fbFiltros.Controls.Add(this.txtCodigoNombre);
            this.fbFiltros.Controls.Add(this.label1);
            this.fbFiltros.Controls.Add(this.cbCategoria);
            this.fbFiltros.Controls.Add(this.cbEstadoProducto);
            this.fbFiltros.Controls.Add(this.label4);
            this.fbFiltros.Location = new System.Drawing.Point(12, 12);
            this.fbFiltros.Name = "fbFiltros";
            this.fbFiltros.Size = new System.Drawing.Size(514, 100);
            this.fbFiltros.TabIndex = 29;
            this.fbFiltros.TabStop = false;
            this.fbFiltros.Text = "Filtros";
            // 
            // btnBuscar
            // 
            this.btnBuscar.BackColor = System.Drawing.Color.SteelBlue;
            this.btnBuscar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscar.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnBuscar.Location = new System.Drawing.Point(352, 35);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(135, 35);
            this.btnBuscar.TabIndex = 30;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = false;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // btnListar
            // 
            this.btnListar.BackColor = System.Drawing.Color.CadetBlue;
            this.btnListar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnListar.Font = new System.Drawing.Font("Microsoft YaHei", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnListar.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnListar.Location = new System.Drawing.Point(718, 108);
            this.btnListar.Name = "btnListar";
            this.btnListar.Size = new System.Drawing.Size(75, 23);
            this.btnListar.TabIndex = 30;
            this.btnListar.Text = "Ver Todos";
            this.btnListar.UseVisualStyleBackColor = false;
            this.btnListar.Click += new System.EventHandler(this.btnListar_Click);
            // 
            // cbStock
            // 
            this.cbStock.FormattingEnabled = true;
            this.cbStock.Location = new System.Drawing.Point(164, 35);
            this.cbStock.Name = "cbStock";
            this.cbStock.Size = new System.Drawing.Size(163, 21);
            this.cbStock.TabIndex = 31;
            // 
            // formProductos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(805, 519);
            this.Controls.Add(this.btnListar);
            this.Controls.Add(this.fbFiltros);
            this.Controls.Add(this.btnVolver);
            this.Controls.Add(this.btnVerDetalle);
            this.Controls.Add(this.btnMostrarFormModificarProducto);
            this.Controls.Add(this.btnAgregar);
            this.Controls.Add(this.btnMostrarFormAltaProducto);
            this.Controls.Add(this.grillaProductos);
            this.Name = "formProductos";
            this.Text = "Productos";
            this.Load += new System.EventHandler(this.formProductos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grillaProductos)).EndInit();
            this.fbFiltros.ResumeLayout(false);
            this.fbFiltros.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView grillaProductos;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCodigoNombre;
        private System.Windows.Forms.ComboBox cbCategoria;
        private System.Windows.Forms.ComboBox cbEstadoProducto;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnMostrarFormAltaProducto;
        private System.Windows.Forms.Button btnAgregar;
        private System.Windows.Forms.Button btnMostrarFormModificarProducto;
        private System.Windows.Forms.Button btnVerDetalle;
        private System.Windows.Forms.Button btnVolver;
        private System.Windows.Forms.GroupBox fbFiltros;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.Button btnListar;
        private System.Windows.Forms.ComboBox cbStock;
    }
}