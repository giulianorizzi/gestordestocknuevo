﻿namespace GestorDeStock
{
    partial class formOrdenDeCompra
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnVerDetalle = new System.Windows.Forms.Button();
            this.cbProveedor = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.grillaOrdenDeCompra = new System.Windows.Forms.DataGridView();
            this.btnVolver = new System.Windows.Forms.Button();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.txtNroOrden = new System.Windows.Forms.TextBox();
            this.calendario = new System.Windows.Forms.MonthCalendar();
            this.chkFecha = new System.Windows.Forms.CheckBox();
            this.cbEstadoOrden = new System.Windows.Forms.ComboBox();
            this.btnGenerarNecesidades = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grillaOrdenDeCompra)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnVerDetalle
            // 
            this.btnVerDetalle.BackColor = System.Drawing.Color.SteelBlue;
            this.btnVerDetalle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVerDetalle.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVerDetalle.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnVerDetalle.Location = new System.Drawing.Point(299, 349);
            this.btnVerDetalle.Name = "btnVerDetalle";
            this.btnVerDetalle.Size = new System.Drawing.Size(120, 43);
            this.btnVerDetalle.TabIndex = 35;
            this.btnVerDetalle.Text = "Ver Detalle";
            this.btnVerDetalle.UseVisualStyleBackColor = false;
            this.btnVerDetalle.Click += new System.EventHandler(this.btnVerDetalle_Click);
            // 
            // cbProveedor
            // 
            this.cbProveedor.FormattingEnabled = true;
            this.cbProveedor.Location = new System.Drawing.Point(14, 141);
            this.cbProveedor.Name = "cbProveedor";
            this.cbProveedor.Size = new System.Drawing.Size(121, 21);
            this.cbProveedor.TabIndex = 32;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.DimGray;
            this.label3.Location = new System.Drawing.Point(11, 120);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 18);
            this.label3.TabIndex = 31;
            this.label3.Text = "Proveedor";
            // 
            // grillaOrdenDeCompra
            // 
            this.grillaOrdenDeCompra.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grillaOrdenDeCompra.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grillaOrdenDeCompra.Location = new System.Drawing.Point(12, 12);
            this.grillaOrdenDeCompra.MultiSelect = false;
            this.grillaOrdenDeCompra.Name = "grillaOrdenDeCompra";
            this.grillaOrdenDeCompra.ReadOnly = true;
            this.grillaOrdenDeCompra.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grillaOrdenDeCompra.Size = new System.Drawing.Size(546, 317);
            this.grillaOrdenDeCompra.TabIndex = 28;
            // 
            // btnVolver
            // 
            this.btnVolver.BackColor = System.Drawing.Color.Tomato;
            this.btnVolver.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVolver.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVolver.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnVolver.Location = new System.Drawing.Point(425, 349);
            this.btnVolver.Name = "btnVolver";
            this.btnVolver.Size = new System.Drawing.Size(133, 43);
            this.btnVolver.TabIndex = 27;
            this.btnVolver.Text = "Volver";
            this.btnVolver.UseVisualStyleBackColor = false;
            this.btnVolver.Click += new System.EventHandler(this.btnVolver_Click);
            // 
            // btnBuscar
            // 
            this.btnBuscar.BackColor = System.Drawing.Color.SteelBlue;
            this.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscar.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnBuscar.Location = new System.Drawing.Point(13, 14);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(263, 43);
            this.btnBuscar.TabIndex = 24;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = false;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // txtNroOrden
            // 
            this.txtNroOrden.Location = new System.Drawing.Point(152, 74);
            this.txtNroOrden.MaxLength = 10;
            this.txtNroOrden.Name = "txtNroOrden";
            this.txtNroOrden.Size = new System.Drawing.Size(121, 20);
            this.txtNroOrden.TabIndex = 22;
            // 
            // calendario
            // 
            this.calendario.Enabled = false;
            this.calendario.Location = new System.Drawing.Point(20, 205);
            this.calendario.MaxDate = new System.DateTime(2050, 6, 20, 0, 0, 0, 0);
            this.calendario.MaxSelectionCount = 30;
            this.calendario.MinDate = new System.DateTime(2020, 1, 1, 0, 0, 0, 0);
            this.calendario.Name = "calendario";
            this.calendario.TabIndex = 36;
            this.calendario.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendar1_DateChanged);
            // 
            // chkFecha
            // 
            this.chkFecha.AutoSize = true;
            this.chkFecha.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkFecha.ForeColor = System.Drawing.Color.DimGray;
            this.chkFecha.Location = new System.Drawing.Point(22, 181);
            this.chkFecha.Name = "chkFecha";
            this.chkFecha.Size = new System.Drawing.Size(140, 22);
            this.chkFecha.TabIndex = 38;
            this.chkFecha.Text = "Filtrar por fecha";
            this.chkFecha.UseVisualStyleBackColor = true;
            this.chkFecha.CheckedChanged += new System.EventHandler(this.chkFecha_CheckedChanged);
            // 
            // cbEstadoOrden
            // 
            this.cbEstadoOrden.FormattingEnabled = true;
            this.cbEstadoOrden.Location = new System.Drawing.Point(155, 141);
            this.cbEstadoOrden.Name = "cbEstadoOrden";
            this.cbEstadoOrden.Size = new System.Drawing.Size(121, 21);
            this.cbEstadoOrden.TabIndex = 41;
            // 
            // btnGenerarNecesidades
            // 
            this.btnGenerarNecesidades.BackColor = System.Drawing.Color.YellowGreen;
            this.btnGenerarNecesidades.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGenerarNecesidades.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenerarNecesidades.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnGenerarNecesidades.Location = new System.Drawing.Point(12, 349);
            this.btnGenerarNecesidades.Name = "btnGenerarNecesidades";
            this.btnGenerarNecesidades.Size = new System.Drawing.Size(240, 43);
            this.btnGenerarNecesidades.TabIndex = 43;
            this.btnGenerarNecesidades.Text = "Generar lista de necesidades";
            this.btnGenerarNecesidades.UseVisualStyleBackColor = false;
            this.btnGenerarNecesidades.Click += new System.EventHandler(this.btnGenerarNecesidades_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LightBlue;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.cbEstadoOrden);
            this.panel1.Controls.Add(this.chkFecha);
            this.panel1.Controls.Add(this.calendario);
            this.panel1.Controls.Add(this.cbProveedor);
            this.panel1.Controls.Add(this.btnBuscar);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.txtNroOrden);
            this.panel1.Location = new System.Drawing.Point(570, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(287, 380);
            this.panel1.TabIndex = 44;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.DimGray;
            this.label1.Location = new System.Drawing.Point(14, 74);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(132, 18);
            this.label1.TabIndex = 43;
            this.label1.Text = "Numero de orden";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.DimGray;
            this.label2.Location = new System.Drawing.Point(152, 120);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 18);
            this.label2.TabIndex = 42;
            this.label2.Text = "Estado";
            // 
            // formOrdenDeCompra
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(869, 411);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnGenerarNecesidades);
            this.Controls.Add(this.btnVerDetalle);
            this.Controls.Add(this.grillaOrdenDeCompra);
            this.Controls.Add(this.btnVolver);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "formOrdenDeCompra";
            this.Text = "Orden de Compra";
            this.Load += new System.EventHandler(this.formOrdenDeCompra_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grillaOrdenDeCompra)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnVerDetalle;
        private System.Windows.Forms.ComboBox cbProveedor;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView grillaOrdenDeCompra;
        private System.Windows.Forms.Button btnVolver;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.TextBox txtNroOrden;
        private System.Windows.Forms.MonthCalendar calendario;
        private System.Windows.Forms.CheckBox chkFecha;
        private System.Windows.Forms.ComboBox cbEstadoOrden;
        private System.Windows.Forms.Button btnGenerarNecesidades;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}