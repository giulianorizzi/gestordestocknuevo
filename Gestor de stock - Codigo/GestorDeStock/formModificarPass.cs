﻿/*
 * UNIVERSIDAD PROVINCIAL DE EZEIZA
 * CARRERA: TENICATURA UNIVERSITARIA EN DESARROLLO DE SOFTWARE
 * MATERIA: TECNICAS DE PROGRAMACION (1ER CUAT 2020)
 * TRABAJO PRACTICO INTEGRADOR
 * INTEGRANTES DEL EQUIPO:
 *      -RIZZI GIULIANO
 *      -SEGADE FLAVIO
 *      -LALANNE DARIO
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLLGestorDeStock;

namespace GestorDeStock
{
    public partial class formModificarPass : Form
    {
        Entity.Usuario usuarioUI = new Entity.Usuario();

        public formModificarPass(Entity.Usuario pUsuarioUI)
        {
            this.usuarioUI = pUsuarioUI;
            InitializeComponent();
        }


        private void btnGuardar_Click(object sender, EventArgs e)
        {
            string pNvaContraseña;
            string pNvaContraseña2;
            string pContraseña;
            BLLUsuario objUsuario = new BLLUsuario();
            objUsuario.Legajo = usuarioUI.Legajo;

            //valido que no haya campos incompletos
            if (String.IsNullOrEmpty(txtConstraseña.Text) || String.IsNullOrEmpty(txtNvaContraseña.Text) || String.IsNullOrEmpty(txtRepiteNvaContraseña.Text))
            {
                MessageBox.Show("Todos los campos deben estar completos.");
            }
            else
            {
                //Encripto las contraseñas ingresadas.
                pNvaContraseña = BLLEncriptacion.GetSHA256(txtNvaContraseña.Text); 
                pNvaContraseña2 = BLLEncriptacion.GetSHA256(txtRepiteNvaContraseña.Text);
                pContraseña = BLLEncriptacion.GetSHA256(txtConstraseña.Text);

                //valida que los datos sean correctos antes de modificar la contraseña actual.
                switch (objUsuario.validarModificacionDeContraseña(pContraseña, pNvaContraseña, pNvaContraseña2, usuarioUI))
                {
                    case 1:
                        MessageBox.Show("La contraseña actual ingresada es incorrecta.");
                        break;

                    case 2:
                        MessageBox.Show("La nueva contraseña debe ser diferente a la actual");
                        txtConstraseña.Clear();
                        txtNvaContraseña.Clear();
                        txtRepiteNvaContraseña.Clear();
                        break;

                    case 3:
                        MessageBox.Show("Las nuevas contraseñas deben ser inguales");
                        txtNvaContraseña.Clear();
                        txtRepiteNvaContraseña.Clear();
                        break;

                    default:
                        if (!objUsuario.modificarContraseña(pNvaContraseña))
                        {
                            MessageBox.Show("Error en intento de modificacion");
                            txtConstraseña.Clear();
                            txtNvaContraseña.Clear();
                            txtRepiteNvaContraseña.Clear();
                        }
                        else
                        {
                            MessageBox.Show("Modificación exitosa");
                            this.Hide();
                        }
                        break;
                }
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void formModificarPass_Load(object sender, EventArgs e)
        {

        }
    }
}
