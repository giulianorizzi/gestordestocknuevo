Se agrego la logica del login
El usuario se puede loguear y ver en la pantalla principal sus datos
-----------------------------
se agrego el script sql para generar las tablas necesarias para el login
-----------------------------
definir previamente la base de datos DBGestorDeStock y 
llenarla con las tablas del script BSGestorDeStockV1.6.sql
-----------------------------
usuario administrador: Legajo: 1000 pass: 123
-----------------------------
Ahora se ocultan los menus segun el tipo de usuario que esta en el lblTipoDeUsuario
del form principal
Pueden probar cambiando Administrador por Repositor, Gerente de compras o Empleado de compras
Faltaria que el lbl se carge solo
-----------------------------
Hecho lo anterior.
Ahora trabajamos con 4 capas
-----------------------------
-Se elimino la clase empleado ahora la herencia es persona -> usuario
-Se cambio en DALusuario la consulta a la base de datos ya que tambien se elimino la entidad
empleado en la misma
-Se agrego la funcionalidad de limpiar los campos de texto "legajo" y "contraseña"
si la contraseña es incorrecta y el focus al campo "legajo"
-----------------------------
Base de datos completa
Modificaciones en la logica del login
Valida que la contraseña y el legajo tengan algo antes de mandarla a la consulta LOGIN
----------------------------
Agregue un script con SOLO los sotred procedures, asi no tenemos que restaurar las demas tablas
a menos que haya un cambio importante, en ese caso avisar antes
Agregue un archivo .gitignore que ignora el seguimiento de carpetas basura
----------------------------
Capa UI:
	-cree una clase en la capa UI llamada "formMoficarPass".
Capa BBL:
	- agregue dos metodos en la BLLUsuario "validarContraseña" y "modificarContraseña".
Capa DAL:
	- En la DALUsuario en el metodo "buscarUsuario" dentro del "foreach (DataRow fila in datos.Rows)", agregue "unUsuario.Pass = fila["pass"].ToString();
  	tambien agregue el metodo "modificarContraseña".
En la BD:
	-Modifique el tamaño "pass varcahar (200)" para que entre completa el pass encriptado en la tabla "Usuario" y en los sp altaUsuario y cambiarContraseña.
Por ultimo, me rescate tarde (en realidad me lo dijo dario) que no esta bueno usar la "ñ", asi que por eso en muchos casos utilice la palabra "contraseña" en vez de "pass".
----------------------------
	