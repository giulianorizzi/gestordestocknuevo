﻿/*
 * UNIVERSIDAD PROVINCIAL DE EZEIZA
 * CARRERA: TENICATURA UNIVERSITARIA EN DESARROLLO DE SOFTWARE
 * MATERIA: TECNICAS DE PROGRAMACION (1ER CUAT 2020)
 * TRABAJO PRACTICO INTEGRADOR
 * INTEGRANTES DEL EQUIPO:
 *      -RIZZI GIULIANO
 *      -SEGADE FLAVIO
 *      -LALANNE DARIO
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity;
using DALGestorDeStock;
namespace BLLGestorDeStock
{
    public class BLLProveedor : Entity.Proveedor
    {
        public List<Entity.Proveedor> Listar(Proveedor pDefault)
        {
            List<Proveedor> listaProveedor = new List<Proveedor>();
            listaProveedor.Add(pDefault);
            listaProveedor.AddRange(new DALProveedor().Listar());
            return listaProveedor;
          
        }

        public List<Entity.Proveedor> Listar()
        {
            return (new DALProveedor()).Listar();
        }
    }
}
