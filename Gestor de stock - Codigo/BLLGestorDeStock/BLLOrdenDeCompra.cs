﻿/*
 * UNIVERSIDAD PROVINCIAL DE EZEIZA
 * CARRERA: TENICATURA UNIVERSITARIA EN DESARROLLO DE SOFTWARE
 * MATERIA: TECNICAS DE PROGRAMACION (1ER CUAT 2020)
 * TRABAJO PRACTICO INTEGRADOR
 * INTEGRANTES DEL EQUIPO:
 *      -RIZZI GIULIANO
 *      -SEGADE FLAVIO
 *      -LALANNE DARIO
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using DALGestorDeStock;
using Entity;

namespace BLLGestorDeStock
{
    public class BLLOrdenDeCompra : Entity.OrdenDeCompra
    {
				
		public void guardarListaNecesidad(Usuario pUsuario)
		{
			DALOrdenDeCompra objDALOrdenDeCompra = new DALOrdenDeCompra();
				if (this.NumeroOrden == 0)
				{
					this.NumeroOrden = objDALOrdenDeCompra.altaOrden();
					HistoricoOrdenDeCompra unHistorico = new HistoricoOrdenDeCompra();
					unHistorico.Usuario = pUsuario;
					unHistorico.Codigo = 1;
					objDALOrdenDeCompra.agregarHistorico(unHistorico,this);
				}
				if (this.NumeroOrden > 0)
				{
					objDALOrdenDeCompra.GuardarProductos(this);
					objDALOrdenDeCompra.ActualizarProveedor(this);
				}
		}
		public List<OrdenDeCompra> buscarOrdenesDeCompra(FiltrosBusquedaDeOrdenes pFiltros)
		{
			DALOrdenDeCompra objDALOrdenDeCompra = new DALOrdenDeCompra();
			return objDALOrdenDeCompra.buscarOrdenesDeCompra(pFiltros);
		}
		public List<HistoricoOrdenDeCompra> consultarHistoricos()
		{
			DALOrdenDeCompra objDALOrdenDeCompra = new DALOrdenDeCompra();
			return objDALOrdenDeCompra.consultarHistoricos(this);
		}

		public bool GenerarOrden(Usuario pUsuario)
		{
			DALOrdenDeCompra objDALOrdenDeCompra = new DALOrdenDeCompra();
			HistoricoOrdenDeCompra unHistorico = new HistoricoOrdenDeCompra();
			unHistorico.Usuario = pUsuario;
			unHistorico.Codigo = 2;
			objDALOrdenDeCompra.agregarHistorico(unHistorico, this);
			if (objDALOrdenDeCompra.GenerarOrden(this)!=0)
			{
				objDALOrdenDeCompra.ActualizarProveedor(this);
				return true;
			}
			return false;
		}

		public bool AprobarOrden(Usuario pUsuario)
		{
			DALOrdenDeCompra objDALOrdenDeCompra = new DALOrdenDeCompra();
			HistoricoOrdenDeCompra unHistorico = new HistoricoOrdenDeCompra();
			unHistorico.Usuario = pUsuario;
			unHistorico.Codigo = 3;
			objDALOrdenDeCompra.agregarHistorico(unHistorico, this);
			if (objDALOrdenDeCompra.AprobarOrden(this) != 0)
			{
				return true;
			}
			return false;
		}

		public bool PagarOrden(Usuario pUsuario)
		{
			DALOrdenDeCompra objDALOrdenDeCompra = new DALOrdenDeCompra();
			HistoricoOrdenDeCompra unHistorico = new HistoricoOrdenDeCompra();
			unHistorico.Usuario = pUsuario;
			unHistorico.Codigo = 4;
			objDALOrdenDeCompra.agregarHistorico(unHistorico, this);
			if (objDALOrdenDeCompra.PagarOrden(this) != 0)
			{
				return true;
			}
			return false;
		}

		public bool ControlarOrden(Usuario pUsuario)
		{
			DALOrdenDeCompra objDALOrdenDeCompra = new DALOrdenDeCompra();
			HistoricoOrdenDeCompra unHistorico = new HistoricoOrdenDeCompra();
			unHistorico.Usuario = pUsuario;
			unHistorico.Codigo = 5;
			objDALOrdenDeCompra.agregarHistorico(unHistorico, this);
			if (objDALOrdenDeCompra.ControlarOrden(this) != 0)
			{
				return true;
			}
			return false;
		}
	}
}