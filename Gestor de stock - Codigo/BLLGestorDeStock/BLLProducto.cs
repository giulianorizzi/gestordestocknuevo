﻿/*
 * UNIVERSIDAD PROVINCIAL DE EZEIZA
 * CARRERA: TENICATURA UNIVERSITARIA EN DESARROLLO DE SOFTWARE
 * MATERIA: TECNICAS DE PROGRAMACION (1ER CUAT 2020)
 * TRABAJO PRACTICO INTEGRADOR
 * INTEGRANTES DEL EQUIPO:
 *      -RIZZI GIULIANO
 *      -SEGADE FLAVIO
 *      -LALANNE DARIO
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DALGestorDeStock;
using Entity;

namespace BLLGestorDeStock
{
    public class BLLProducto : Entity.Producto
    {
		public bool RegistrarProducto()
		{
			DALProducto objDALProducto = new DALProducto();
			if (objDALProducto.Alta( this ) > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public bool ModificarProducto()
		{
			DALProducto objDALProducto = new DALProducto();
			if (objDALProducto.Modificacion(this) > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public List<Producto> BuscarPorParametros(string[] param)
		{
			DALProducto datos = new DALProducto();
			List<Producto> resultado = datos.BuscarPorParametros(param);
			return resultado;
		}

		public List<Producto> Listar()
		{
			DALProducto datos = new DALProducto();
			List<Producto> resultado = datos.Listar();
			return resultado;
		}


	}
	
}
