﻿/*
 * UNIVERSIDAD PROVINCIAL DE EZEIZA
 * CARRERA: TENICATURA UNIVERSITARIA EN DESARROLLO DE SOFTWARE
 * MATERIA: TECNICAS DE PROGRAMACION (1ER CUAT 2020)
 * TRABAJO PRACTICO INTEGRADOR
 * INTEGRANTES DEL EQUIPO:
 *      -RIZZI GIULIANO
 *      -SEGADE FLAVIO
 *      -LALANNE DARIO
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DALGestorDeStock;
using Entity;

namespace BLLGestorDeStock
{
    public class BLLTipoProducto : Entity.TipoProducto
    { 
        public List<Entity.TipoProducto> Listar(TipoProducto pDefault)
        {
            List<Entity.TipoProducto> listaTipoProducto = new List<TipoProducto>();
            listaTipoProducto.Add(pDefault);
            listaTipoProducto.AddRange(new DALTipoProducto().Listar());
            return listaTipoProducto;
        }

        public List<Entity.TipoProducto> Listar()
        {
            return (new DALTipoProducto().Listar());
        }


        
    }
}
