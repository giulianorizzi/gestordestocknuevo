﻿/*
 * UNIVERSIDAD PROVINCIAL DE EZEIZA
 * CARRERA: TENICATURA UNIVERSITARIA EN DESARROLLO DE SOFTWARE
 * MATERIA: TECNICAS DE PROGRAMACION (1ER CUAT 2020)
 * TRABAJO PRACTICO INTEGRADOR
 * INTEGRANTES DEL EQUIPO:
 *      -RIZZI GIULIANO
 *      -SEGADE FLAVIO
 *      -LALANNE DARIO
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DALGestorDeStock;
using Entity;
namespace BLLGestorDeStock
{
    public class BLLUsuario : Entity.Usuario    
    {
        public static Usuario login(string pass, int legajo)
        {
            DALUsuario usuario = new DALUsuario();
            Usuario usuarioBLL = new Usuario();
            usuarioBLL = usuario.login(legajo, pass);
            if(usuarioBLL != null)
            {
                usuarioBLL.Cuil = BLLEncriptacion.DesEncriptar(usuarioBLL.Cuil); //Desencripto el cuil del usuario a mostrar.
            }
            
            return usuarioBLL;
        }
        
        //######### METODO PARA VALIDAR LOS CAMPOS #################
        public int validarUsuario()
        {
            if (!validarNombre())
            {
                return ERR_NOMBRE;
            }
            else if (!validarApellido())
            {
                return ERR_APELLIDO;
            }
            else if (!validarCUIL())
            {
                return ERR_CUIL;
            }
            else if (!validarCodArea())
            {
                return ERR_COD_AREA;
            }
            else if (!validarNumero())
            {
                return ERR_NUM_TEL;
            }
            else if (!validarCalle())
            {
                return ERR_CALLE;
            }
            else if (!validarAltura())
            {
                return ERR_NUM_CALLE;
            }
            else if (!validarPiso())
            {
                return ERR_PISO;
            }
            else if (this.Domicilio.Localidad.Codigo == -1)
            {
                return ERR_LOC;
            }
            else if (this.TipoUsuario.Codigo == -1)
            {
                return ERR_TIPO;
            }
            else
            {
                return OK;
            }
        }
        public int registrarUsuario()
        {
            int validacion = validarUsuario();
            if (validacion == OK) 
            
            {
                DALUsuario objDALUsuario = new DALUsuario();
                this.Pass = BLLEncriptacion.GetSHA256(this.Cuil.ToString()); //asigno el cuil encriptado como pass default (SHA256)
                this.Cuil = BLLEncriptacion.Encriptar(this.Cuil.ToString()); //Encripto el Cuil con algritmo reversible (ToBase64)
                int resultado = objDALUsuario.registrarUsuario(this);
                this.Cuil = BLLEncriptacion.DesEncriptar(this.Cuil.ToString()); //Encripto el Cuil con algritmo reversible (ToBase64)
                if (resultado > 1000)
                {
                    this.Legajo = resultado;
                    
                }
                else
                {
                    return resultado;
                }
                
                return OK;
            }

            else
            {
                return validacion;
            }

        }
        public int modificarUsuario()
        {
            int validacion = validarUsuario();
            if (validacion == OK)

            {
                DALUsuario objDALUsuario = new DALUsuario();
                this.Cuil = BLLEncriptacion.Encriptar(this.Cuil); //Encripto el cuil.
                objDALUsuario.modificarUsuario(this);
                return OK;
            }

            else
            {
                return validacion;
            }
        }
        public List<Usuario> buscarUsuario(FiltrosDeBusquedaDeUsuario pFiltros)
        {
            DALUsuario objDALUsuario = new DALUsuario();
            List<Usuario> listaUsuarios = new List<Usuario>();
            listaUsuarios = objDALUsuario.buscarUsuario(pFiltros);

            foreach(Usuario _usuario in listaUsuarios)
            {
                _usuario.Cuil = BLLEncriptacion.DesEncriptar(_usuario.Cuil); //Desencripta el cuil de cada usuario. 
            }

            return listaUsuarios;
     
        }

        public bool validarNombre()
        {
            if (string.IsNullOrWhiteSpace(this.Nombre))
            {
                return false;
            }
            else if(this.Nombre.Replace(" ", "").Length < 2)
            {
                return false;
            }
            else if (!isAllLetters(this.Nombre))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool validarApellido()
        {
            if (string.IsNullOrWhiteSpace(this.Apellido))
            {
                return false;
            }
            else if (this.Apellido.Replace(" ", "").Length < 2)
            {
                return false;
            }
            else if (!isAllLetters(this.Apellido))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool validarCUIL()
        {
            if (string.IsNullOrWhiteSpace(this.Cuil))
            {
                return false;
            }
            else if (this.Cuil.Length != 11)
            {
                return false;
            }
            else if (!isAllNumbers(this.Cuil))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool validarCodArea()
        {
            if (string.IsNullOrWhiteSpace(this.Telefono.CodArea))
            {
                return false;
            }
            else if (this.Telefono.CodArea.Length > 4)
            {
                return false;
            }
            else if (!isAllNumbers(this.Telefono.CodArea))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool validarNumero()
        {
            if (string.IsNullOrWhiteSpace(this.Telefono.Numero))
            {
                return false;
            }
            else if (this.Telefono.Numero.Length > 10)
            {
                return false;
            }
            else if (this.Telefono.Numero.Length < 6)
            {
                return false;
            }
            else if (!isAllNumbers(this.Telefono.Numero))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool validarCalle()
        {
            if (string.IsNullOrWhiteSpace(this.Domicilio.Calle))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool validarAltura()
        {
            if (this.Domicilio.Altura < 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool validarPiso()
        {
            if (this.Domicilio.Piso < 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private bool isAllLetters(string cadena)
        {
            foreach (char letra in cadena.Replace(" ",""))
            {
                if (!char.IsLetter(letra))
                {
                    return false;
                }
            }
            return true;
        }
        private bool isAllNumbers(string cadena)
        {
            foreach (char letra in cadena)
            {
                if (!char.IsNumber(letra))
                {
                    return false;
                }
            }
            return true;
        }

        public BLLUsuario copiar()
        {
            BLLUsuario copiaBLLUsuario = new BLLUsuario();
            copiaBLLUsuario.Nombre = this.Nombre;
            copiaBLLUsuario.Apellido = this.Apellido;
            copiaBLLUsuario.Cuil = this.Cuil;
            copiaBLLUsuario.Activo = this.Activo;
            copiaBLLUsuario.Domicilio = this.Domicilio;
            copiaBLLUsuario.Telefono = this.Telefono;
            copiaBLLUsuario.TipoUsuario = this.TipoUsuario;
            copiaBLLUsuario.Legajo = this.Legajo;
            copiaBLLUsuario.Pass = this.Pass;

            return copiaBLLUsuario;
        }

        public int validarModificacionDeContraseña(string pContraseñaActual, string pNvacontraseña, string pNvacontraseña2, Usuario pUsuario)
        {

            if (!validarConstraseñaActual(pContraseñaActual, pUsuario))
            {
                return 1;
            }
            
            if (!validarNvaContraseña(pContraseñaActual, pNvacontraseña))
            {
                return 2;
            }

            if (!validarNvasContraseñas(pNvacontraseña, pNvacontraseña2))
            {
                return 3;
            }

            return 4;
        }

        public bool validarConstraseñaActual(string pContraseñaActual, Usuario pUsuario)
        {
            if (pUsuario.Pass != pContraseñaActual)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool validarNvaContraseña(string pContraseña, string pNvaContraseña)
        {
            if(pContraseña == pNvaContraseña)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool validarNvasContraseñas(string pNvaContraseña, string pNvaContraseña2)
        {
            if(pNvaContraseña == pNvaContraseña2)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool modificarContraseña(string pNvaContraseña)
        {
            DALUsuario objUsuario = new DALUsuario();


            if (objUsuario.modificarConstraseña(this, pNvaContraseña))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }



}
