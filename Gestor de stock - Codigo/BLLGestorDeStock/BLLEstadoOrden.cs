﻿/*
 * UNIVERSIDAD PROVINCIAL DE EZEIZA
 * CARRERA: TENICATURA UNIVERSITARIA EN DESARROLLO DE SOFTWARE
 * MATERIA: TECNICAS DE PROGRAMACION (1ER CUAT 2020)
 * TRABAJO PRACTICO INTEGRADOR
 * INTEGRANTES DEL EQUIPO:
 *      -RIZZI GIULIANO
 *      -SEGADE FLAVIO
 *      -LALANNE DARIO
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity;
using DALGestorDeStock;
namespace BLLGestorDeStock
{
    public class BLLEstadoOrden : EstadoOrden
    {
        //Metodo que devuelve una lista de Estados de Orden y recibe un Estado de orden 
        //que se agregara a la lista por defecto
        public List<EstadoOrden> Listar(EstadoOrden pDefault)
        {
            List<EstadoOrden> listaEstadoOrden = new List<EstadoOrden>();
            listaEstadoOrden.Add(pDefault);
            listaEstadoOrden.AddRange(new DALEstadoOrden().Listar());
            return listaEstadoOrden;
        }
        //Metodo que devuelve una lista de Estados de Orden
        public List<EstadoOrden> Listar()
        {
            return (new DALEstadoOrden().Listar());
        }
    }
}
