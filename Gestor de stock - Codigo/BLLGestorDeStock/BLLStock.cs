﻿/*
 * UNIVERSIDAD PROVINCIAL DE EZEIZA
 * CARRERA: TENICATURA UNIVERSITARIA EN DESARROLLO DE SOFTWARE
 * MATERIA: TECNICAS DE PROGRAMACION (1ER CUAT 2020)
 * TRABAJO PRACTICO INTEGRADOR
 * INTEGRANTES DEL EQUIPO:
 *      -RIZZI GIULIANO
 *      -SEGADE FLAVIO
 *      -LALANNE DARIO
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity;
using DALGestorDeStock;

namespace BLLGestorDeStock
{
    public class BLLStock : Stock
    {
		public List<Stock> ListarDetalle(Producto pProducto)
		{
			DALStock datos = new DALStock();
			List<Stock> resultado = datos.ListarDetalle(pProducto);
			return resultado;
		}

		public bool MoverDeAlmacen()
		{
			DALStock datos = new DALStock();
			if (datos.MoverDeAlmacen(this) > 0)
			{
				return true;
			}
			return false;
		}

	}
}
