USE [DBGestorDeStock]
GO
/****** Object:  StoredProcedure [dbo].[sp_modificarUsuario]    Script Date: 20/06/2020 19:07:27 ******/
DROP PROCEDURE [dbo].[sp_modificarUsuario]
GO
/****** Object:  StoredProcedure [dbo].[sp_buscarUsuario]    Script Date: 20/06/2020 19:07:27 ******/
DROP PROCEDURE [dbo].[sp_buscarUsuario]
GO
/****** Object:  StoredProcedure [dbo].[sp_altaUsuario]    Script Date: 20/06/2020 19:07:27 ******/
DROP PROCEDURE [dbo].[sp_altaUsuario]
GO
ALTER TABLE [dbo].[persona] DROP CONSTRAINT [FK__persona__idTelef__5DCAEF64]
GO
ALTER TABLE [dbo].[persona] DROP CONSTRAINT [FK__persona__idDomic__5CD6CB2B]
GO
/****** Object:  Table [dbo].[persona]    Script Date: 20/06/2020 19:07:27 ******/
DROP TABLE [dbo].[persona]
GO
/****** Object:  Table [dbo].[persona]    Script Date: 20/06/2020 19:07:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[persona](
	[idPersona] [int] IDENTITY(1,1) NOT NULL,
	[cuil] [varchar](32) NOT NULL,
	[nombre] [varchar](20) NOT NULL,
	[apellido] [varchar](20) NOT NULL,
	[idDomicilio] [int] NOT NULL,
	[idTelefono] [int] NOT NULL,
 CONSTRAINT [PK_persona_1] PRIMARY KEY CLUSTERED 
(
	[idPersona] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[persona]  WITH CHECK ADD  CONSTRAINT [FK__persona__idDomic__5CD6CB2B] FOREIGN KEY([idDomicilio])
REFERENCES [dbo].[domicilio] ([idDomicilio])
GO
ALTER TABLE [dbo].[persona] CHECK CONSTRAINT [FK__persona__idDomic__5CD6CB2B]
GO
ALTER TABLE [dbo].[persona]  WITH CHECK ADD  CONSTRAINT [FK__persona__idTelef__5DCAEF64] FOREIGN KEY([idTelefono])
REFERENCES [dbo].[telefono] ([idTelefono])
GO
ALTER TABLE [dbo].[persona] CHECK CONSTRAINT [FK__persona__idTelef__5DCAEF64]
GO
/****** Object:  StoredProcedure [dbo].[sp_altaUsuario]    Script Date: 20/06/2020 19:07:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_altaUsuario]

	@pPass varchar(11),
	@pActivo bit,
	@pCodTipo int,
	@pIdPersona int
	
AS
	INSERT INTO usuario(pass,activo,codTipo,idPersona)
	VALUES(@pPass,@pActivo,@pCodTipo,@pIdPersona)

	SELECT SCOPE_IDENTITY()




GO
/****** Object:  StoredProcedure [dbo].[sp_buscarUsuario]    Script Date: 20/06/2020 19:07:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_buscarUsuario]
@pCodTipo INT,
@pLegajo INT,
@pActivo INT,
@pNombre VARCHAR(20)

AS

select * from vista_buscarUsuario

WHERE 1 = 1
AND (@pCodTipo = 0 OR codTipo = @pCodTipo )
AND (@pLegajo = 0 OR idUsuario = @pLegajo)
AND (@pActivo = 2 OR activo = @pActivo)
AND (@pNombre = ''  OR nombreEmpleado LIKE '%' + @pNombre + '%')




GO
/****** Object:  StoredProcedure [dbo].[sp_modificarUsuario]    Script Date: 20/06/2020 19:07:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_modificarUsuario]
@pCodTipo int,
@pActivo BIT,
@pIdPersona INT,
@pCuil varchar(32),
@pNombre varchar(20),
@pApellido varchar(20),
@pIdDomicilio INT,
@pCalle varchar(11),
@pAltura int,
@pPiso int,          
@pDepto varchar(5),
@pCodLocalidad int,
@pIdTelefono INT,
@pCodArea varchar(10),
@pNumero varchar(10),
@pIdUsuario INT
AS

UPDATE usuario


SET 

usuario.codTipo = @pCodTipo,
usuario.activo = @pActivo

WHERE 

usuario.idUsuario = @pIdUsuario

UPDATE persona

SET

persona.nombre = @pNombre,
persona.apellido = @pApellido,
persona.cuil = @pCuil

WHERE

persona.idPersona = @pIdPersona

UPDATE domicilio 

SET

domicilio.calle = @pCalle,
domicilio.altura = @pAltura,
domicilio.piso = @pPiso,
domicilio.depto = @pDepto,
domicilio.codLocalidad = @pCodLocalidad

WHERE

idDomicilio = @pIdDomicilio

UPDATE telefono 

SET

telefono.codArea = @pCodArea,
telefono.numero = @pNumero

WHERE idTelefono = @pIdTelefono;





GO
