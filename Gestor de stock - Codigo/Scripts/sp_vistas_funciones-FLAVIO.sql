USE [DBGestorDeStock ]
GO
/****** Object:  StoredProcedure [dbo].[sp_cambiarContraseña]    Script Date: 13/06/2020 03:03:24 p.m. ******/
DROP PROCEDURE [dbo].[sp_cambiarContraseña]
GO
/****** Object:  StoredProcedure [dbo].[sp_buscarUsuario]    Script Date: 13/06/2020 03:03:24 p.m. ******/
DROP PROCEDURE [dbo].[sp_buscarUsuario]
GO
ALTER TABLE [dbo].[usuario] DROP CONSTRAINT [FK_usuario_persona]
GO
ALTER TABLE [dbo].[usuario] DROP CONSTRAINT [DF__usuario__activo__59FA5E80]
GO
/****** Object:  View [dbo].[vista_buscarUsuario]    Script Date: 13/06/2020 03:03:24 p.m. ******/
DROP VIEW [dbo].[vista_buscarUsuario]
GO
/****** Object:  Table [dbo].[usuario]    Script Date: 13/06/2020 03:03:24 p.m. ******/
DROP TABLE [dbo].[usuario]
GO
/****** Object:  Table [dbo].[usuario]    Script Date: 13/06/2020 03:03:24 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[usuario](
	[idUsuario] [int] IDENTITY(1000,1) NOT NULL,
	[pass] [varchar](200) NOT NULL,
	[activo] [bit] NOT NULL,
	[codTipo] [int] NOT NULL,
	[idPersona] [int] NOT NULL,
 CONSTRAINT [PK__usuario__645723A60BC0A870] PRIMARY KEY CLUSTERED 
(
	[idUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[vista_buscarUsuario]    Script Date: 13/06/2020 03:03:24 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vista_buscarUsuario]

 AS

 SELECT 
		
	u.idUsuario,
	u.activo,
	u.pass,
	u.codTipo,
	tipoUsuario.nombre as descripcionTipoUsuario,
	p.idPersona,
	p.nombre as nombreEmpleado,
	p.apellido,
	p.cuil,
	d.idDomicilio,
	d.calle,
	d.altura,
	d.piso,
	d.depto,
	t.idTelefono,
	t.codArea,
	t.numero,
	l.codLocalidad,
	l.nombreLocalidad,
	pr.codProvincia,
	pr.nombreProvincia
FROM usuario AS u

INNER JOIN tipoUsuario  ON u.codTipo = tipoUsuario.codTipo
INNER JOIN persona AS p ON p.idPersona = u.idPersona
INNER JOIN domicilio AS d ON p.idDomicilio = d.idDomicilio
INNER JOIN telefono as t ON p.idTelefono = t.idTelefono
INNER JOIN localidad as l ON d.codLocalidad = l.codLocalidad
INNER JOIN provincia AS pr ON pr.codProvincia = l.codProvincia



GO
SET IDENTITY_INSERT [dbo].[usuario] ON 

INSERT [dbo].[usuario] ([idUsuario], [pass], [activo], [codTipo], [idPersona]) VALUES (1000, N'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', 1, 1, 1)
INSERT [dbo].[usuario] ([idUsuario], [pass], [activo], [codTipo], [idPersona]) VALUES (1001, N'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', 1, 2, 2)
INSERT [dbo].[usuario] ([idUsuario], [pass], [activo], [codTipo], [idPersona]) VALUES (1002, N'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', 1, 3, 3)
INSERT [dbo].[usuario] ([idUsuario], [pass], [activo], [codTipo], [idPersona]) VALUES (1003, N'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', 1, 4, 4)
INSERT [dbo].[usuario] ([idUsuario], [pass], [activo], [codTipo], [idPersona]) VALUES (1004, N'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', 0, 1, 5)
INSERT [dbo].[usuario] ([idUsuario], [pass], [activo], [codTipo], [idPersona]) VALUES (2004, N'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', 1, 2, 1005)
SET IDENTITY_INSERT [dbo].[usuario] OFF
ALTER TABLE [dbo].[usuario] ADD  CONSTRAINT [DF__usuario__activo__59FA5E80]  DEFAULT ((1)) FOR [activo]
GO
ALTER TABLE [dbo].[usuario]  WITH CHECK ADD  CONSTRAINT [FK_usuario_persona] FOREIGN KEY([idPersona])
REFERENCES [dbo].[persona] ([idPersona])
GO
ALTER TABLE [dbo].[usuario] CHECK CONSTRAINT [FK_usuario_persona]
GO
/****** Object:  StoredProcedure [dbo].[sp_buscarUsuario]    Script Date: 13/06/2020 03:03:24 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_buscarUsuario]
@pCodTipo INT,
@pLegajo INT,
@pActivo INT,
@pNombre VARCHAR(20)

AS

select * from vista_buscarUsuario

WHERE 1 = 1
AND (@pCodTipo = 0 OR codTipo = @pCodTipo )
AND (@pLegajo = 0 OR idUsuario = @pLegajo)
AND (@pActivo = 2 OR activo = @pActivo)
AND (@pNombre = ''  OR nombreEmpleado LIKE '%' + @pNombre + '%')



GO
/****** Object:  StoredProcedure [dbo].[sp_cambiarContraseña]    Script Date: 13/06/2020 03:03:24 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[sp_cambiarContraseña]
		@pLegajo int,
		@pNvaContraseña varchar(200)
as
update usuario
set usuario.pass = @pNvaContraseña
where usuario.idUsuario = @pLegajo


GO
