USE [DBGestorDeStock ]
GO
/****** Object:  StoredProcedure [dbo].[sp_verStock]    Script Date: 14/06/2020 02:35:55 p.m. ******/
DROP PROCEDURE [dbo].[sp_verStock]
GO
/****** Object:  StoredProcedure [dbo].[sp_verProductos]    Script Date: 14/06/2020 02:35:55 p.m. ******/
DROP PROCEDURE [dbo].[sp_verProductos]
GO
/****** Object:  StoredProcedure [dbo].[sp_pagarOrden]    Script Date: 14/06/2020 02:35:55 p.m. ******/
DROP PROCEDURE [dbo].[sp_pagarOrden]
GO
/****** Object:  StoredProcedure [dbo].[sp_moverDeAlmacen]    Script Date: 14/06/2020 02:35:55 p.m. ******/
DROP PROCEDURE [dbo].[sp_moverDeAlmacen]
GO
/****** Object:  StoredProcedure [dbo].[sp_modificacionProducto]    Script Date: 14/06/2020 02:35:55 p.m. ******/
DROP PROCEDURE [dbo].[sp_modificacionProducto]
GO
/****** Object:  StoredProcedure [dbo].[sp_guardarNecesidad]    Script Date: 14/06/2020 02:35:55 p.m. ******/
DROP PROCEDURE [dbo].[sp_guardarNecesidad]
GO
/****** Object:  StoredProcedure [dbo].[sp_bajaDetalle]    Script Date: 14/06/2020 02:35:55 p.m. ******/
DROP PROCEDURE [dbo].[sp_bajaDetalle]
GO
/****** Object:  StoredProcedure [dbo].[sp_aprobarOrden]    Script Date: 14/06/2020 02:35:55 p.m. ******/
DROP PROCEDURE [dbo].[sp_aprobarOrden]
GO
/****** Object:  StoredProcedure [dbo].[sp_altaStock]    Script Date: 14/06/2020 02:35:55 p.m. ******/
DROP PROCEDURE [dbo].[sp_altaStock]
GO
/****** Object:  StoredProcedure [dbo].[sp_altaProducto]    Script Date: 14/06/2020 02:35:55 p.m. ******/
DROP PROCEDURE [dbo].[sp_altaProducto]
GO
/****** Object:  StoredProcedure [dbo].[sp_altaOrden]    Script Date: 14/06/2020 02:35:55 p.m. ******/
DROP PROCEDURE [dbo].[sp_altaOrden]
GO
/****** Object:  StoredProcedure [dbo].[sp_altaNecesidad]    Script Date: 14/06/2020 02:35:55 p.m. ******/
DROP PROCEDURE [dbo].[sp_altaNecesidad]
GO
/****** Object:  StoredProcedure [dbo].[sp_altaHistorico]    Script Date: 14/06/2020 02:35:55 p.m. ******/
DROP PROCEDURE [dbo].[sp_altaHistorico]
GO
/****** Object:  StoredProcedure [dbo].[sp_actualizarProveedorOrden]    Script Date: 14/06/2020 02:35:55 p.m. ******/
DROP PROCEDURE [dbo].[sp_actualizarProveedorOrden]
GO
/****** Object:  View [dbo].[vista_stock]    Script Date: 14/06/2020 02:35:55 p.m. ******/
DROP VIEW [dbo].[vista_stock]
GO
/****** Object:  View [dbo].[vista_productos]    Script Date: 14/06/2020 02:35:55 p.m. ******/
DROP VIEW [dbo].[vista_productos]
GO
/****** Object:  View [dbo].[vista_buscarOrdenDeCompra]    Script Date: 14/06/2020 02:35:55 p.m. ******/
DROP VIEW [dbo].[vista_buscarOrdenDeCompra]
GO
/****** Object:  UserDefinedFunction [dbo].[calcularStock]    Script Date: 14/06/2020 02:35:55 p.m. ******/
DROP FUNCTION [dbo].[calcularStock]
GO
/****** Object:  UserDefinedFunction [dbo].[calcularStock]    Script Date: 14/06/2020 02:35:56 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[calcularStock] (@pCodProducto int)
RETURNS int
AS BEGIN
    DECLARE @cantidad int

    select @cantidad = (select sum(s.cantidadRecibida) 
	from detalleOrden d
	inner join stock s
	on s.nroDetalleOrden = d.nroDetalleOrden
	where d.codProducto = @pCodProducto)

	if @cantidad is null
		set @cantidad = 0
    RETURN @cantidad
END
GO
/****** Object:  View [dbo].[vista_buscarOrdenDeCompra]    Script Date: 14/06/2020 02:35:56 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vista_buscarOrdenDeCompra]

AS 

SELECT 
ordenDeCompra.nroOrden, 
codEstado,
ordenDeCompra.fecha,
estadoOrden.descripcion,
producto.nombre,
producto.codProducto,
producto.codCategoria,
categoriaProducto.nombreCategoria,
detalleOrden.nroDetalleOrden,
detalleOrden.cantidad,
stock.cantidadPedida,
stock.cantidadRecibida,
detalleOrden.precio,
proveedor.razonSocial,
cuitProveedor,
domicilio.idDomicilio,
domicilio.calle,
domicilio.altura,
domicilio.piso,
domicilio.depto,
localidad.codLocalidad,
localidad.nombreLocalidad,
provincia.codProvincia,
provincia.nombreProvincia,
telefono.idTelefono,
telefono.codArea,
telefono.numero

FROM ordenDeCompra

INNER JOIN  estadoOrden ON estadoOrden.codEstado = ordenDeCompra.estado 
LEFT JOIN proveedor   ON proveedor.cuit = ordenDeCompra.cuitProveedor
LEFT JOIN detalleOrden ON detalleOrden.nroOrden = ordenDeCompra.nroOrden 
LEFT JOIN producto ON producto.codProducto = detalleOrden.codProducto
LEFT JOIN categoriaProducto ON categoriaProducto.codCategoria = producto.codCategoria
INNER JOIN domicilio ON proveedor.idDomicilio = domicilio.idDomicilio
INNER JOIN localidad ON localidad.codLocalidad = domicilio.codLocalidad
INNER JOIN provincia ON provincia.codProvincia = localidad.codProvincia
INNER JOIN telefono ON telefono.idTelefono = proveedor.idTelefono
left join stock on stock.nroDetalleOrden = detalleOrden.nroDetalleOrden


GO
/****** Object:  View [dbo].[vista_productos]    Script Date: 14/06/2020 02:35:56 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vista_productos] AS
SELECT p.codProducto as "Codigo", p.nombre as "Nombre", p.descripcion as "Descripcion", p.activo as "Estado" ,
	cp.codCategoria as "codCategoria", cp.nombreCategoria as "Categoria", dbo.calcularStock(p.codProducto) as "Stock"
	from producto p
	inner join categoriaProducto cp
	on p.codCategoria = cp.codCategoria
GO
/****** Object:  View [dbo].[vista_stock]    Script Date: 14/06/2020 02:35:56 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







CREATE VIEW [dbo].[vista_stock] AS
SELECT s.idStock "ID", do.codProducto as "Codigo", s.cantidadRecibida as 'Cantidad recibida', s.cantidadPedida as 'Cantidad pedida',
s.precio as "Precio", a.nroAlmacen as "Codigo Almacen", a.descripcion as "Almacen", do.nroOrden as "Orden", 
do.nroDetalleOrden as "Detalle de Orden", s.fecha as "Fecha"
from stock s
inner join detalleOrden do on s.nroDetalleOrden = do.nroDetalleOrden
inner join almacen a on s.nroAlmacen = a.nroAlmacen
GO
/****** Object:  StoredProcedure [dbo].[sp_actualizarProveedorOrden]    Script Date: 14/06/2020 02:35:56 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_actualizarProveedorOrden]
	@pCuitProveedor  varchar(50),
	@pNroOrden  int
as
	update ordenDeCompra
	set cuitProveedor = @pCuitProveedor
	where nroOrden = @pNroOrden


GO
/****** Object:  StoredProcedure [dbo].[sp_altaHistorico]    Script Date: 14/06/2020 02:35:56 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

  CREATE procedure [dbo].[sp_altaHistorico]
	@pIdUsuario  int,
	@pEstado  int,
	@pNroOrden int
as
  insert into historicoOrdenDeCompra(idUsuario, codEstado, nroOrden, fecha)
  values(@pIdUsuario,@pEstado,@pNroOrden,SYSDATETIME())


GO
/****** Object:  StoredProcedure [dbo].[sp_altaNecesidad]    Script Date: 14/06/2020 02:35:56 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE procedure [dbo].[sp_altaNecesidad]
	--@pFecha nvarchar(50)
	--@pCuit  nvarchar(50)
	
as
	declare @nroOrden int
	INSERT INTO [dbo].[ordenDeCompra]
           ([fecha],
		   estado)
           --,[cuitProveedor])
     VALUES
           (SYSDATETIME(), 1)
		   --,@pCuit)
	SET @nroOrden = SCOPE_IDENTITY()
	select @nroOrden


GO
/****** Object:  StoredProcedure [dbo].[sp_altaOrden]    Script Date: 14/06/2020 02:35:56 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_altaOrden]
	@pNroDetalle  int,
	@pCantidad  int,
	@pPrecio float,
	@pNroOrden int
as
	UPDATE [dbo].[detalleOrden]
	   SET [cantidad] = @pCantidad
		  ,[precio] = @pPrecio
	 WHERE detalleOrden.nroDetalleOrden = @pNroDetalle

	 update ordenDeCompra
	 set estado = 2
	 where nroOrden = @pNroOrden
GO
/****** Object:  StoredProcedure [dbo].[sp_altaProducto]    Script Date: 14/06/2020 02:35:56 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_altaProducto]
	@pNombre  nvarchar(50),
	@pDescripcion  nvarchar(50),
	@pCodCategoria int
as
	INSERT INTO [dbo].[producto]([nombre],[descripcion],[codCategoria])
     VALUES(@pNombre, @pDescripcion, @pCodCategoria)


GO
/****** Object:  StoredProcedure [dbo].[sp_altaStock]    Script Date: 14/06/2020 02:35:56 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE procedure [dbo].[sp_altaStock]
	@pNroDetalle  int,
	@pCantidadRecibida  int,
	@pPrecio float,
	@pNroOrden int
as
	DECLARE @pCantidadPedida int
	set @pCantidadPedida = (SELECT cantidad 
	from detalleOrden
	where detalleOrden.nroDetalleOrden = @pNroDetalle)

	UPDATE [dbo].[detalleOrden]
	   SET [cantidad] = @pCantidadRecibida
		  ,[precio] = @pPrecio
	 WHERE detalleOrden.nroDetalleOrden = @pNroDetalle

	 update ordenDeCompra
	 set estado = 5
	 where nroOrden = @pNroOrden

	INSERT INTO [dbo].[stock]
           ([nroDetalleOrden]
           ,[nroAlmacen]
           ,[cantidadRecibida]
		   ,[cantidadPedida]
           ,[precio]
           ,[fecha])
     VALUES(
           @pNroDetalle
           ,(select nroAlmacen from categoriaProducto
			where codCategoria = (select codCategoria from producto
			where codProducto = (select codProducto from detalleOrden
			where nroDetalleOrden = @pNroDetalle)))
           ,@pCantidadRecibida
		   ,@pCantidadPedida
           ,@pPrecio
           ,SYSDATETIME())
GO
/****** Object:  StoredProcedure [dbo].[sp_aprobarOrden]    Script Date: 14/06/2020 02:35:56 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_aprobarOrden]
	@pNroOrden  int
as
	UPDATE [dbo].[ordenDeCompra]
	SET [estado] = 3
	WHERE nroOrden = @pNroOrden
GO
/****** Object:  StoredProcedure [dbo].[sp_bajaDetalle]    Script Date: 14/06/2020 02:35:56 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_bajaDetalle]
	@pNroDetalle  int
as
delete from detalleOrden
where detalleOrden.nroDetalleOrden = @pNroDetalle
GO
/****** Object:  StoredProcedure [dbo].[sp_guardarNecesidad]    Script Date: 14/06/2020 02:35:56 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 create procedure [dbo].[sp_guardarNecesidad]
	@pCodProducto  int,
	@pNroOrden  int
as
	insert into detalleOrden
	(codProducto, nroOrden)
	values (@pCodProducto, @pNroOrden)
GO
/****** Object:  StoredProcedure [dbo].[sp_modificacionProducto]    Script Date: 14/06/2020 02:35:56 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_modificacionProducto]
	@pCodProducto int,
	@pNombre  nvarchar(50),
	@pDescripcion  nvarchar(50),
	@pActivo bit,
	@pCodCategoria int
	
as
	update [dbo].[producto]
	SET [nombre] = @pNombre
      ,[descripcion] = @pDescripcion
      ,[activo] = @pActivo
      ,[codCategoria] = @pCodCategoria
 WHERE producto.codProducto = @pCodProducto
GO
/****** Object:  StoredProcedure [dbo].[sp_moverDeAlmacen]    Script Date: 14/06/2020 02:35:56 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_moverDeAlmacen]
	@pIDStock  int,
	@pCodAlmacen  int
as
UPDATE [dbo].[stock]
   SET [nroAlmacen] = @pCodAlmacen
WHERE stock.idStock = @pIDStock


GO
/****** Object:  StoredProcedure [dbo].[sp_pagarOrden]    Script Date: 14/06/2020 02:35:56 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[sp_pagarOrden]
	@pNroOrden  int
as
	UPDATE [dbo].[ordenDeCompra]
	SET [estado] = 4
	WHERE nroOrden = @pNroOrden
GO
/****** Object:  StoredProcedure [dbo].[sp_verProductos]    Script Date: 14/06/2020 02:35:56 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[sp_verProductos]
	@pNombreOCodigo  nvarchar(50),
	@pCategoria  nvarchar(50),
	@pEstado nvarchar(50),
	@pStock nvarchar(50)
as
	SELECT * from vista_productos
    where 1=1
	and (Codigo like @pNombreOCodigo or Nombre like @pNombreOCodigo+'%')
	and (@pEstado = 2 or Estado = CAST(@pEstado AS bit))
	and (@pCategoria = -1 or codCategoria = CAST(@pCategoria AS int))
	and (@pStock = -1 or Stock < CAST(@pStock AS int))
GO
/****** Object:  StoredProcedure [dbo].[sp_verStock]    Script Date: 14/06/2020 02:35:56 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



create procedure [dbo].[sp_verStock]
	@pCodigo  nvarchar(50)
as
	SELECT * from vista_stock
    where Codigo = @pCodigo
GO
