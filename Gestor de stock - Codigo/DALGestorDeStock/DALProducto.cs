﻿/*
 * UNIVERSIDAD PROVINCIAL DE EZEIZA
 * CARRERA: TENICATURA UNIVERSITARIA EN DESARROLLO DE SOFTWARE
 * MATERIA: TECNICAS DE PROGRAMACION (1ER CUAT 2020)
 * TRABAJO PRACTICO INTEGRADOR
 * INTEGRANTES DEL EQUIPO:
 *      -RIZZI GIULIANO
 *      -SEGADE FLAVIO
 *      -LALANNE DARIO
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity;

namespace DALGestorDeStock 
{
    public class DALProducto : Entity.Producto
    {
        public int Alta(Entity.Producto pProducto)
        {
            Conexion objConexion = new Conexion();
            SqlParameter[] parametros = new SqlParameter[3];
            parametros[0] = objConexion.crearParametro("@pNombre", pProducto.Nombre);
            parametros[1] = objConexion.crearParametro("@pDescripcion", pProducto.Descripcion);
            parametros[2] = objConexion.crearParametro("@pCodCategoria", pProducto.TipoDeProducto.Codigo);

            return objConexion.EscribirPorStoreProcedure("sp_altaProducto", parametros);
        }

        public int Modificacion(Entity.Producto pProducto)
        {
            Conexion objConexion = new Conexion();
            SqlParameter[] parametros = new SqlParameter[5];
            parametros[0] = objConexion.crearParametro("@pCodProducto", pProducto.Codigo);
            parametros[1] = objConexion.crearParametro("@pNombre", pProducto.Nombre);
            parametros[2] = objConexion.crearParametro("@pDescripcion", pProducto.Descripcion);
            parametros[3] = objConexion.crearParametro("@pActivo", pProducto.Activo);
            parametros[4] = objConexion.crearParametro("@pCodCategoria", pProducto.TipoDeProducto.Codigo);

            return objConexion.EscribirPorStoreProcedure("sp_modificacionProducto", parametros);
        }


        public List<Producto> BuscarPorParametros(string[] param)
        {
            List<Producto> listaProductos = new List<Producto>();
            Conexion objConexion = new Conexion();
            //return objConexion.EscribirPorComando("insert into tProducto (DESCRIPCION, ID_TIPO) values ('" + pDescripcion + "', '" + pTipo + "')");
            objConexion = new Conexion();

            SqlParameter[] parametros = new SqlParameter[4];
            parametros[0] = objConexion.crearParametro("@pNombreOCodigo", param[0]);
            parametros[1] = objConexion.crearParametro("@pCategoria", param[1]);
            parametros[2] = objConexion.crearParametro("@pEstado", param[2]);
            parametros[3] = objConexion.crearParametro("@pStock", param[3]);
            DataTable datos = objConexion.LeerPorStoreProcedure("sp_verProductos", parametros);
            listaProductos = LlenarLista(datos);
            return listaProductos;
        }

        public List<Producto> Listar()
        {
            List<Producto> listaProductos = new List<Producto>();
            Conexion objConexion = new Conexion();
            objConexion = new Conexion();

            DataTable datos = objConexion.LeerPorComando("select * from vista_productos");
            listaProductos = LlenarLista(datos);
            return listaProductos;
        }

        public List<Producto> LlenarLista (DataTable dt)
        {
            Producto unProducto;
            List<Producto> lista = new List<Producto>();
            foreach (DataRow fila in dt.Rows)
            {
                unProducto = new Producto();
                unProducto.Codigo = int.Parse(fila["Codigo"].ToString());
                unProducto.Nombre = fila["Nombre"].ToString();
                unProducto.Descripcion = fila["Descripcion"].ToString();
                unProducto.Activo = bool.Parse(fila["Estado"].ToString());
                unProducto.Stock = int.Parse(fila["Stock"].ToString());
                TipoProducto tipoProducto = new TipoProducto();
                tipoProducto.Codigo = int.Parse(fila["codCategoria"].ToString());
                tipoProducto.Descripcion = fila["Categoria"].ToString();
                unProducto.TipoDeProducto = tipoProducto;
                lista.Add(unProducto);
            }
            return lista;
        }


    }
}
