﻿/*
 * UNIVERSIDAD PROVINCIAL DE EZEIZA
 * CARRERA: TENICATURA UNIVERSITARIA EN DESARROLLO DE SOFTWARE
 * MATERIA: TECNICAS DE PROGRAMACION (1ER CUAT 2020)
 * TRABAJO PRACTICO INTEGRADOR
 * INTEGRANTES DEL EQUIPO:
 *      -RIZZI GIULIANO
 *      -SEGADE FLAVIO
 *      -LALANNE DARIO
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DALGestorDeStock
{
    public class DALProveedor
    {
        public List<Entity.Proveedor> Listar()
        {

            Conexion objCon = new Conexion();
            DataTable dt = objCon.LeerPorComando("select * from proveedor");

            List<Entity.Proveedor> listaDeProveedores = new List<Entity.Proveedor>();
            Entity.Proveedor unProveedor;

            foreach (DataRow fila in dt.Rows)
            {
                unProveedor = new Entity.Proveedor();

                unProveedor.Cuit = fila["cuit"].ToString();
                unProveedor.RazonSocial = fila["razonSocial"].ToString();


                listaDeProveedores.Add(unProveedor);
            }

            return listaDeProveedores;
        }
    }
}
