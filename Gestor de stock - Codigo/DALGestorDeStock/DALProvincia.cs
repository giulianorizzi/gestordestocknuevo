﻿/*
 * UNIVERSIDAD PROVINCIAL DE EZEIZA
 * CARRERA: TENICATURA UNIVERSITARIA EN DESARROLLO DE SOFTWARE
 * MATERIA: TECNICAS DE PROGRAMACION (1ER CUAT 2020)
 * TRABAJO PRACTICO INTEGRADOR
 * INTEGRANTES DEL EQUIPO:
 *      -RIZZI GIULIANO
 *      -SEGADE FLAVIO
 *      -LALANNE DARIO
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity;
namespace DALGestorDeStock
{
    public class DALProvincia : Provincia
    {
        public static List<Provincia> listar()
        {
            List<Provincia> listadoProvincias = new List<Provincia>();
          
            Conexion objConexion = new Conexion();
            string consulta = @"SELECT 
                                p.codProvincia, 
                                p.nombreProvincia, 
                                l.codLocalidad, 
                                l.nombreLocalidad 
                                FROM 
                                provincia as p, 
                                localidad as l 
                                WHERE 
                                p.codProvincia = l.codProvincia ";
            DataTable datosProvinciasLocalidades = objConexion.LeerPorComando(consulta);

            int codProvincia = -1;
            int codProvinciaActual;
            Provincia unaProvincia = null;
            Localidad unaLocalidad = null;
            unaProvincia = new Provincia();
            unaProvincia.Nombre = "Seleccionar";
            unaProvincia.Codigo = codProvincia;

            listadoProvincias.Add(unaProvincia);
            foreach (DataRow fila in datosProvinciasLocalidades.Rows)
            {
                codProvinciaActual = int.Parse(fila["codProvincia"].ToString());
                if (codProvinciaActual != codProvincia)
                {
                    unaProvincia = new Provincia();
                    unaProvincia.Codigo = codProvinciaActual;
                    unaProvincia.Nombre = fila["nombreProvincia"].ToString();
                    listadoProvincias.Add(unaProvincia);
                    codProvincia = codProvinciaActual;
                    unaLocalidad = new Localidad();
                    unaLocalidad.Nombre = "Seleccionar";
                    unaLocalidad.Codigo = -1;
                    unaProvincia.Localidades.Add(unaLocalidad);
                }
                else
                {
                    unaLocalidad = new Localidad();
                    unaLocalidad.Codigo = int.Parse(fila["codLocalidad"].ToString());
                    unaLocalidad.Nombre = fila["nombreLocalidad"].ToString();
                    unaProvincia.Localidades.Add(unaLocalidad);
                }
            }
            return listadoProvincias;
        }


    }
}
