﻿/*
 * UNIVERSIDAD PROVINCIAL DE EZEIZA
 * CARRERA: TENICATURA UNIVERSITARIA EN DESARROLLO DE SOFTWARE
 * MATERIA: TECNICAS DE PROGRAMACION (1ER CUAT 2020)
 * TRABAJO PRACTICO INTEGRADOR
 * INTEGRANTES DEL EQUIPO:
 *      -RIZZI GIULIANO
 *      -SEGADE FLAVIO
 *      -LALANNE DARIO
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity;

namespace DALGestorDeStock
{
    public class DALAlmacen : Almacen
    {
        public List<Almacen> Listar()
        {

            Conexion objCon = new Conexion();
            DataTable dt = objCon.LeerPorComando("select * from almacen");

            List<Almacen> listaDeAlmacenes = new List<Almacen>();
            Almacen unAlmacen;

            foreach (DataRow fila in dt.Rows)
            {
                unAlmacen = new Almacen();

                unAlmacen.Codigo = int.Parse(fila["nroAlmacen"].ToString());
                unAlmacen.Descripcion = fila["descripcion"].ToString();


                listaDeAlmacenes.Add(unAlmacen);
            }

            return listaDeAlmacenes;
        }
    }
}
