﻿/*
 * UNIVERSIDAD PROVINCIAL DE EZEIZA
 * CARRERA: TENICATURA UNIVERSITARIA EN DESARROLLO DE SOFTWARE
 * MATERIA: TECNICAS DE PROGRAMACION (1ER CUAT 2020)
 * TRABAJO PRACTICO INTEGRADOR
 * INTEGRANTES DEL EQUIPO:
 *      -RIZZI GIULIANO
 *      -SEGADE FLAVIO
 *      -LALANNE DARIO
 */

using Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DALGestorDeStock
{
    public class DALOrdenDeCompra
    {
        public int altaOrden()
        {
            Conexion objConexion = new Conexion();
            DataTable resultado = objConexion.LeerPorStoreProcedure("sp_altaNecesidad");
            int numOrden = int.Parse(resultado.Rows[0][0].ToString());
            return numOrden;
        }

        public int GuardarProductos(OrdenDeCompra pOrden)
        {
            int res = 0;
            Conexion objConexion = new Conexion();
            SqlParameter[] parametros = new SqlParameter[2];
                foreach (DetalleOrden detalle in pOrden.DetalleOrden)
                {
                    if(detalle.NroDetalle == 0)
                    {
                        parametros[0] = objConexion.crearParametro("@pCodProducto", detalle.Codigo);
                        parametros[1] = objConexion.crearParametro("@pNroOrden", pOrden.NumeroOrden);
                        res = objConexion.EscribirPorStoreProcedure("sp_guardarNecesidad", parametros);
                    }

                }
            return 1;
        }

        public int ActualizarProveedor(OrdenDeCompra pOrden)
        {
            int res = 0;
            Conexion objConexion = new Conexion();
            SqlParameter[] parametros = new SqlParameter[2];
            parametros[0] = objConexion.crearParametro("@pCuitProveedor", pOrden.Proveedor.Cuit);
            parametros[1] = objConexion.crearParametro("@pNroOrden", pOrden.NumeroOrden);
            return objConexion.EscribirPorStoreProcedure("sp_actualizarProveedorOrden", parametros);
        }


        public List<OrdenDeCompra> buscarOrdenesDeCompra(FiltrosBusquedaDeOrdenes pFiltros)
        {
            List<OrdenDeCompra> listaOrdenes = new List<OrdenDeCompra>();
            Conexion objConexion = new Conexion();
            SqlParameter[] parametros = new SqlParameter[6];
            parametros[0] = objConexion.crearParametro("@pNroOrden", pFiltros.NumOrden);
            parametros[1] = objConexion.crearParametro("@pCodEstado", pFiltros.Estado.Codigo);
            parametros[2] = objConexion.crearParametro("@pCuitProveedor", pFiltros.Proveedor.Cuit);
            parametros[3] = objConexion.crearParametro("@pFechaInicial", pFiltros.FechaInicial);
            parametros[4] = objConexion.crearParametro("@pFechaFinal", pFiltros.FechaFinal);
            parametros[5] = objConexion.crearParametro("@pFiltrarPorFecha", pFiltros.FiltrarPorFecha);
            DataTable datos = objConexion.LeerPorStoreProcedure("sp_buscarOrdenDeCompra",parametros);
            OrdenDeCompra ordenActual = null;
            int nroDetalle, cant, cantRec;
            float prec;
            foreach (DataRow fila in datos.Rows)
            {
                if(ordenActual == null || ordenActual.NumeroOrden != int.Parse(fila["nroOrden"].ToString()))
                {
                    ordenActual = new OrdenDeCompra();
                    ordenActual.NumeroOrden = int.Parse(fila["nroOrden"].ToString());
                    ordenActual.Fecha = DateTime.Parse(fila["fecha"].ToString());
                    EstadoOrden unEstado = new EstadoOrden();
                    unEstado.Codigo = int.Parse(fila["codEstado"].ToString());
                    unEstado.Descripcion = fila["descripcion"].ToString();
                    ordenActual.Estado = unEstado;
                    Proveedor unProveedor = new Proveedor();
                    unProveedor.Cuit = fila["cuitProveedor"].ToString();
                    unProveedor.RazonSocial = fila["razonSocial"].ToString();
                    ordenActual.Proveedor = unProveedor;
                    ordenActual.DetalleOrden = new List<DetalleOrden>();
                    listaOrdenes.Add(ordenActual);
                }
                
                if(int.TryParse(fila["nroDetalleOrden"].ToString(), out nroDetalle)){
                    DetalleOrden unDetalle = new DetalleOrden();
                    unDetalle.NroDetalle = nroDetalle;
                    unDetalle.Codigo = int.Parse(fila["codProducto"].ToString());
                    unDetalle.Nombre = fila["nombre"].ToString();
                    unDetalle.Descripcion = fila["desc prod"].ToString();
                    TipoProducto unTipoProducto = new TipoProducto();
                    unTipoProducto.Codigo = int.Parse(fila["codCategoria"].ToString());
                    unTipoProducto.Descripcion = fila["nombreCategoria"].ToString();
                    unDetalle.TipoDeProducto = unTipoProducto;
                    cant = 0;
                    int.TryParse(fila["cantidadPedida"].ToString(), out cant);
                    if (cant == 0)
                    {
                        int.TryParse(fila["cantidad"].ToString(), out cant);
                    }
                    unDetalle.CantidadPedida = cant;
                    cantRec = 0;
                    int.TryParse(fila["cantidadRecibida"].ToString(), out cantRec);
                    unDetalle.CantidadRecibida = cantRec;
                    prec = 0;
                    float.TryParse(fila["precio"].ToString(), out prec);
                    unDetalle.Precio = prec;
                    ordenActual.DetalleOrden.Add(unDetalle);
                }

            }

            return listaOrdenes;
        }

        public List<HistoricoOrdenDeCompra> consultarHistoricos(OrdenDeCompra pOrden)
        {
            List<HistoricoOrdenDeCompra> listaHistoricos = new List<HistoricoOrdenDeCompra>();
            Conexion objConexion = new Conexion();
            SqlParameter[] parametros = new SqlParameter[1];
            parametros[0] = objConexion.crearParametro("@pNumOrden", pOrden.NumeroOrden);
            DataTable datos = objConexion.LeerPorStoreProcedure("sp_consultarHistoricoOrdenCompra",parametros);
            foreach (DataRow fila in datos.Rows)
            {
                HistoricoOrdenDeCompra unHistorico = new HistoricoOrdenDeCompra();
                unHistorico.Fecha = DateTime.Parse(fila["fecha"].ToString());
                unHistorico.Codigo = int.Parse(fila["codEstado"].ToString());
                unHistorico.Descripcion = fila["descripcionEstadoOrden"].ToString();
                int legajoUsuario = int.Parse(fila["idUsuario"].ToString());
                DALUsuario usuarioBuscado = new DALUsuario();
                unHistorico.Usuario = usuarioBuscado.buscarUsuario(new FiltrosDeBusquedaDeUsuario
                {
                    CadenaContenidaEnNombreOApellido = "",
                    Estado = FiltrosDeBusquedaDeUsuario.TODOS_LOS_ESTADOS,
                    TipoUsuario = new TipoUsuario()
                    {
                        Codigo = FiltrosDeBusquedaDeUsuario.TODOS_LOS_TIPOS
                    },
                    Legajo = legajoUsuario
                })[0];
                listaHistoricos.Add(unHistorico);
            }
                return listaHistoricos;
        }

        public int agregarHistorico(HistoricoOrdenDeCompra pHistorico,OrdenDeCompra pOrden)
        {
            Conexion objConexion = new Conexion();
            SqlParameter[] parametros = new SqlParameter[3];
   
                parametros[0] = objConexion.crearParametro("@pIdUsuario", pHistorico.Usuario.Legajo);
                parametros[1] = objConexion.crearParametro("@pEstado", pHistorico.Codigo);
                parametros[2] = objConexion.crearParametro("@pNroOrden", pOrden.NumeroOrden);

                objConexion.EscribirPorStoreProcedure("sp_altaHistorico", parametros);
            return 1;
        }


        public int GenerarOrden(OrdenDeCompra pOrden)
        {
            Conexion objConexion = new Conexion();
            SqlParameter[] parametros = new SqlParameter[4];
            int res = 0;

            foreach (DetalleOrden detalle in pOrden.DetalleOrden)
            {
                parametros[0] = objConexion.crearParametro("@pNroDetalle", detalle.NroDetalle);
                parametros[1] = objConexion.crearParametro("@pCantidad", detalle.CantidadPedida);
                parametros[2] = objConexion.crearParametro("@pPrecio", detalle.Precio);
                parametros[3] = objConexion.crearParametro("@pNroOrden", pOrden.NumeroOrden);
                res = objConexion.EscribirPorStoreProcedure("sp_altaOrden", parametros);
            }
            return res;
        }

        public int AprobarOrden(OrdenDeCompra pOrden)
        {
            Conexion objConexion = new Conexion();
            SqlParameter[] parametros = new SqlParameter[1];
            parametros[0] = objConexion.crearParametro("@pNroOrden", pOrden.NumeroOrden);
            return objConexion.EscribirPorStoreProcedure("sp_aprobarOrden", parametros);
        }

        public int PagarOrden(OrdenDeCompra pOrden)
        {
            Conexion objConexion = new Conexion();
            SqlParameter[] parametros = new SqlParameter[1];
            parametros[0] = objConexion.crearParametro("@pNroOrden", pOrden.NumeroOrden);
            return objConexion.EscribirPorStoreProcedure("sp_pagarOrden", parametros);
        }


        public int ControlarOrden(OrdenDeCompra pOrden)
        {
            Conexion objConexion = new Conexion();
            SqlParameter[] parametros = new SqlParameter[3];
            int res = 0;

            foreach (DetalleOrden detalle in pOrden.DetalleOrden)
            {
                parametros[0] = objConexion.crearParametro("@pNroDetalle", detalle.NroDetalle);
                parametros[1] = objConexion.crearParametro("@pCantidadRecibida", detalle.CantidadRecibida);
                parametros[2] = objConexion.crearParametro("@pNroOrden", pOrden.NumeroOrden);
                res = objConexion.EscribirPorStoreProcedure("sp_altaStock", parametros);
            }
            return res;
        }
    }
}
