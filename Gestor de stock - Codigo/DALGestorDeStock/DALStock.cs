﻿/*
 * UNIVERSIDAD PROVINCIAL DE EZEIZA
 * CARRERA: TENICATURA UNIVERSITARIA EN DESARROLLO DE SOFTWARE
 * MATERIA: TECNICAS DE PROGRAMACION (1ER CUAT 2020)
 * TRABAJO PRACTICO INTEGRADOR
 * INTEGRANTES DEL EQUIPO:
 *      -RIZZI GIULIANO
 *      -SEGADE FLAVIO
 *      -LALANNE DARIO
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity;

namespace DALGestorDeStock
{
    public class DALStock : Stock
    {
        public List<Stock> ListarDetalle(Producto pProducto)
        {
            List<Stock> listaStock = new List<Stock>();
            Conexion objConexion = new Conexion();
            objConexion = new Conexion();
            SqlParameter[] parametros = new SqlParameter[1];
            parametros[0] = objConexion.crearParametro("@pCodigo", pProducto.Codigo);
            DataTable datos = objConexion.LeerPorStoreProcedure("sp_verStock", parametros);
            listaStock = LlenarLista(datos);
            return listaStock;
        }

        public List<Stock> LlenarLista(DataTable dt)
        {
            Stock unStock;
            int cantPed, cantRec;
            List<Stock> lista = new List<Stock>();
            foreach (DataRow fila in dt.Rows)
            {
                unStock = new Stock();
                unStock.ID = int.Parse(fila["ID"].ToString());
                cantRec = 0;
                int.TryParse(fila["Cantidad recibida"].ToString(), out cantRec);
                unStock.CantidadRecibida = cantRec;
                cantPed = 0;
                int.TryParse(fila["Cantidad pedida"].ToString(), out cantPed);
                unStock.CantidadPedida = cantPed;
                unStock.Precio = float.Parse(fila["Precio"].ToString());
                unStock.Fecha = DateTime.Parse(fila["Fecha"].ToString());
                Almacen unAlmacen = new Almacen();
                unAlmacen.Codigo = int.Parse(fila["Codigo Almacen"].ToString());
                unAlmacen.Descripcion = fila["Almacen"].ToString();
                unStock.Almacen = unAlmacen;
                DetalleOrden unDetalleOrden = new DetalleOrden();
                unDetalleOrden.NroDetalle = int.Parse(fila["Detalle de Orden"].ToString());
                unStock.DetalleOrden = unDetalleOrden;
                unStock.NroOrden = int.Parse(fila["Orden"].ToString());
                lista.Add(unStock);
            }
            return lista;
        }

        public int MoverDeAlmacen(Stock pStock)
        {
            Conexion objConexion = new Conexion();
            SqlParameter[] parametros = new SqlParameter[2];
            parametros[0] = objConexion.crearParametro("@pIDStock", pStock.ID);
            parametros[1] = objConexion.crearParametro("@pCodAlmacen", pStock.Almacen.Codigo);

            return objConexion.EscribirPorStoreProcedure("sp_moverDeAlmacen", parametros);
        }
    }
}
