﻿/*
 * UNIVERSIDAD PROVINCIAL DE EZEIZA
 * CARRERA: TENICATURA UNIVERSITARIA EN DESARROLLO DE SOFTWARE
 * MATERIA: TECNICAS DE PROGRAMACION (1ER CUAT 2020)
 * TRABAJO PRACTICO INTEGRADOR
 * INTEGRANTES DEL EQUIPO:
 *      -RIZZI GIULIANO
 *      -SEGADE FLAVIO
 *      -LALANNE DARIO
 */

using Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DALGestorDeStock
{
   public class DALUsuario
    {
        //########### METODO PARA LOGUEARSE #############

        public Usuario login(int legajo, string pass)
        {
            try
            {
                Conexion conexion = new Conexion();
                string consulta = "SELECT * FROM usuario WHERE idUsuario = " + legajo + " AND pass = '" + pass + "'";
                DataTable datos = conexion.LeerPorComando(consulta);

                //si la consulta arroja resultados el pass y idUsuario son correctos
                if (datos.Rows.Count == 1)
                {
                    FiltrosDeBusquedaDeUsuario filtros = new FiltrosDeBusquedaDeUsuario();
                    //Busco el usuario con el legajo especificado y lo devuelvo
                    filtros.Legajo = legajo;
                    return buscarUsuario(filtros)[0];
                }
                else
                {
                    //Si el pass o usuario es incorrecto devuelvo un Usuario nulo
                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        //########### METODO PARA REGISTRAR UN NUEVO USUARIO #############

        public int registrarUsuario(Usuario pUsuario)
        {
            Conexion conexion = new Conexion();
            try
            {
                //Verifico si existe un usuario con el mismo CUIL
                string consulta = "SELECT cuil FROM persona WHERE cuil = '" + pUsuario.Cuil + "'";
                DataTable datos = conexion.LeerPorComando(consulta);

                if (datos.Rows.Count > 0)
                {
                    //Si la consulta no fue vacia es porque existe, devuelvo el error de cuil repetido
                    return Usuario.ERR_CUIL_REPETIDO;
                }
                else
                {
                    //Si la verificacion de CUIL fue exitosa guardo los datos en las tablas

                    //Cargo los parametros para el stored procedure
                    SqlParameter[] paramSp = new SqlParameter[13];
                    paramSp[0] = conexion.crearParametro("@pCalle", pUsuario.Domicilio.Calle);
                    paramSp[1] = conexion.crearParametro("@pAltura", pUsuario.Domicilio.Altura);
                    paramSp[2] = conexion.crearParametro("@pPiso", pUsuario.Domicilio.Piso);
                    paramSp[3] = conexion.crearParametro("@pDepto", pUsuario.Domicilio.Depto);
                    paramSp[4] = conexion.crearParametro("@pCodLocalidad", pUsuario.Domicilio.Localidad.Codigo);
                    paramSp[5] = conexion.crearParametro("@pCodArea", pUsuario.Telefono.CodArea);
                    paramSp[6] = conexion.crearParametro("@pNumero", pUsuario.Telefono.Numero);
                    paramSp[7] = conexion.crearParametro("@pNombre", pUsuario.Nombre);
                    paramSp[8] = conexion.crearParametro("@pApellido", pUsuario.Apellido);
                    paramSp[9] = conexion.crearParametro("@pCuil", pUsuario.Cuil);
                    paramSp[10] = conexion.crearParametro("@pPass", pUsuario.Pass);
                    paramSp[11] = conexion.crearParametro("@pActivo", 1);
                    paramSp[12] = conexion.crearParametro("@pCodTipo", pUsuario.TipoUsuario.Codigo);
                    //Ejecuto el store procedure
                    datos = conexion.LeerPorStoreProcedure("sp_registrarUsuario", paramSp);
                    //Obtengo el idUsuario del nuevo usuario
                    pUsuario.Legajo = int.Parse(datos.Rows[0][0].ToString());

                    //Devuelvo el legajo del nuevo usuario
                    return pUsuario.Legajo;
                }
            }
            catch (Exception)
            {
                //Si ocurrio un error al guardar en la base de datos devuelvo ERR_DB
                return Usuario.ERR_DB;
            }
        }

        //########### METODO PARA MODIFICAR UN USUARIO #############

        public int modificarUsuario(Usuario pUsuario)
        {
            try
            {
                Conexion conexion = new Conexion();
                SqlParameter[] paramSp = new SqlParameter[16];
                paramSp[0] = conexion.crearParametro("@pIdDomicilio", pUsuario.Domicilio.IdDomicilio);
                paramSp[1] = conexion.crearParametro("@pCalle", pUsuario.Domicilio.Calle);
                paramSp[2] = conexion.crearParametro("@pAltura", pUsuario.Domicilio.Altura);
                paramSp[3] = conexion.crearParametro("@pPiso", pUsuario.Domicilio.Piso);
                paramSp[4] = conexion.crearParametro("@pDepto", pUsuario.Domicilio.Depto);
                paramSp[5] = conexion.crearParametro("@pCodLocalidad", pUsuario.Domicilio.Localidad.Codigo);
                paramSp[6] = conexion.crearParametro("@pIdTelefono", pUsuario.Telefono.IdTelefono);
                paramSp[7] = conexion.crearParametro("@pCodArea", pUsuario.Telefono.CodArea);
                paramSp[8] = conexion.crearParametro("@pNumero", pUsuario.Telefono.Numero);
                paramSp[9] = conexion.crearParametro("@pIdPersona", pUsuario.IdPersona);
                paramSp[10] = conexion.crearParametro("@pNombre", pUsuario.Nombre);
                paramSp[11] = conexion.crearParametro("@pApellido", pUsuario.Apellido);
                paramSp[12] = conexion.crearParametro("@pCuil", pUsuario.Cuil);
                paramSp[13] = conexion.crearParametro("@pIdUsuario", pUsuario.Legajo);
                paramSp[14] = conexion.crearParametro("@pActivo", pUsuario.Activo);
                paramSp[15] = conexion.crearParametro("@pCodTipo", pUsuario.TipoUsuario.Codigo);
                conexion.LeerPorStoreProcedure("sp_modificarUsuario", paramSp);
            }
            catch (Exception)
            {
                return Usuario.ERR_DB;
            }
            return Usuario.OK;
        }
        public List<Usuario> buscarUsuario(FiltrosDeBusquedaDeUsuario pFiltros)
        {
            
            List<Usuario> listaUsuarios = new List<Usuario>(); //Creo una lista de usuarios
            Usuario unUsuario; //Creo un usuario
            Conexion conexion = new Conexion(); //Me conecto a la base de datos
            //Cargo los parametros para el store procedure
            SqlParameter[] paramSp = new SqlParameter[4];
            paramSp[0] = conexion.crearParametro("@pActivo", pFiltros.Estado);
            paramSp[1] = conexion.crearParametro("@pNombre", pFiltros.CadenaContenidaEnNombreOApellido);
            paramSp[2] = conexion.crearParametro("@pLegajo", pFiltros.Legajo);
            paramSp[3] = conexion.crearParametro("@pCodTipo", pFiltros.TipoUsuario.Codigo);
            //Ejecuto el store procedure
            DataTable datos = conexion.LeerPorStoreProcedure("sp_buscarUsuario", paramSp);

            //Cargo los usuarios en la lista con los datos de la base
            foreach (DataRow fila in datos.Rows)
            {
                unUsuario = new Usuario();
                unUsuario.Activo = Boolean.Parse(fila["activo"].ToString());
                unUsuario.IdPersona = int.Parse(fila["idPersona"].ToString());
                unUsuario.Nombre = fila["nombreEmpleado"].ToString();
                unUsuario.Apellido = fila["apellido"].ToString();
                unUsuario.Cuil = fila["cuil"].ToString();
                unUsuario.Legajo = int.Parse(fila["idUsuario"].ToString());
                unUsuario.Pass = fila["pass"].ToString();
                //Cargo tipo usuario
                TipoUsuario unTipoUsuario = new TipoUsuario();
                unTipoUsuario.Descripcion = fila["descripcionTipoUsuario"].ToString();
                unTipoUsuario.Codigo = int.Parse(fila["codTipo"].ToString());
                unUsuario.TipoUsuario = unTipoUsuario;
                //Cargo telefono
                Telefono unTelefono = new Telefono();
                unTelefono.IdTelefono = int.Parse(fila["idTelefono"].ToString());
                unTelefono.CodArea = fila["codArea"].ToString();
                unTelefono.Numero = fila["numero"].ToString();
                unUsuario.Telefono = unTelefono;
                //Cargo domicilio
                Domicilio unDomicilio = new Domicilio();
                unDomicilio.IdDomicilio = int.Parse(fila["idDomicilio"].ToString());
                unDomicilio.Calle = fila["calle"].ToString();
                unDomicilio.Altura = int.Parse(fila["altura"].ToString());
                //Cargo localidad
                Localidad unaLocalidad = new Localidad();
                unaLocalidad.Codigo = int.Parse(fila["codLocalidad"].ToString());
                unaLocalidad.Nombre = fila["nombreLocalidad"].ToString();
                unaLocalidad.Provincia = new Provincia() { Codigo = int.Parse(fila["codProvincia"].ToString()), Nombre = fila["nombreProvincia"].ToString() };
                unDomicilio.Localidad = unaLocalidad;
                unUsuario.Domicilio = unDomicilio;
                //Agrego el usuario a la lista
                listaUsuarios.Add(unUsuario);
            }
            return listaUsuarios;
        }

        //########### METODO PARA CAMBIAR EL PASS DEL USUARIO #############

        public bool modificarConstraseña(Usuario pUsuario, string pNvaContraseña)
        {
            try
            {
                Conexion conexion = new Conexion();
                SqlParameter[] paramSP = new SqlParameter[2];

                paramSP[0] = conexion.crearParametro("@pNvaContraseña", pNvaContraseña);
                paramSP[1] = conexion.crearParametro("@pLegajo", pUsuario.Legajo);
                DataTable resultado = conexion.LeerPorStoreProcedure("sp_cambiarContraseña", paramSP);
            }
            catch
            {
                return false;
            }
            return true;
        }
    }
}
