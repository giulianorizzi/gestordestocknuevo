﻿/*
 * UNIVERSIDAD PROVINCIAL DE EZEIZA
 * CARRERA: TENICATURA UNIVERSITARIA EN DESARROLLO DE SOFTWARE
 * MATERIA: TECNICAS DE PROGRAMACION (1ER CUAT 2020)
 * TRABAJO PRACTICO INTEGRADOR
 * INTEGRANTES DEL EQUIPO:
 *      -RIZZI GIULIANO
 *      -SEGADE FLAVIO
 *      -LALANNE DARIO
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DALGestorDeStock
{
    public class DALTipoProducto : Entity.TipoProducto
    {
        public List<Entity.TipoProducto> Listar()
        {

            Conexion objCon = new Conexion();
            DataTable dt = objCon.LeerPorComando("select * from categoriaProducto");

            List<Entity.TipoProducto> listaDeTipoProducto = new List<Entity.TipoProducto>();
            Entity.TipoProducto unTipoProducto;

            foreach (DataRow fila in dt.Rows)
            {
                unTipoProducto = new Entity.TipoProducto();

                unTipoProducto.Codigo = int.Parse(fila["codCategoria"].ToString());
                unTipoProducto.Descripcion = fila["nombreCategoria"].ToString();


                listaDeTipoProducto.Add(unTipoProducto);
            }

            return listaDeTipoProducto;
        }
    }
}
