﻿/*
 * UNIVERSIDAD PROVINCIAL DE EZEIZA
 * CARRERA: TENICATURA UNIVERSITARIA EN DESARROLLO DE SOFTWARE
 * MATERIA: TECNICAS DE PROGRAMACION (1ER CUAT 2020)
 * TRABAJO PRACTICO INTEGRADOR
 * INTEGRANTES DEL EQUIPO:
 *      -RIZZI GIULIANO
 *      -SEGADE FLAVIO
 *      -LALANNE DARIO
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DALGestorDeStock
{
    public class DALEstadoOrden : Entity.EstadoOrden
    {
        public List<Entity.EstadoOrden> Listar()
        {

            Conexion objCon = new Conexion();
            DataTable dt = objCon.LeerPorComando("select * from estadoOrden");

            List<Entity.EstadoOrden> listaDeEstados = new List<Entity.EstadoOrden>();
            Entity.EstadoOrden unEstadoOrden;

            foreach (DataRow fila in dt.Rows)
            {
                unEstadoOrden = new Entity.EstadoOrden();

                unEstadoOrden.Codigo = int.Parse(fila["codEstado"].ToString());
                unEstadoOrden.Descripcion = fila["descripcion"].ToString();


                listaDeEstados.Add(unEstadoOrden);
            }

            return listaDeEstados;
        }
    }
}
