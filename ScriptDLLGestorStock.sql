USE [master]
GO

/****** Object:  Database [DBGestorDeStock ]    Script Date: 22/06/2020 12:14:33 p.m. ******/
CREATE DATABASE [DBGestorDeStock ]
 
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [DBGestorDeStock ].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO

ALTER DATABASE [DBGestorDeStock ] SET ANSI_NULL_DEFAULT OFF 
GO

ALTER DATABASE [DBGestorDeStock ] SET ANSI_NULLS OFF 
GO

ALTER DATABASE [DBGestorDeStock ] SET ANSI_PADDING OFF 
GO

ALTER DATABASE [DBGestorDeStock ] SET ANSI_WARNINGS OFF 
GO

ALTER DATABASE [DBGestorDeStock ] SET ARITHABORT OFF 
GO

ALTER DATABASE [DBGestorDeStock ] SET AUTO_CLOSE OFF 
GO

ALTER DATABASE [DBGestorDeStock ] SET AUTO_SHRINK OFF 
GO

ALTER DATABASE [DBGestorDeStock ] SET AUTO_UPDATE_STATISTICS ON 
GO

ALTER DATABASE [DBGestorDeStock ] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO

ALTER DATABASE [DBGestorDeStock ] SET CURSOR_DEFAULT  GLOBAL 
GO

ALTER DATABASE [DBGestorDeStock ] SET CONCAT_NULL_YIELDS_NULL OFF 
GO

ALTER DATABASE [DBGestorDeStock ] SET NUMERIC_ROUNDABORT OFF 
GO

ALTER DATABASE [DBGestorDeStock ] SET QUOTED_IDENTIFIER OFF 
GO

ALTER DATABASE [DBGestorDeStock ] SET RECURSIVE_TRIGGERS OFF 
GO

ALTER DATABASE [DBGestorDeStock ] SET  DISABLE_BROKER 
GO

ALTER DATABASE [DBGestorDeStock ] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO

ALTER DATABASE [DBGestorDeStock ] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO

ALTER DATABASE [DBGestorDeStock ] SET TRUSTWORTHY OFF 
GO

ALTER DATABASE [DBGestorDeStock ] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO

ALTER DATABASE [DBGestorDeStock ] SET PARAMETERIZATION SIMPLE 
GO

ALTER DATABASE [DBGestorDeStock ] SET READ_COMMITTED_SNAPSHOT OFF 
GO

ALTER DATABASE [DBGestorDeStock ] SET HONOR_BROKER_PRIORITY OFF 
GO

ALTER DATABASE [DBGestorDeStock ] SET RECOVERY SIMPLE 
GO

ALTER DATABASE [DBGestorDeStock ] SET  MULTI_USER 
GO

ALTER DATABASE [DBGestorDeStock ] SET PAGE_VERIFY CHECKSUM  
GO

ALTER DATABASE [DBGestorDeStock ] SET DB_CHAINING OFF 
GO

ALTER DATABASE [DBGestorDeStock ] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO

ALTER DATABASE [DBGestorDeStock ] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO

ALTER DATABASE [DBGestorDeStock ] SET DELAYED_DURABILITY = DISABLED 
GO

ALTER DATABASE [DBGestorDeStock ] SET QUERY_STORE = OFF
GO

ALTER DATABASE [DBGestorDeStock ] SET  READ_WRITE 
GO

USE [DBGestorDeStock ]
GO
/****** Object:  UserDefinedFunction [dbo].[calcularStock]    Script Date: 22/06/2020 12:11:43 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[calcularStock] (@pCodProducto int)
RETURNS int
AS BEGIN
    DECLARE @cantidad int

    select @cantidad = (select sum(s.cantidadRecibida) 
	from detalleOrden d
	inner join stock s
	on s.nroDetalleOrden = d.nroDetalleOrden
	where d.codProducto = @pCodProducto)

	if @cantidad is null
		set @cantidad = 0
    RETURN @cantidad
END
GO
/****** Object:  Table [dbo].[persona]    Script Date: 22/06/2020 12:11:43 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[persona](
	[idPersona] [int] IDENTITY(1,1) NOT NULL,
	[cuil] [varchar](32) NOT NULL,
	[nombre] [varchar](20) NOT NULL,
	[apellido] [varchar](20) NOT NULL,
	[idDomicilio] [int] NOT NULL,
	[idTelefono] [int] NOT NULL,
 CONSTRAINT [PK_persona_1] PRIMARY KEY CLUSTERED 
(
	[idPersona] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[domicilio]    Script Date: 22/06/2020 12:11:43 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[domicilio](
	[idDomicilio] [int] IDENTITY(1,1) NOT NULL,
	[calle] [varchar](20) NOT NULL,
	[altura] [int] NOT NULL,
	[piso] [int] NOT NULL,
	[depto] [varchar](5) NOT NULL,
	[codLocalidad] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[idDomicilio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[localidad]    Script Date: 22/06/2020 12:11:43 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[localidad](
	[codLocalidad] [int] IDENTITY(1,1) NOT NULL,
	[nombreLocalidad] [varchar](50) NOT NULL,
	[codProvincia] [int] NOT NULL,
 CONSTRAINT [PK_Table_4] PRIMARY KEY CLUSTERED 
(
	[codLocalidad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[provincia]    Script Date: 22/06/2020 12:11:43 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[provincia](
	[codProvincia] [int] IDENTITY(1,1) NOT NULL,
	[nombreProvincia] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Table_2] PRIMARY KEY CLUSTERED 
(
	[codProvincia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[telefono]    Script Date: 22/06/2020 12:11:43 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[telefono](
	[idTelefono] [int] IDENTITY(1,1) NOT NULL,
	[codArea] [varchar](10) NOT NULL,
	[numero] [varchar](10) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[idTelefono] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tipoUsuario]    Script Date: 22/06/2020 12:11:43 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tipoUsuario](
	[codTipo] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](30) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[codTipo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[usuario]    Script Date: 22/06/2020 12:11:43 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[usuario](
	[idUsuario] [int] IDENTITY(1000,1) NOT NULL,
	[pass] [varchar](200) NOT NULL,
	[activo] [bit] NOT NULL,
	[codTipo] [int] NOT NULL,
	[idPersona] [int] NOT NULL,
 CONSTRAINT [PK__usuario__645723A60BC0A870] PRIMARY KEY CLUSTERED 
(
	[idUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[vista_buscarUsuario]    Script Date: 22/06/2020 12:11:43 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vista_buscarUsuario]

 AS

 SELECT 
		
	u.idUsuario,
	u.activo,
	u.pass,
	u.codTipo,
	tipoUsuario.nombre as descripcionTipoUsuario,
	p.idPersona,
	p.nombre as nombreEmpleado,
	p.apellido,
	p.cuil,
	d.idDomicilio,
	d.calle,
	d.altura,
	d.piso,
	d.depto,
	t.idTelefono,
	t.codArea,
	t.numero,
	l.codLocalidad,
	l.nombreLocalidad,
	pr.codProvincia,
	pr.nombreProvincia
FROM usuario AS u

INNER JOIN tipoUsuario  ON u.codTipo = tipoUsuario.codTipo
INNER JOIN persona AS p ON p.idPersona = u.idPersona
INNER JOIN domicilio AS d ON p.idDomicilio = d.idDomicilio
INNER JOIN telefono as t ON p.idTelefono = t.idTelefono
INNER JOIN localidad as l ON d.codLocalidad = l.codLocalidad
INNER JOIN provincia AS pr ON pr.codProvincia = l.codProvincia



GO
/****** Object:  Table [dbo].[stock]    Script Date: 22/06/2020 12:11:43 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[stock](
	[idStock] [int] IDENTITY(1,1) NOT NULL,
	[nroDetalleOrden] [int] NOT NULL,
	[nroAlmacen] [int] NOT NULL,
	[cantidadPedida] [int] NULL,
	[cantidadRecibida] [int] NULL,
	[precio] [money] NULL,
	[fecha] [datetime2](7) NULL,
 CONSTRAINT [PK_stock] PRIMARY KEY CLUSTERED 
(
	[idStock] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ordenDeCompra]    Script Date: 22/06/2020 12:11:43 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ordenDeCompra](
	[nroOrden] [int] IDENTITY(1,1) NOT NULL,
	[fecha] [datetime2](7) NULL,
	[cuitProveedor] [varchar](11) NULL,
	[estado] [int] NULL,
 CONSTRAINT [PK_ordenDeCompra] PRIMARY KEY CLUSTERED 
(
	[nroOrden] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[categoriaProducto]    Script Date: 22/06/2020 12:11:43 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[categoriaProducto](
	[codCategoria] [int] IDENTITY(1,1) NOT NULL,
	[nombreCategoria] [nvarchar](50) NOT NULL,
	[nroAlmacen] [int] NOT NULL,
 CONSTRAINT [PK_categoriaProducto] PRIMARY KEY CLUSTERED 
(
	[codCategoria] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[detalleOrden]    Script Date: 22/06/2020 12:11:43 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[detalleOrden](
	[nroDetalleOrden] [int] IDENTITY(1,1) NOT NULL,
	[codProducto] [int] NOT NULL,
	[cantidad] [int] NULL,
	[precio] [money] NULL,
	[nroOrden] [int] NOT NULL,
 CONSTRAINT [PK_detalleOrden] PRIMARY KEY CLUSTERED 
(
	[nroDetalleOrden] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[estadoOrden]    Script Date: 22/06/2020 12:11:43 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[estadoOrden](
	[codEstado] [int] IDENTITY(1,1) NOT NULL,
	[descripcion] [nvarchar](50) NULL,
 CONSTRAINT [PK_estadoOrden] PRIMARY KEY CLUSTERED 
(
	[codEstado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[producto]    Script Date: 22/06/2020 12:11:43 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[producto](
	[codProducto] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nvarchar](50) NOT NULL,
	[descripcion] [nvarchar](50) NULL,
	[activo] [bit] NULL,
	[codCategoria] [int] NOT NULL,
 CONSTRAINT [PK_producto] PRIMARY KEY CLUSTERED 
(
	[codProducto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[proveedor]    Script Date: 22/06/2020 12:11:43 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[proveedor](
	[cuit] [varchar](11) NOT NULL,
	[razonSocial] [varchar](50) NOT NULL,
	[idDomicilio] [int] NOT NULL,
	[idTelefono] [int] NOT NULL,
 CONSTRAINT [PK_proveedor] PRIMARY KEY CLUSTERED 
(
	[cuit] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[vista_buscarOrdenDeCompra]    Script Date: 22/06/2020 12:11:43 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[vista_buscarOrdenDeCompra]

AS 

SELECT 
ordenDeCompra.nroOrden, 
codEstado,
ordenDeCompra.fecha,
estadoOrden.descripcion,
producto.nombre,
producto.descripcion as 'desc prod',
producto.codProducto,
producto.codCategoria,
categoriaProducto.nombreCategoria,
detalleOrden.nroDetalleOrden,
detalleOrden.cantidad,
stock.cantidadPedida,
stock.cantidadRecibida,
detalleOrden.precio,
proveedor.razonSocial,
cuitProveedor,
domicilio.idDomicilio,
domicilio.calle,
domicilio.altura,
domicilio.piso,
domicilio.depto,
localidad.codLocalidad,
localidad.nombreLocalidad,
provincia.codProvincia,
provincia.nombreProvincia,
telefono.idTelefono,
telefono.codArea,
telefono.numero

FROM ordenDeCompra

INNER JOIN  estadoOrden ON estadoOrden.codEstado = ordenDeCompra.estado 
LEFT JOIN proveedor   ON proveedor.cuit = ordenDeCompra.cuitProveedor
LEFT JOIN detalleOrden ON detalleOrden.nroOrden = ordenDeCompra.nroOrden 
LEFT JOIN producto ON producto.codProducto = detalleOrden.codProducto
LEFT JOIN categoriaProducto ON categoriaProducto.codCategoria = producto.codCategoria
INNER JOIN domicilio ON proveedor.idDomicilio = domicilio.idDomicilio
INNER JOIN localidad ON localidad.codLocalidad = domicilio.codLocalidad
INNER JOIN provincia ON provincia.codProvincia = localidad.codProvincia
INNER JOIN telefono ON telefono.idTelefono = proveedor.idTelefono
left join stock on stock.nroDetalleOrden = detalleOrden.nroDetalleOrden


GO
/****** Object:  View [dbo].[vista_productos]    Script Date: 22/06/2020 12:11:43 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vista_productos] AS
SELECT p.codProducto as "Codigo", p.nombre as "Nombre", p.descripcion as "Descripcion", p.activo as "Estado" ,
	cp.codCategoria as "codCategoria", cp.nombreCategoria as "Categoria", dbo.calcularStock(p.codProducto) as "Stock"
	from producto p
	inner join categoriaProducto cp
	on p.codCategoria = cp.codCategoria
GO
/****** Object:  Table [dbo].[almacen]    Script Date: 22/06/2020 12:11:43 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[almacen](
	[nroAlmacen] [int] IDENTITY(1,1) NOT NULL,
	[descripcion] [varchar](50) NULL,
 CONSTRAINT [PK_almacen] PRIMARY KEY CLUSTERED 
(
	[nroAlmacen] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[vista_stock]    Script Date: 22/06/2020 12:11:43 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







CREATE VIEW [dbo].[vista_stock] AS
SELECT s.idStock "ID", do.codProducto as "Codigo", s.cantidadRecibida as 'Cantidad recibida', s.cantidadPedida as 'Cantidad pedida',
s.precio as "Precio", a.nroAlmacen as "Codigo Almacen", a.descripcion as "Almacen", do.nroOrden as "Orden", 
do.nroDetalleOrden as "Detalle de Orden", s.fecha as "Fecha"
from stock s
inner join detalleOrden do on s.nroDetalleOrden = do.nroDetalleOrden
inner join almacen a on s.nroAlmacen = a.nroAlmacen
GO
/****** Object:  Table [dbo].[historicoOrdenDeCompra]    Script Date: 22/06/2020 12:11:43 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[historicoOrdenDeCompra](
	[idHistoricoOrden] [int] IDENTITY(1,1) NOT NULL,
	[idUsuario] [int] NOT NULL,
	[codEstado] [int] NOT NULL,
	[nroOrden] [int] NOT NULL,
	[fecha] [datetime2](7) NULL,
 CONSTRAINT [PK_historicoOrdenDeCompra] PRIMARY KEY CLUSTERED 
(
	[idHistoricoOrden] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[producto] ADD  CONSTRAINT [DF_producto_activo]  DEFAULT ((1)) FOR [activo]
GO
ALTER TABLE [dbo].[usuario] ADD  CONSTRAINT [DF__usuario__activo__59FA5E80]  DEFAULT ((1)) FOR [activo]
GO
ALTER TABLE [dbo].[categoriaProducto]  WITH CHECK ADD  CONSTRAINT [FK_categoriaProducto_almacen] FOREIGN KEY([nroAlmacen])
REFERENCES [dbo].[almacen] ([nroAlmacen])
GO
ALTER TABLE [dbo].[categoriaProducto] CHECK CONSTRAINT [FK_categoriaProducto_almacen]
GO
ALTER TABLE [dbo].[detalleOrden]  WITH CHECK ADD  CONSTRAINT [FK_detalleOrden_ordenDeCompra] FOREIGN KEY([nroOrden])
REFERENCES [dbo].[ordenDeCompra] ([nroOrden])
GO
ALTER TABLE [dbo].[detalleOrden] CHECK CONSTRAINT [FK_detalleOrden_ordenDeCompra]
GO
ALTER TABLE [dbo].[detalleOrden]  WITH CHECK ADD  CONSTRAINT [FK_detalleOrden_producto] FOREIGN KEY([codProducto])
REFERENCES [dbo].[producto] ([codProducto])
GO
ALTER TABLE [dbo].[detalleOrden] CHECK CONSTRAINT [FK_detalleOrden_producto]
GO
ALTER TABLE [dbo].[domicilio]  WITH CHECK ADD FOREIGN KEY([codLocalidad])
REFERENCES [dbo].[localidad] ([codLocalidad])
GO
ALTER TABLE [dbo].[domicilio]  WITH CHECK ADD FOREIGN KEY([codLocalidad])
REFERENCES [dbo].[localidad] ([codLocalidad])
GO
ALTER TABLE [dbo].[historicoOrdenDeCompra]  WITH CHECK ADD  CONSTRAINT [FK_historicoOrdenDeCompra_estadoOrden] FOREIGN KEY([codEstado])
REFERENCES [dbo].[estadoOrden] ([codEstado])
GO
ALTER TABLE [dbo].[historicoOrdenDeCompra] CHECK CONSTRAINT [FK_historicoOrdenDeCompra_estadoOrden]
GO
ALTER TABLE [dbo].[historicoOrdenDeCompra]  WITH CHECK ADD  CONSTRAINT [FK_historicoOrdenDeCompra_ordenDeCompra] FOREIGN KEY([nroOrden])
REFERENCES [dbo].[ordenDeCompra] ([nroOrden])
GO
ALTER TABLE [dbo].[historicoOrdenDeCompra] CHECK CONSTRAINT [FK_historicoOrdenDeCompra_ordenDeCompra]
GO
ALTER TABLE [dbo].[historicoOrdenDeCompra]  WITH CHECK ADD  CONSTRAINT [FK_historicoOrdenDeCompra_usuario] FOREIGN KEY([idUsuario])
REFERENCES [dbo].[usuario] ([idUsuario])
GO
ALTER TABLE [dbo].[historicoOrdenDeCompra] CHECK CONSTRAINT [FK_historicoOrdenDeCompra_usuario]
GO
ALTER TABLE [dbo].[localidad]  WITH CHECK ADD  CONSTRAINT [fk_provincia_localidad] FOREIGN KEY([codProvincia])
REFERENCES [dbo].[provincia] ([codProvincia])
GO
ALTER TABLE [dbo].[localidad] CHECK CONSTRAINT [fk_provincia_localidad]
GO
ALTER TABLE [dbo].[ordenDeCompra]  WITH CHECK ADD  CONSTRAINT [FK_ordenDeCompra_estadoOrden] FOREIGN KEY([estado])
REFERENCES [dbo].[estadoOrden] ([codEstado])
GO
ALTER TABLE [dbo].[ordenDeCompra] CHECK CONSTRAINT [FK_ordenDeCompra_estadoOrden]
GO
ALTER TABLE [dbo].[ordenDeCompra]  WITH CHECK ADD  CONSTRAINT [FK_ordenDeCompra_proveedor] FOREIGN KEY([cuitProveedor])
REFERENCES [dbo].[proveedor] ([cuit])
GO
ALTER TABLE [dbo].[ordenDeCompra] CHECK CONSTRAINT [FK_ordenDeCompra_proveedor]
GO
ALTER TABLE [dbo].[persona]  WITH CHECK ADD  CONSTRAINT [FK__persona__idDomic__5CD6CB2B] FOREIGN KEY([idDomicilio])
REFERENCES [dbo].[domicilio] ([idDomicilio])
GO
ALTER TABLE [dbo].[persona] CHECK CONSTRAINT [FK__persona__idDomic__5CD6CB2B]
GO
ALTER TABLE [dbo].[persona]  WITH CHECK ADD  CONSTRAINT [FK__persona__idTelef__5DCAEF64] FOREIGN KEY([idTelefono])
REFERENCES [dbo].[telefono] ([idTelefono])
GO
ALTER TABLE [dbo].[persona] CHECK CONSTRAINT [FK__persona__idTelef__5DCAEF64]
GO
ALTER TABLE [dbo].[producto]  WITH CHECK ADD  CONSTRAINT [FK_producto_categoriaProducto] FOREIGN KEY([codCategoria])
REFERENCES [dbo].[categoriaProducto] ([codCategoria])
GO
ALTER TABLE [dbo].[producto] CHECK CONSTRAINT [FK_producto_categoriaProducto]
GO
ALTER TABLE [dbo].[proveedor]  WITH CHECK ADD  CONSTRAINT [FK_proveedor_domicilio1] FOREIGN KEY([idDomicilio])
REFERENCES [dbo].[domicilio] ([idDomicilio])
GO
ALTER TABLE [dbo].[proveedor] CHECK CONSTRAINT [FK_proveedor_domicilio1]
GO
ALTER TABLE [dbo].[proveedor]  WITH CHECK ADD  CONSTRAINT [FK_proveedor_telefono] FOREIGN KEY([idTelefono])
REFERENCES [dbo].[telefono] ([idTelefono])
GO
ALTER TABLE [dbo].[proveedor] CHECK CONSTRAINT [FK_proveedor_telefono]
GO
ALTER TABLE [dbo].[stock]  WITH CHECK ADD  CONSTRAINT [FK_stock_almacen] FOREIGN KEY([nroAlmacen])
REFERENCES [dbo].[almacen] ([nroAlmacen])
GO
ALTER TABLE [dbo].[stock] CHECK CONSTRAINT [FK_stock_almacen]
GO
ALTER TABLE [dbo].[stock]  WITH CHECK ADD  CONSTRAINT [FK_stock_detalleOrden] FOREIGN KEY([nroDetalleOrden])
REFERENCES [dbo].[detalleOrden] ([nroDetalleOrden])
GO
ALTER TABLE [dbo].[stock] CHECK CONSTRAINT [FK_stock_detalleOrden]
GO
ALTER TABLE [dbo].[usuario]  WITH CHECK ADD  CONSTRAINT [FK_usuario_persona] FOREIGN KEY([idPersona])
REFERENCES [dbo].[persona] ([idPersona])
GO
ALTER TABLE [dbo].[usuario] CHECK CONSTRAINT [FK_usuario_persona]
GO
/****** Object:  StoredProcedure [dbo].[sp_actualizarProveedorOrden]    Script Date: 22/06/2020 12:11:43 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_actualizarProveedorOrden]
	@pCuitProveedor  varchar(50),
	@pNroOrden  int
as
	update ordenDeCompra
	set cuitProveedor = @pCuitProveedor
	where nroOrden = @pNroOrden


GO
/****** Object:  StoredProcedure [dbo].[sp_altaDomicilio]    Script Date: 22/06/2020 12:11:43 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_altaDomicilio]

	@pCalle varchar(11),
	@pAltura int,
	@pPiso int,
	@pDepto varchar(5),
	@pCodLocalidad int
	
AS
	INSERT INTO domicilio(calle,altura,piso,depto,codLocalidad)
	VALUES(@pCalle,@pAltura,@pPiso,@pDepto,@pCodLocalidad)

	SELECT SCOPE_IDENTITY()


GO
/****** Object:  StoredProcedure [dbo].[sp_altaHistorico]    Script Date: 22/06/2020 12:11:43 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

  CREATE procedure [dbo].[sp_altaHistorico]
	@pIdUsuario  int,
	@pEstado  int,
	@pNroOrden int
as
  insert into historicoOrdenDeCompra(idUsuario, codEstado, nroOrden, fecha)
  values(@pIdUsuario,@pEstado,@pNroOrden,SYSDATETIME())


GO
/****** Object:  StoredProcedure [dbo].[sp_altaNecesidad]    Script Date: 22/06/2020 12:11:43 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE procedure [dbo].[sp_altaNecesidad]

	--@pFecha nvarchar(50)
	--@pCuit  nvarchar(50)
	
as
SET DATEFORMAT dmy;  

	declare @nroOrden int
	INSERT INTO [dbo].[ordenDeCompra]
           ([fecha],
		   estado)
           --,[cuitProveedor])
     VALUES
           (SYSDATETIME(), 1)
		   --,@pCuit)
	SET @nroOrden = SCOPE_IDENTITY()
	select @nroOrden


GO
/****** Object:  StoredProcedure [dbo].[sp_altaOrden]    Script Date: 22/06/2020 12:11:43 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_altaOrden]
	@pNroDetalle  int,
	@pCantidad  int,
	@pPrecio float,
	@pNroOrden int
as
	UPDATE [dbo].[detalleOrden]
	   SET [cantidad] = @pCantidad
		  ,[precio] = @pPrecio
	 WHERE detalleOrden.nroDetalleOrden = @pNroDetalle

	 update ordenDeCompra
	 set estado = 2
	 where nroOrden = @pNroOrden
GO
/****** Object:  StoredProcedure [dbo].[sp_altaPersona]    Script Date: 22/06/2020 12:11:43 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

	
CREATE PROCEDURE [dbo].[sp_altaPersona]

	@pCuil varchar(32),
	@pNombre varchar(20),
	@pApellido varchar(20),
	@pIdDomicilio int,
	@pIdTelefono int

	
	
AS
	INSERT INTO persona(cuil,nombre,apellido,idDomicilio,idTelefono)
	VALUES(@pCuil,@pNombre,@pApellido,@pIdDomicilio,@pIdTelefono)

	SELECT SCOPE_IDENTITY()


GO
/****** Object:  StoredProcedure [dbo].[sp_altaProducto]    Script Date: 22/06/2020 12:11:43 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_altaProducto]
	@pNombre  nvarchar(50),
	@pDescripcion  nvarchar(50),
	@pCodCategoria int
as
	INSERT INTO [dbo].[producto]([nombre],[descripcion],[codCategoria])
     VALUES(@pNombre, @pDescripcion, @pCodCategoria)


GO
/****** Object:  StoredProcedure [dbo].[sp_altaStock]    Script Date: 22/06/2020 12:11:43 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE procedure [dbo].[sp_altaStock]
	@pNroDetalle  int,
	@pCantidadRecibida  int,
	@pNroOrden int
as
	DECLARE @pCantidadPedida int
	DECLARE @pPrecio float

	set @pCantidadPedida = (SELECT cantidad 
	from detalleOrden
	where detalleOrden.nroDetalleOrden = @pNroDetalle)

	set @pPrecio = (SELECT precio 
	from detalleOrden
	where detalleOrden.nroDetalleOrden = @pNroDetalle)

	UPDATE [dbo].[detalleOrden]
	   SET [cantidad] = @pCantidadRecibida
	 WHERE detalleOrden.nroDetalleOrden = @pNroDetalle

	 update ordenDeCompra
	 set estado = 5
	 where nroOrden = @pNroOrden

	INSERT INTO [dbo].[stock]
           ([nroDetalleOrden]
           ,[nroAlmacen]
           ,[cantidadRecibida]
		   ,[cantidadPedida]
           ,[precio]
           ,[fecha])
     VALUES(
           @pNroDetalle
           ,(select nroAlmacen from categoriaProducto
			where codCategoria = (select codCategoria from producto
			where codProducto = (select codProducto from detalleOrden
			where nroDetalleOrden = @pNroDetalle)))
           ,@pCantidadRecibida
		   ,@pCantidadPedida
           ,@pPrecio
           ,SYSDATETIME())
GO
/****** Object:  StoredProcedure [dbo].[sp_altaTelefono]    Script Date: 22/06/2020 12:11:43 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_altaTelefono]

	@pCodArea varchar(10),
	@pNumero varchar(10)
	
AS
	INSERT INTO telefono(codArea,numero)
	VALUES(@pCodArea,@pNumero)

	SELECT SCOPE_IDENTITY()


GO
/****** Object:  StoredProcedure [dbo].[sp_altaUsuario]    Script Date: 22/06/2020 12:11:43 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[sp_altaUsuario]

	@pPass varchar(100),
	@pActivo bit,
	@pCodTipo int,
	@pIdPersona int
	
AS
	INSERT INTO usuario(pass,activo,codTipo,idPersona)
	VALUES(@pPass,@pActivo,@pCodTipo,@pIdPersona)

	SELECT SCOPE_IDENTITY()




GO
/****** Object:  StoredProcedure [dbo].[sp_aprobarOrden]    Script Date: 22/06/2020 12:11:43 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_aprobarOrden]
	@pNroOrden  int
as
	UPDATE [dbo].[ordenDeCompra]
	SET [estado] = 3
	WHERE nroOrden = @pNroOrden
GO
/****** Object:  StoredProcedure [dbo].[sp_bajaDetalle]    Script Date: 22/06/2020 12:11:43 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_bajaDetalle]
	@pNroDetalle  int
as
delete from detalleOrden
where detalleOrden.nroDetalleOrden = @pNroDetalle
GO
/****** Object:  StoredProcedure [dbo].[sp_buscarOrdenDeCompra]    Script Date: 22/06/2020 12:11:43 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_buscarOrdenDeCompra]

@pNroOrden INT,
@pCodEstado INT,
@pCuitProveedor VARCHAR(11),
@pFechaInicial DATE,
@pFechaFinal DATE,
@pFiltrarPorFecha BIT
AS

SELECT * 

FROM 

vista_buscarOrdenDeCompra

WHERE 1 = 1
AND (@pNroOrden = 0 OR nroOrden = @pNroOrden )
AND (@pCodEstado  = 0 OR codEstado = @pCodEstado )
AND (@pCuitProveedor = '0' OR cuitProveedor = @pCuitProveedor)
AND (@pFiltrarPorFecha = 0 OR CONVERT(date,fecha) BETWEEN @pFechaInicial AND @pFechaFinal)

GO
/****** Object:  StoredProcedure [dbo].[sp_buscarUsuario]    Script Date: 22/06/2020 12:11:43 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_buscarUsuario]
@pCodTipo INT,
@pLegajo INT,
@pActivo INT,
@pNombre VARCHAR(20)

AS

select * from vista_buscarUsuario

WHERE 1 = 1
AND (@pCodTipo = 0 OR codTipo = @pCodTipo )
AND (@pLegajo = 0 OR idUsuario = @pLegajo)
AND (@pActivo = 2 OR activo = @pActivo)
AND (@pNombre = ''  OR nombreEmpleado LIKE '%' + @pNombre + '%')




GO
/****** Object:  StoredProcedure [dbo].[sp_cambiarContraseña]    Script Date: 22/06/2020 12:11:43 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[sp_cambiarContraseña]
		@pLegajo int,
		@pNvaContraseña varchar(200)
as
update usuario
set usuario.pass = @pNvaContraseña
where usuario.idUsuario = @pLegajo


GO
/****** Object:  StoredProcedure [dbo].[sp_consultarHistoricoOrdenCompra]    Script Date: 22/06/2020 12:11:43 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_consultarHistoricoOrdenCompra] 

@pNumOrden int

as

select

h.idHistoricoOrden,
h.fecha,
e.codEstado,
e.descripcion as descripcionEstadoOrden,
u.idUsuario,
u.activo,
u.codTipo,
tu.nombre as descripcionTipoUsuario,
p.nombre,
p.apellido,
p.cuil,
p.idDomicilio,
p.idTelefono,
t.codArea,
t.numero,
d.calle,
d.altura,
d.piso,
d.piso,
d.codLocalidad,
l.nombreLocalidad,
l.codProvincia,
pr.nombreProvincia

from

historicoOrdenDeCompra as h

inner join estadoOrden as e on e.codEstado = h.codEstado
inner join usuario as u on u.idUsuario = h.idUsuario
inner join tipoUsuario tu on tu.codTipo = u.codTipo
inner join persona as p on p.idPersona = u.idPersona
inner join domicilio as d on d.idDomicilio = p.idDomicilio
inner join localidad as l on l.codLocalidad = d.codLocalidad
inner join provincia as pr on pr.codProvincia = l.codProvincia
inner join telefono as t on t.idTelefono = p.idTelefono

where

h.nroOrden = @pNumOrden
GO
/****** Object:  StoredProcedure [dbo].[sp_guardarNecesidad]    Script Date: 22/06/2020 12:11:43 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 create procedure [dbo].[sp_guardarNecesidad]
	@pCodProducto  int,
	@pNroOrden  int
as
	insert into detalleOrden
	(codProducto, nroOrden)
	values (@pCodProducto, @pNroOrden)
GO
/****** Object:  StoredProcedure [dbo].[sp_modificacionProducto]    Script Date: 22/06/2020 12:11:43 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_modificacionProducto]
	@pCodProducto int,
	@pNombre  nvarchar(50),
	@pDescripcion  nvarchar(50),
	@pActivo bit,
	@pCodCategoria int
	
as
	update [dbo].[producto]
	SET [nombre] = @pNombre
      ,[descripcion] = @pDescripcion
      ,[activo] = @pActivo
      ,[codCategoria] = @pCodCategoria
 WHERE producto.codProducto = @pCodProducto
GO
/****** Object:  StoredProcedure [dbo].[sp_modificarUsuario]    Script Date: 22/06/2020 12:11:43 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_modificarUsuario]
@pCodTipo int,
@pActivo BIT,
@pIdPersona INT,
@pCuil varchar(32),
@pNombre varchar(20),
@pApellido varchar(20),
@pIdDomicilio INT,
@pCalle varchar(11),
@pAltura int,
@pPiso int,          
@pDepto varchar(5),
@pCodLocalidad int,
@pIdTelefono INT,
@pCodArea varchar(10),
@pNumero varchar(10),
@pIdUsuario INT
AS

UPDATE usuario


SET 

usuario.codTipo = @pCodTipo,
usuario.activo = @pActivo

WHERE 

usuario.idUsuario = @pIdUsuario

UPDATE persona

SET

persona.nombre = @pNombre,
persona.apellido = @pApellido,
persona.cuil = @pCuil

WHERE

persona.idPersona = @pIdPersona

UPDATE domicilio 

SET

domicilio.calle = @pCalle,
domicilio.altura = @pAltura,
domicilio.piso = @pPiso,
domicilio.depto = @pDepto,
domicilio.codLocalidad = @pCodLocalidad

WHERE

idDomicilio = @pIdDomicilio

UPDATE telefono 

SET

telefono.codArea = @pCodArea,
telefono.numero = @pNumero

WHERE idTelefono = @pIdTelefono;





GO
/****** Object:  StoredProcedure [dbo].[sp_moverDeAlmacen]    Script Date: 22/06/2020 12:11:43 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_moverDeAlmacen]
	@pIDStock  int,
	@pCodAlmacen  int
as
UPDATE [dbo].[stock]
   SET [nroAlmacen] = @pCodAlmacen
WHERE stock.idStock = @pIDStock


GO
/****** Object:  StoredProcedure [dbo].[sp_pagarOrden]    Script Date: 22/06/2020 12:11:43 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[sp_pagarOrden]
	@pNroOrden  int
as
	UPDATE [dbo].[ordenDeCompra]
	SET [estado] = 4
	WHERE nroOrden = @pNroOrden
GO
/****** Object:  StoredProcedure [dbo].[sp_verProductos]    Script Date: 22/06/2020 12:11:43 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[sp_verProductos]
	@pNombreOCodigo  nvarchar(50),
	@pCategoria  nvarchar(50),
	@pEstado nvarchar(50),
	@pStock nvarchar(50)
as
	SELECT * from vista_productos
    where 1=1
	and (Codigo like @pNombreOCodigo or Nombre like @pNombreOCodigo+'%')
	and (@pEstado = 2 or Estado = CAST(@pEstado AS bit))
	and (@pCategoria = -1 or codCategoria = CAST(@pCategoria AS int))
	and (@pStock = -1 or Stock < CAST(@pStock AS int))
GO
/****** Object:  StoredProcedure [dbo].[sp_verStock]    Script Date: 22/06/2020 12:11:43 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



create procedure [dbo].[sp_verStock]
	@pCodigo  nvarchar(50)
as
	SELECT * from vista_stock
    where Codigo = @pCodigo
GO
/****** Object:  StoredProcedure [dbo].[spObtenerUsuarioLogueado]    Script Date: 22/06/2020 12:11:43 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spObtenerUsuarioLogueado]

	@pLegajo int

AS
	
	SELECT 
		tipoUsuario.codTipo AS codigoTipoUsuario,
		tipoUsuario.nombre AS descripcionTipoUsuario,
		persona.cuil AS cuilPersona, 
		persona.nombre AS nombrePersona,
		persona.apellido AS apellidoPersona 
	FROM 
		usuario
		INNER JOIN persona ON persona.idPersona = usuario.idPersona
		INNER JOIN tipoUsuario ON tipoUsuario.codTipo = usuario.codTipo
	WHERE 
		usuario.idUsuario = @pLegajo
		
	
GO
