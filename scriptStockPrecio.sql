USE [DBGestorDeStock ]
GO
/****** Object:  StoredProcedure [dbo].[sp_altaStock]    Script Date: 20/06/2020 09:47:03 p.m. ******/
DROP PROCEDURE [dbo].[sp_altaStock]
GO
/****** Object:  StoredProcedure [dbo].[sp_altaStock]    Script Date: 20/06/2020 09:47:03 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE procedure [dbo].[sp_altaStock]
	@pNroDetalle  int,
	@pCantidadRecibida  int,
	@pNroOrden int
as
	DECLARE @pCantidadPedida int
	DECLARE @pPrecio float

	set @pCantidadPedida = (SELECT cantidad 
	from detalleOrden
	where detalleOrden.nroDetalleOrden = @pNroDetalle)

	set @pPrecio = (SELECT precio 
	from detalleOrden
	where detalleOrden.nroDetalleOrden = @pNroDetalle)

	UPDATE [dbo].[detalleOrden]
	   SET [cantidad] = @pCantidadRecibida
	 WHERE detalleOrden.nroDetalleOrden = @pNroDetalle

	 update ordenDeCompra
	 set estado = 5
	 where nroOrden = @pNroOrden

	INSERT INTO [dbo].[stock]
           ([nroDetalleOrden]
           ,[nroAlmacen]
           ,[cantidadRecibida]
		   ,[cantidadPedida]
           ,[precio]
           ,[fecha])
     VALUES(
           @pNroDetalle
           ,(select nroAlmacen from categoriaProducto
			where codCategoria = (select codCategoria from producto
			where codProducto = (select codProducto from detalleOrden
			where nroDetalleOrden = @pNroDetalle)))
           ,@pCantidadRecibida
		   ,@pCantidadPedida
           ,@pPrecio
           ,SYSDATETIME())
GO
