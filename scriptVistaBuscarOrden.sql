USE [DBGestorDeStock ]
GO
/****** Object:  View [dbo].[vista_buscarOrdenDeCompra]    Script Date: 22/06/2020 12:24:37 a.m. ******/
DROP VIEW [dbo].[vista_buscarOrdenDeCompra]
GO
/****** Object:  View [dbo].[vista_buscarOrdenDeCompra]    Script Date: 22/06/2020 12:24:37 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[vista_buscarOrdenDeCompra]

AS 

SELECT 
ordenDeCompra.nroOrden, 
codEstado,
ordenDeCompra.fecha,
estadoOrden.descripcion,
producto.nombre,
producto.descripcion as 'desc prod',
producto.codProducto,
producto.codCategoria,
categoriaProducto.nombreCategoria,
detalleOrden.nroDetalleOrden,
detalleOrden.cantidad,
stock.cantidadPedida,
stock.cantidadRecibida,
detalleOrden.precio,
proveedor.razonSocial,
cuitProveedor,
domicilio.idDomicilio,
domicilio.calle,
domicilio.altura,
domicilio.piso,
domicilio.depto,
localidad.codLocalidad,
localidad.nombreLocalidad,
provincia.codProvincia,
provincia.nombreProvincia,
telefono.idTelefono,
telefono.codArea,
telefono.numero

FROM ordenDeCompra

INNER JOIN  estadoOrden ON estadoOrden.codEstado = ordenDeCompra.estado 
LEFT JOIN proveedor   ON proveedor.cuit = ordenDeCompra.cuitProveedor
LEFT JOIN detalleOrden ON detalleOrden.nroOrden = ordenDeCompra.nroOrden 
LEFT JOIN producto ON producto.codProducto = detalleOrden.codProducto
LEFT JOIN categoriaProducto ON categoriaProducto.codCategoria = producto.codCategoria
INNER JOIN domicilio ON proveedor.idDomicilio = domicilio.idDomicilio
INNER JOIN localidad ON localidad.codLocalidad = domicilio.codLocalidad
INNER JOIN provincia ON provincia.codProvincia = localidad.codProvincia
INNER JOIN telefono ON telefono.idTelefono = proveedor.idTelefono
left join stock on stock.nroDetalleOrden = detalleOrden.nroDetalleOrden


GO
